package fr.soleil.tango.server.watcher.controller.cyclemanager;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.server.watcher.controller.AttributeSelection;
import fr.soleil.tango.server.watcher.controller.diagnosis.Diagnosis;
import fr.soleil.tango.server.watcher.db.DBReader;
import fr.soleil.tango.server.watcher.dto.attribute.ModeData;
import fr.soleil.tango.server.watcher.dto.result.Result;

/**
 *
 * @author fourneau
 *
 *         This abstract class is a default implementation of
 * @see{ICycleManager It can be use for simplify the implementation of a cycle
 *                    manager thus you only have to write the run method
 */
public abstract class AdapterCycleManager implements ICycleManager {

    protected static final String DO_RETRY = "DoRetry";

    /**
     * Used to tell the device if it should diagnose KO attributes. If set to
     * true, the archivers part of the generated attribute report will contain
     * specific attribute diagnosis information<br>
     * If set to false, those details will remain blank.<br>
     * Setting the property to true will increase the database load.
     */
    protected final boolean doDiagnosis;

    /**
     * Used to tell the device if it should ask the archiver to retry archiving
     * KO attributes. If set to true, the device automatically calls the
     * HDBArchiver command "retryForAttributes" for KO attributes.<br>
     * If set to false, it doesn't.
     */
    protected final boolean doRetry;

    /**
     * Object for communication with the database
     *
     * @see DBReader
     */
    protected final DBReader dbReader;

    /**
     * @see AttributeSelection
     */
    protected final AttributeSelection attributeSelection;

    /**
     * @see Diagnosis
     */
    protected Diagnosis diagnosis = null;

    /**
     * The number of cycle
     */
    protected int endCycle = 0;

    /**
     * The result report
     *
     * @see Result
     */
    protected final Result result;

    /**
     * A map for save different errors occured during lifetime of the object
     */
    protected final Map<String, String> errorMap = new ConcurrentHashMap<String, String>();

    // protected final Map<String, String> infos = new ConcurrentHashMap<String, String>();

    /**
     * The start of the current cycle. The difference, measured in milliseconds,
     * between the start time of the cycle and midnight, January 1, 1970 UTC.
     */
    protected volatile long cycleStartTime = 0;

    protected volatile long cycleStopTime = 0;
    /**
     * The number of attribute monitored
     */
    protected volatile int nbrAttrMonitoredCurrentCycle = 0;

    /**
     * The thread of diagnosis
     */
    protected Thread diagnosisThread;

    protected final boolean checkLastIsert;

    protected volatile Set<String> attributes;

    /**
     * Constructor
     *
     * @param dbReader
     * @param unitaryPause
     * @param doDiagnosis
     * @param doArchiverDiagnosis
     * @param doRetry
     * @param attributeSelection
     * @param safetyPeriod
     */
    public AdapterCycleManager(final DBReader dbReader, final boolean doDiagnosis, final boolean doArchiverDiagnosis,
            final boolean doRetry, final AttributeSelection attributeSelection, final int safetyPeriod,
            final boolean checkLastIsert) {
        this.doRetry = doRetry;
        this.dbReader = dbReader;
        this.attributeSelection = attributeSelection;
        this.doDiagnosis = doDiagnosis;
        this.result = new Result(doArchiverDiagnosis);
        this.checkLastIsert = checkLastIsert;
        attributes = null;
    }

    /**
     * Get the result from this CycleManager
     *
     * @see Result
     * @return the result from the cycle
     */
    @Override
    public final Result getResult() {
        return result;
    }

    /**
     * Get all monitored attributes
     *
     * @return Set<String> all monitored attributes
     * @throws DevFailed
     */
    @Override
    public final Set<String> getMonitoredAttributes() throws DevFailed {
        return attributeSelection.getAttributesToMonitor();
    }

    /**
     * Get the errors thats occurred
     *
     * @return Map<String, String> errors from this object
     */
    @Override
    public final Map<String, String> getErrors() {
        return errorMap;
    }

    // @Override
    // public final Map<String, String> getInfos() {
    // return infos;
    // }

    /**
     * Get the start date of the current cycle ie : The difference, measured in
     * milliseconds, between the start time of the cycle and midnight, January
     * 1, 1970 UTC.
     *
     * @return the start date of the current cycle
     */
    @Override
    public final long getCycleStartTime() {
        return cycleStartTime;
    }

    /**
     * Get the stop date of the current cycle ie : The difference, measured in
     * milliseconds, between the stop time of the cycle and midnight, January 1,
     * 1970 UTC.
     *
     * @return the stop date of the current cycle
     */
    @Override
    public final long getCycleStopTime() {
        return cycleStopTime;
    }

    /**
     * Get the number of attribute monitored
     *
     * @return the number of attribute monitored
     */
    @Override
    public final int getNbrAttrMonitoredCurrentCycle() {
        return nbrAttrMonitoredCurrentCycle;
    }

    /**
     * Get the number of cycle during the start of the CycleManager
     *
     * @return the number of cycle during the start of the CycleManager
     */
    @Override
    public final int getNbrEndCycle() {
        return endCycle;
    }

    /**
     * Put all the attributes with their archiving modes on the result object
     *
     * @throws DevFailed
     */
    @Override
    public void putAllAttributesToResult() throws DevFailed {
        final Map<String, ModeData> finalMap = new TreeMap<String, ModeData>(String.CASE_INSENSITIVE_ORDER);
        finalMap.putAll(dbReader.getModeDataForAll());
        result.putAllAttributes(finalMap);
    }

    /**
     * Get all archived attributes with their archiving modes
     *
     * @return String[] each row representing an archived attribute and its
     *         archiving mode
     */
    @Override
    public final String[] getAllAttributes() {
        final Map<String, ModeData> tmpMap = this.result.getAllAttributes();
        final String[] argout = new String[tmpMap.size()];
        int i = 0;
        for (final Map.Entry<String, ModeData> e : tmpMap.entrySet()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(e.getKey()).append(": ").append(e.getValue().getMode().toStringWatcher());
            argout[i++] = sb.toString();
        }
        return argout;
    }

    protected boolean shouldDiagnose() {
        return doDiagnosis;
    }

    @Override
    public final void startDiagnosis() {
        if (doDiagnosis) {
            diagnosis = new Diagnosis(dbReader, result, checkLastIsert);
            diagnosisThread = new Thread(diagnosis);
            diagnosisThread.setName("Watcher - Diagnosis");
            diagnosisThread.start();
        }
    }

    @Override
    public final void stopDiagnosis() {
        if (doDiagnosis && diagnosis != null) {
            diagnosis.stopDiagnosis();
            diagnosisThread.interrupt();
            diagnosisThread = null;
            diagnosis = null;
        }
    }

}

package fr.soleil.tango.server.watcher.controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevError;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.utils.DateUtil;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.tango.server.watcher.db.DBReader;
import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;
import fr.soleil.tango.server.watcher.dto.attribute.ModeData;
import fr.soleil.tango.server.watcher.dto.result.Archiver;

/**
 *
 * This class permit to control an archived attribute to define if is null or ko
 *
 * @author fourneau
 *
 */
public class Controller {

    private static final String NULL_VALUE = "null";

    /**
     * The command called to retry the archiving of some attributes
     */
    private static final String RETRY_COMMAND_NAME = "RetryForAttributes";

    /**
     * The logger for the device attribute
     */
    private final Logger loggerAttribute = LoggerFactory.getLogger("Watcher");

    /**
     * The logger
     */
    private final Logger logger = LoggerFactory.getLogger(Controller.class);

    /**
     * The timeout for the retry command
     */
    private static final int TIMEOUT_RETRY = 10000;

    /**
     * An enum for define the result of an controlled attribute
     *
     * @author fourneau
     *
     */
    public enum Control {
        CONTROL_OK, CONTROL_KO, CONTROL_NULL, CONTROL_NOT_DONE, CONTROL_FAILED
    }

    /**
     * To access the database
     */
    private final DBReader dbReader;

    /**
     * Given a theoretical archiving period, computes an effective, "safer"
     * period by which the archiving controls will be performed. This
     * saferPeriod is longer than the theoretical archiving period, to account
     * for the various delays introduced by network, database,... times and
     * delays. This way, one can avoid "false positives" controls, where fine
     * working attributes would be wrongfully marked KO because of those
     * physical delays.
     * */
    private final int safetyPeriod;

    /**
     * Constructor
     *
     * @param dbReader
     */
    public Controller(final DBReader dbReader) {
        this(dbReader, 0);
    }

    /**
     * Constructor
     *
     * @param dbReader
     * @param safetyPeriod
     */
    public Controller(final DBReader dbReader, final int safetyPeriod) {
        this.dbReader = dbReader;
        this.safetyPeriod = safetyPeriod;
    }

    /**
     * �Permit to control an attribute and know if it is well archived or not<br/>
     * ie : that looks how many records (if any) have been inserted since
     * <i>f(period)</i> ago. Where <i>f(period)</i> is the "safety period",
     * meaning a time span longer than period to allow for network (or any other
     * reason) delays. And check if the last value inserted is not null.
     *
     * @param attr
     * @return CONTROL_FAILED if the control has failed, CONTROL_KO if the the
     *         safety archiving period is exceeded, CONTROL_NULL if the attribut
     *         is archived but is null or CONTROL_OK
     * @throws DevFailed
     * @throws ArchivingException
     */
    public final Control control(final String attr, final ModeData modeData) throws DevFailed, ArchivingException {
        Control control = Control.CONTROL_FAILED;
        if (dbReader != null) {
            final String[] timeAndValue = dbReader.getTimeValueNullOrNotOfLastInsert(attr,
                    dbReader.getAttFormatCriterion(attr), this.dbReader.getAttWritableCriterion(attr));
            if (timeAndValue != null && modeData != null && modeData.getMode() != null) {
                // need to get the archiving polling time
                final ModePeriode modePeriode = modeData.getMode().getModeP();
                if (modePeriode != null) {
                    final long archivingPoll = modePeriode.getPeriod();
                    final long lastInsertTime = new Timestamp(DateUtil.stringToMilli(timeAndValue[0])).getTime();
                    final long delta = dbReader.now().getTime() - lastInsertTime;
                    if (delta > archivingPoll + this.safetyPeriod) {
                        control = Control.CONTROL_KO;
                    } else if (timeAndValue[1].equals(NULL_VALUE)) {
                        control = Control.CONTROL_NULL;
                    } else {
                        control = Control.CONTROL_OK;
                    }
                }
            }
        }
        return control;
    }

    /**
     * Permit to try to relaunch the archiving of ko attributes (not null).
     *
     * @param archivers
     * @param attributes
     * @throws DevFailed If a problem occurred when trying to relaunch the archiving of some attribute
     */
    public final void doRetry(final Collection<Archiver> archivers, final Map<String, ArchivedAttribute> attributes)
            throws DevFailed {
        final StringBuilder errorBuilder = new StringBuilder(
                "Failed to retry for KO attributes with following archivers: ");
        DevError[] errors = new DevError[0];
        boolean error = false;
        for (final Archiver archiver : archivers) {
            final String nextArchiver = archiver.getName();
            // TANGOARCH-497: Do not exit loop on the 1st archiver with problem. Retry for all available ones.
            try {
                final Collection<String> allKOAttributes = archiver.getKOAttributes();
                final Collection<String> koAttributes = new HashSet<String>();
                for (final String a : allKOAttributes) {
                    final ArchivedAttribute aa = attributes.get(a);
                    if (aa.getArchivingStatus() == Control.CONTROL_KO) {
                        // TANGOARCH-497: using complete name, we are sure the archiver will retry this attribute.
                        // koAttributes.add(aa.getDeviceName());
                        koAttributes.add(aa.getCompleteName());
                    }
                }
                final String[] parameters = koAttributes.toArray(new String[koAttributes.size()]);
                this.loggerAttribute.info("retrying {} on {}", allKOAttributes, nextArchiver);
                this.logger.debug("retrying {} on {}", allKOAttributes, nextArchiver);
                final DeviceProxy proxy = new DeviceProxy(nextArchiver);
                proxy.set_timeout_millis(TIMEOUT_RETRY);
                final DeviceData argin = new DeviceData();
                argin.insert(parameters);
                proxy.command_inout_asynch(RETRY_COMMAND_NAME, argin, true);
            } catch (final Exception e) {
                error = true;
                errorBuilder.append(nextArchiver);
                if (e instanceof DevFailed) {
                    final DevFailed df = (DevFailed) e;
                    final DevError[] tmp = df.errors;
                    if (tmp != null && tmp.length > 0) {
                        final int pos = errors.length;
                        errors = Arrays.copyOf(errors, pos + tmp.length);
                        System.arraycopy(tmp, 0, errors, pos, tmp.length);
                    }
                } else {
                    // unexpected error: register a new DevError for this one.
					final DevError tmp = new DevError(e.getMessage(), ErrSeverity.PANIC,
							appendErrorToStringBuilder(null, e).toString(), "Controller.doRetry");
                    errors = Arrays.copyOf(errors, errors.length + 1);
                    errors[errors.length - 1] = tmp;
                }
                errorBuilder.append(", ");
            }
        }
        if (error) {
            errorBuilder.delete(errorBuilder.length() - 2, errorBuilder.length());
            final DevError[] toThrow = new DevError[errors.length + 1];
            System.arraycopy(errors, 0, toThrow, 1, errors.length);
            toThrow[0] = new DevError("Communication error", ErrSeverity.ERR, errorBuilder.toString(),
                    "Controller.doRetry");
            throw new DevFailed(toThrow);
        }
    }

	private StringBuilder appendErrorToStringBuilder(StringBuilder builder, Throwable error) {
		if (builder == null) {
			builder = new StringBuilder();
		}
		if (error != null) {
			if (error instanceof DevFailed) {
				builder.append(DevFailedUtils.toString((DevFailed) error));
			} else {
				StringWriter errors = new StringWriter();
				error.printStackTrace(new PrintWriter(errors));
				builder.append(errors.toString());
			}
		}
		return builder;
	}
}
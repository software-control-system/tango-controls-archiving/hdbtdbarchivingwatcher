package fr.soleil.tango.server.watcher;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.tango.server.watcher.controller.cyclemanager.ICycleManager;
import fr.soleil.tango.server.watcher.dto.result.Result;
import fr.soleil.tango.server.watcher.notification.EMailNotification;
import fr.soleil.tango.server.watcher.notification.TextTalkerNotification;

/**
 * This class is in charge to send notification according to the Watcher device
 *
 * @author Fourneau
 *
 */
public class NotificationRunnable implements Runnable {

    public static final String EMAIL_COMMAND = "sendEmail";
    private static final int HOUR_OF_DAY_NOTIFICATION = 16;

    /**
     * The Logger
     */
    private final Logger logger = LoggerFactory.getLogger(NotificationRunnable.class);

    private final ICycleManager cycleManager;
    private final List<String> periodicRecipients;
    private final List<String> thresholdRecipients;
    private final long threshold;
    private boolean isThresholdExceeded = false;
    private boolean isThresholdExceededForTalker = false;
    private static final int SLEEP_DURATION = 60 * 1000;
    private final EMailNotification notificationSender = new EMailNotification();
    private TextTalkerNotification textTalkerNotification = null;
    private final String emitter;
    private int lastDayPeriodicSend = -1;
    private final Locale language;
    private final String notificationDevice;
    private DeviceProxy notifierProxy;

    /**
     * Constructor
     *
     * @param cycleManager
     * @param periodicRecipients
     * @param thresholdRecipients
     * @param talkerRecipient
     * @param threshold
     * @param emitter
     * @param notificationDevice
     * @param language
     * @throws DevFailed
     */
    public NotificationRunnable(final ICycleManager cycleManager, final List<String> periodicRecipients,
            final List<String> thresholdRecipients, final List<String> talkerRecipient, final long threshold,
            final String emitter, final String notificationDevice, final Locale language) throws DevFailed {

        if (periodicRecipients.size() == 0 && thresholdRecipients.size() == 0 && talkerRecipient.size() == 0) {
			throw DevFailedUtils.newDevFailed("We need at least one recipient");
        }
        if (talkerRecipient.size() > 0) {
            try {
                this.textTalkerNotification = new TextTalkerNotification(talkerRecipient);
            } catch (final DevFailed e) {
                DevFailedUtils.logDevFailed(e, this.logger);
				throw DevFailedUtils.newDevFailed(new StringBuilder("Error during TextTalker instantiation : ")
						.append(
                        DevFailedUtils.toString(e)).toString());
            }
        }
        this.language = language;
        this.cycleManager = cycleManager;
        this.periodicRecipients = periodicRecipients;
        this.thresholdRecipients = thresholdRecipients;
        this.threshold = threshold;
        this.emitter = emitter;
        this.notificationDevice = notificationDevice;
    }

    /**
     * Constructor
     *
     * @param cycleManager
     * @param periodicRecipients
     * @param thresholdRecipients
     * @param threshold
     * @throws DevFailed
     */
    public NotificationRunnable(final ICycleManager cycleManager, final List<String> periodicRecipients,
            final List<String> thresholdRecipients, final List<String> talkerRecipient, final long threshold,
            final String notificationDevice, final Locale language) throws DevFailed {
        this(cycleManager, periodicRecipients, thresholdRecipients, talkerRecipient, threshold,
                "no-reply@synchrotron-soleil.fr", notificationDevice, language);
    }

    @Override
    public final void run() {
        while (true) {
            this.logger.info("Notification runnable is running");
            final Result result = this.cycleManager.getResult();
            final int nbrOfKo = result.getNumberOfKoAttributes();
            // We want to send complete repport
            if (this.cycleManager.getNbrEndCycle() > 0) {

                if (nbrOfKo >= this.threshold) {
                    // TEXT TALKER
                    if (this.textTalkerNotification != null && !this.isThresholdExceededForTalker) {
                        this.textTalkerNotification.setMessage(this.getMessageFromLocale(nbrOfKo));
                        this.textTalkerNotification.emit();
                        this.isThresholdExceededForTalker = true;
                    }
                    // END TEXT TALKER
                    if (!this.isThresholdExceeded) {
                        this.send(TriggerNotification.THRESHOLD, result);
                        this.isThresholdExceeded = true;
                        this.logger.info("text talker should talk");
                    }
                } else {
                    if (this.isThresholdExceeded) {
                        this.isThresholdExceeded = false;
                        this.send(TriggerNotification.OK, result);
                    }
                    this.isThresholdExceededForTalker = false;
                }
                final Calendar calendar = Calendar.getInstance();
                // Daily at 8 am
                final int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                if (calendar.get(Calendar.HOUR_OF_DAY) == HOUR_OF_DAY_NOTIFICATION
                        && dayOfYear != this.lastDayPeriodicSend) {
                    this.send(TriggerNotification.PERIODIC, result);
                    this.lastDayPeriodicSend = dayOfYear;
                }

            }
            try {
                Thread.sleep(SLEEP_DURATION);
            } catch (final InterruptedException e) {
                Thread.currentThread().interrupt();
                break;
            }
        }
    }

    private void send(final TriggerNotification type, final Result result) {
        String[] recipients;
        String subject;
        switch (type) {
            case PERIODIC:
                recipients = this.periodicRecipients.toArray(new String[this.periodicRecipients.size()]);
                subject = "[Watcher] Periodic report ";
                break;
            case THRESHOLD:
                recipients = this.thresholdRecipients.toArray(new String[this.thresholdRecipients.size()]);
                subject = "[Watcher] /!\\ Threshold report ";
                break;
            case OK:
                recipients = this.thresholdRecipients.toArray(new String[this.thresholdRecipients.size()]);
                subject = "[Watcher] /!\\ OK report ";
                break;
            default:
                recipients = null;
                subject = null;
                break;
        }
        if (recipients != null) {
            final String[][] report = result.getSimpleReportPerAttribute(false);
            final StringBuilder sb = new StringBuilder();
            for (final String[] ss : report) {
                for (final String s : ss) {
                    sb.append(s).append(" ");
                }
                sb.append("\n");
            }
            if (sb.toString().trim().isEmpty()) {
                sb.append("No KO attribute");
            }
            if (notificationDevice == null) {
                this.notificationSender.setEmitter(this.emitter);
                this.notificationSender.setRecipient(recipients);
                this.notificationSender.setSubject(subject);
                this.notificationSender.setMessage(sb.toString());
                this.notificationSender.emit();
            } else {
                try {
                    if (notifierProxy == null) {
                        notifierProxy = DeviceProxyFactory.get(notificationDevice);
                    }
                    final DeviceData argin = new DeviceData();
                    final String[] parameters = new String[recipients.length + 2];
                    parameters[0] = subject;
                    parameters[1] = sb.toString();
                    System.arraycopy(recipients, 0, parameters, 2, recipients.length);
                    argin.insert(parameters);
                    notifierProxy.command_inout_asynch(EMAIL_COMMAND, argin, true);
                } catch (final Exception e) {
					logger.error(appendErrorToStringBuilder(
                            new StringBuilder("Failed to ask device '").append(notificationDevice).append(
                                    "' to send emails:\n"), e).toString());
                }
            }
        }
    }

    private String getMessageFromLocale(final int nbrOfKo) {
        final StringBuilder sb = new StringBuilder();
        if (this.language.equals(Locale.FRENCH)) {
            sb.append("Le seuil de KO est dépassé sur l'archivage. Il y a ").append(nbrOfKo).append(" attributs KO");
        } else /* (this.language.equals(Locale.ENGLISH)) */{
            sb.append("The KO attributes threshold is exceed on the archiving system. There are ").append(nbrOfKo)
            .append(" KO attributes");
        }
        return sb.toString();
    }

    // ///////////// //
    // Inner Classes //
    // ///////////// //

    public static enum TriggerNotification {
        THRESHOLD, PERIODIC, OK
    }

	public static StringBuilder appendErrorToStringBuilder(StringBuilder builder, Throwable error) {
		if (builder == null) {
			builder = new StringBuilder();
		}
		if (error != null) {
			if (error instanceof DevFailed) {
				builder.append(DevFailedUtils.toString((DevFailed) error));
			} else {
				StringWriter errors = new StringWriter();
				error.printStackTrace(new PrintWriter(errors));
				builder.append(errors.toString());
			}
		}
		return builder;
	}

}

package fr.soleil.tango.server.watcher.controller.diagnosis;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.lib.project.ObjectUtils;

/**
 * This class permit to add some information to a diagnosis
 *
 * @author fourneau
 *
 */
public class DiagnosisInfo implements Comparable<DiagnosisInfo> {

    private static final String DD_MM_YYYY_HH_MM_SS = "dd-MM-yyyy HH:mm:ss";
    /**
     * The raw diagnosis
     */
    private final ResDiagnosis diagnosis;
    /**
     * Some more information of the diagnosis, like an exception
     */
    private String info;
    /**
     * The date of last insert value
     */
    private final String lastInsert;

    private final Date infoDate;

    /**
     * Constructor
     *
     * @param diagnosis
     *            The raw diagnosis
     */
    public DiagnosisInfo(final ResDiagnosis diagnosis) {
        this(diagnosis, "", "");
    }

    /**
     * Constructor
     *
     * @param diagnosis
     *            The raw diagnosis
     * @param lastInsert
     *            The date of last insert value
     */
    public DiagnosisInfo(final ResDiagnosis diagnosis, final String lastInsert) {
        this(diagnosis, "", lastInsert);
    }

    /**
     * Constructor
     *
     * @param diagnosis
     *            The raw diagnosis
     * @param e
     *            An exception for the information field
     */
    public DiagnosisInfo(final ResDiagnosis diagnosis, final DevFailed e) {
        this(diagnosis, DevFailedUtils.toString(e), "");
    }

    /**
     * Constructor
     *
     * @param diagnosis
     *            diagnosis The raw diagnosis
     * @param e
     *            An exception for the information field
     * @param lastInsert
     *            The date of last insert value
     */
    public DiagnosisInfo(final ResDiagnosis diagnosis, final DevFailed e, final String lastInsert) {
        this(diagnosis, DevFailedUtils.toString(e), lastInsert);
    }

    /**
     * Constructor
     *
     * @param diagnosis The raw diagnosis
     * @param info some more information
     * @param lastInsert The date of last insert value
     */
    public DiagnosisInfo(final ResDiagnosis diagnosis, final String info, final String lastInsert) {
        this.diagnosis = diagnosis == null ? ResDiagnosis.DIS_DIAG : diagnosis;
        this.info = info == null ? "" : info;
        this.lastInsert = lastInsert == null ? "" : lastInsert;
        this.infoDate = new Date(System.currentTimeMillis());
    }

    public void addInfo(final String info) {
        this.info = this.info + info;
    }

    /**
     * Get the ResDiagnosis : The raw diagnosis
     *
     * @return ResDiagnosis The raw diagnosis
     */
    public final ResDiagnosis getDiagnosis() {
        return this.diagnosis;
    }

    /**
     * Get some more information about the diagnosis
     *
     * @return String some more information about the diagnosis
     */
    public final String getInfo() {
        return this.info + " - " + new SimpleDateFormat(DD_MM_YYYY_HH_MM_SS).format(infoDate);
    }

    /**
     * Get the last insert date
     *
     * @return the last insert date
     */
    public final String getLastInsert() {
        return this.lastInsert;
    }

    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.diagnosis);
        if (!this.info.trim().isEmpty()) {
            sb.append(". ");
            sb.append(this.info);
        }
        if (!this.lastInsert.trim().isEmpty()) {
            sb.append(". Last insert ok is : ").append(this.lastInsert);
        }
        sb.append(" - ").append(new SimpleDateFormat(DD_MM_YYYY_HH_MM_SS).format(infoDate));
        return sb.toString();
    }

    @Override
    public final boolean equals(final Object o) {
        boolean equals;
        if (this == o) {
            equals = true;
        } else if (o == null) {
            equals = false;
        } else if (ObjectUtils.sameObject(getClass(), o.getClass())) {
            final DiagnosisInfo other = (DiagnosisInfo) o;
            equals = ObjectUtils.sameObject(diagnosis, other.diagnosis) && ObjectUtils.sameObject(info, other.info)
                    && ObjectUtils.sameObject(lastInsert, other.lastInsert);
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public final int compareTo(final DiagnosisInfo tmp) {
        int compare;
        if (this.equals(tmp)) {
            compare = 0;
        } else {
            compare = this.diagnosis.compareTo(tmp.getDiagnosis());
        }
        return compare;
    }
}

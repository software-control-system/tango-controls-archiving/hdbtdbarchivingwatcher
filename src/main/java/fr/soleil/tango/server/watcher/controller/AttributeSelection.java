package fr.soleil.tango.server.watcher.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.server.watcher.db.DBReader;
import fr.soleil.tango.server.watcher.dto.attribute.ModeData;

/**
 * This class permit to select attribute archived in the data base according to
 * his type/format/writable type.
 *
 * @author fourneau
 *
 */
public class AttributeSelection {

    /**
     * To access the database
     */
    private final DBReader dbReader;
    /**
     * A list of regex to only select attributes that validate these
     */
    private List<String> onlyIncludeAttributes;
    /**
     * A list of regex to exclude attributes that validate these
     */
    private List<String> excludeAttributes;
    /**
     * An ordered set of the selected attributes
     */
    private Set<String> attributes = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
    /**
     * An array of format that the select attributes must respect
     */
    private AttrDataFormat[] formats;
    /**
     * An array of types that the select attributes must respect
     */
    private Integer[] types;
    /**
     * An array of writables types that the select attributes must respect
     */
    private AttrWriteType[] writables;

    /**
     * Constructor
     *
     * @param dbReader
     * @throws DevFailed
     */
    public AttributeSelection(final DBReader dbReader) throws DevFailed {
        this(dbReader, null, null, null, null, null);
    }

    /**
     * Constructor
     *
     * @param dbReader
     * @param onlyIncludeAttributes
     * @throws DevFailed
     */
    public AttributeSelection(final DBReader dbReader, final List<String> onlyIncludeAttributes) throws DevFailed {
        this(dbReader, onlyIncludeAttributes, null, null, null, null);
    }

    /**
     * Constructor.<br/>
     * Call the constructor, will not compute the list, you should call the
     * refreshAttributes method !
     *
     * @param dbReader
     *            to access the data base
     * @param onlyIncludeAttributes
     *            A list of regex to only select attributes that validate these
     * @param excludeAttributes
     *            A list of regex to exclude attributes that validate these
     * @param formats
     *            An array of format that the select attributes must respect
     * @param types
     *            An array of types that the select attributes must respect
     * @param writables
     *            An array of writable types that the select attributes must
     *            respect
     */
    public AttributeSelection(final DBReader dbReader, final List<String> onlyIncludeAttributes,
            final List<String> excludeAttributes, final AttrDataFormat[] formats, final Integer[] types,
            final AttrWriteType[] writables) {
        if (onlyIncludeAttributes == null) {
            this.onlyIncludeAttributes = new ArrayList<String>();
        } else {
            this.onlyIncludeAttributes = onlyIncludeAttributes;
        }
        if (excludeAttributes == null) {
            this.excludeAttributes = new ArrayList<String>();
        } else {
            this.excludeAttributes = excludeAttributes;
        }
        if (formats == null) {
            this.formats = new AttrDataFormat[0];
        } else {
            this.formats = Arrays.copyOf(formats, formats.length);
        }
        if (types == null) {
            this.types = new Integer[0];
        } else {
            this.types = Arrays.copyOf(types, types.length);
        }
        if (writables == null) {
            this.writables = new AttrWriteType[0];
        } else {
            this.writables = Arrays.copyOf(writables, writables.length);
        }
        this.dbReader = dbReader;
    }

    /**
     * Get the attribute archived in the database and create the list of
     * selected attributes according to their type/format/writable type and some
     * regex of exclude attribute nor only include attributes.
     *
     * @throws DevFailed
     */
    public void compute() throws DevFailed {
        final Map<String, ModeData> allAttributes = this.dbReader.getArchivedAttributes(this.writables, this.types,
                this.formats);
        final Set<String> res = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
        for (final String attribute : allAttributes.keySet()) {
            if (attribute != null) {
                final String attributeToLowerCase = attribute.toLowerCase(Locale.ENGLISH);
                boolean excluded = false;
                final Collection<String> excludeAttributes = this.excludeAttributes;
                if (excludeAttributes != null && !excludeAttributes.isEmpty()) {
                    for (final String excludeAttribute : excludeAttributes) {
                        if (matches(attribute, attributeToLowerCase, excludeAttribute)) {
                            excluded = true;
                            break;
                        }
                    }
                }
                if (!excluded) {
                    final Collection<String> onlyIncludeAttributes = this.onlyIncludeAttributes;
                    if (onlyIncludeAttributes != null && !onlyIncludeAttributes.isEmpty()) {
                        for (final String includeAttribute : onlyIncludeAttributes) {
                            if (matches(attribute, attributeToLowerCase, includeAttribute)) {
                                res.add(attribute);
                                break;
                            }
                        }
                    } else {
                        res.add(attribute);
                    }
                }
            }
        }
        this.attributes = res;
    }

    protected static boolean matches(final String toTest, final String regex) {
        return matches(toTest, toTest == null ? null : toTest.toLowerCase(Locale.ENGLISH), regex);
    }

    protected static boolean matches(final String toTest, final String toTestLowerCase, final String regex) {
        boolean matches;
        if (toTest == null || toTestLowerCase == null || regex == null) {
            matches = false;
        } else {
            try {
                matches = toTestLowerCase.matches(regex) || toTest.matches(regex)
                        || toTestLowerCase.matches(regex.toLowerCase(Locale.ENGLISH));
            } catch (final Exception e) {
                matches = false;
            }
        }
        return matches;
    }

    /**
     * Set the list of regex to only select attributes that validate these
     *
     * @param onlyIncludeAttributes
     *            A list of regex to only select attributes that validate these
     * @throws DevFailed
     */
    public final void setOnlyIncludeAttributes(final List<String> onlyIncludeAttributes) throws DevFailed {
        this.onlyIncludeAttributes = onlyIncludeAttributes;
        this.compute();
    }

    /**
     * Set the list of regex to exclude attributes that validate these
     *
     * @param excludeAttributes
     *            A list of regex to exclude attributes that validate these
     * @throws DevFailed
     */
    public final void setExcludeAttributes(final List<String> excludeAttributes) throws DevFailed {
        this.excludeAttributes = excludeAttributes;
        this.compute();
    }

    /**
     * Set a list of regex to only select attributes that validate these and a
     * list of regex to exclude attributes that validate these
     *
     * @param onlyIncludeAttributes
     *            A list of regex to only select attributes that validate these
     * @param excludeAttributes
     *            A list of regex to exclude attributes that validate these
     * @throws DevFailed
     */
    public final void setExcludeAndIncludeAttributes(final List<String> onlyIncludeAttributes,
            final List<String> excludeAttributes) throws DevFailed {
        this.onlyIncludeAttributes = onlyIncludeAttributes;
        this.excludeAttributes = excludeAttributes;
        this.compute();
    }

    /**
     * Get the list of selected attributes
     *
     * @return Set<String> : list of selected attributes
     * @throws DevFailed
     */
    public final Set<String> getAttributesToMonitor() throws DevFailed {
        this.compute();
        return this.attributes;
    }

    /**
     * Force the refresh of the selected attributes. In case of you have changed
     * some configuration or the archived attributes are changed too.
     *
     * @throws DevFailed
     */
    public final void refreshAttributes() throws DevFailed {
        this.compute();
    }

    /**
     * Get the number of attribute selected
     *
     * @return int : the number of attribute selected
     */
    public final int getNbrAttributesToMonitor() {
        return this.attributes.size();
    }

    /**
     * Get the formats used
     *
     * @return AttrDataFormat[]
     */
    public final AttrDataFormat[] getFormats() {
        return Arrays.copyOf(this.formats, this.formats.length);
    }

    /**
     * Set new format type condition
     *
     * @param formats
     *            AttrDataFormat[]
     * @throws DevFailed
     */
    public final void setFormats(final AttrDataFormat[] formats) throws DevFailed {
        this.formats = Arrays.copyOf(formats, formats.length);
        this.compute();
    }

    /**
     * Get the types used
     *
     * @return Integer[]
     */
    public final Integer[] getTypes() {
        return Arrays.copyOf(this.types, this.types.length);
    }

    /**
     * Set new type condition
     *
     * @param types
     *            Integer[]
     * @throws DevFailed
     */
    public final void setTypes(final Integer[] types) throws DevFailed {
        this.types = Arrays.copyOf(types, types.length);
        this.compute();
    }

    /**
     * Get the writable types used
     *
     * @return AttrWriteType[]
     */
    public final AttrWriteType[] getWritables() {
        return Arrays.copyOf(this.writables, this.writables.length);
    }

    /**
     * Set new writable type condition
     *
     * @param writables
     *            AttrWriteType[]
     * @throws DevFailed
     */
    public final void setWritables(final AttrWriteType[] writables) throws DevFailed {
        this.writables = Arrays.copyOf(writables, writables.length);
        this.compute();
    }

}

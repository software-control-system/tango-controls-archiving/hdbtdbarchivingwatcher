/*
 * Synchrotron Soleil
 * 
 * File : Attribute.java
 * 
 * Project : archiving_watcher
 * 
 * Description :
 * 
 * Author : CLAISSE
 * 
 * Original : 28 nov. 2005
 * 
 * Revision: Author:
 * Date: State:
 * 
 * Log: Attribute.java,v
 */
/*
 * Created on 28 nov. 2005
 * 
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.tango.server.watcher.dto.attribute;

import java.sql.Timestamp;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.tango.server.watcher.controller.Controller.Control;
import fr.soleil.tango.server.watcher.controller.diagnosis.DiagnosisInfo;
import fr.soleil.tango.server.watcher.controller.diagnosis.ResDiagnosis;

/**
 * Models an attribute that is supposed to be archiving
 *
 */
public class ArchivedAttribute implements Comparable<ArchivedAttribute> {
    /**
     * The logger
     */
    private final Logger logger = LoggerFactory.getLogger(ArchivedAttribute.class);

	public static final String SEPARATOR = "/";
    /**
     * The complete name of the attribute
     */
    private String completeName = "";
    /**
     * The last insert in database
     */
    private Timestamp lastInsert;
    /**
     * The last try to insert on the database
     */
    private Timestamp lastInsertRequest;
    /**
     * The result of the control
     */
    private Control archivingStatus = Control.CONTROL_NOT_DONE;
    /**
     * The diagnosis of the control
     */
    private DiagnosisInfo diagnosis = new DiagnosisInfo(ResDiagnosis.NOT_DIAG);
    /**
     * The archiver name in charge of the attribute
     */
    private String archiver;
    /**
     * The period of archiving
     */
    private int period = -1;
    /**
     * The id on the database
     */
    private int id = -1;
    /**
     * The tango domain of the attribute
     */
    private String domain;
    /**
     * The tango family if the atribute
     */
    private String family;
    /**
     * The tango attribute short name of the attribute
     */
    private String attribute;
    /**
     * The device name in charge of the attribute
     */
    private String device;

    /**
     * Constructor
     *
     * @param attributeName
     *            the full attribute name
     */
    public ArchivedAttribute(final String attributeName) {
        this.completeName = attributeName;
		final StringTokenizer st = new StringTokenizer(this.completeName, SEPARATOR);

        try {
            this.domain = st.nextToken();
            this.family = st.nextToken();
            final String mem = st.nextToken();
            this.attribute = st.nextToken();
			this.device = this.domain + SEPARATOR + this.family + SEPARATOR + mem;

        } catch (final NoSuchElementException e) {
            this.logger.error(e.getMessage());
        }
    }

    /**
     * @return Returns the completeName.
     */
    public final String getCompleteName() {
        return this.completeName;
    }

    /**
     * @return Returns the lastInsert.
     */
    public final Timestamp getLastInsert() {
        return this.lastInsert;
    }

    /**
     * @param lastInsert
     *            The lastInsert to set.
     */
    public final void setLastInsert(final Timestamp lastInsert) {
        this.lastInsert = lastInsert;
    }

    /**
     * @return Returns the attributeStatus.
     */
    public final Control getArchivingStatus() {
        return this.archivingStatus;
    }

    /**
     * @param attributeStatus
     *            The attributeStatus to set.
     */
    public final void setArchivingStatus(final Control attributeStatus) {
        this.archivingStatus = attributeStatus;
    }

    /**
     * Returns the domain part of the attribute's complete name
     *
     * @return The domain name
     */
    public final String getDomain() {
        return this.domain;
    }

    /**
     * Returns the family part of the attribute's complete name
     *
     * @return The domain name
     */
    public final String getFamily() {
        return this.family;
    }

    /**
     * Returns the device part of the attribute's complete name
     *
     * @return The device name
     */
    public final String getDeviceName() {
        return this.device;
    }

    /**
     * @return Returns the archiver.
     */
    public final String getArchiver() {
        return this.archiver;
    }

    /**
     * @param archiver
     *            The archiver to set.
     */
    public final void setArchiver(final String archiver) {
        this.archiver = archiver;
    }

    /**
     * @return Returns the period.
     */
    public final int getPeriod() {
        return this.period;
    }

    /**
     * @param period
     *            The period to set.
     */
    public final void setPeriod(final int period) {
        this.period = period;
    }

    /**
     * Returns the attribute part of the attribute's complete name
     *
     * @return The attribute name
     */
    public final String getAttributeSubName() {
        return this.attribute;
    }

    /**
     * @param lastInsertRequest
     */
    public final void setLastInsertRequest(final Timestamp lastInsertRequest) {
        this.lastInsertRequest = lastInsertRequest;
    }

    /**
     * @return Returns the lastInsertRequest.
     */
    public final Timestamp getLastInsertRequest() {
        return this.lastInsertRequest;
    }

    /**
     * Set the id of the attribute in the database
     *
     * @return the id of attribute in the database
     */
    public final int getId() {
        return this.id;

    }

    /**
     * Get the id of attribute in the database
     *
     * @param id
     *            the id of attribute in the database
     */
    public final void setId(final int id) {
        this.id = id;
    }

    /**
     * Get the diagnosis of the attribute
     *
     * @return DiagnosisInfo the diagnosis of the attribute
     */
    public final DiagnosisInfo getDiagnosis() {
        return this.diagnosis;
    }

    /**
     * Set the diagnosis of the attribute
     *
     * @param DiagnosisInfo
     *            the diagnosis of the attribute
     */
    public final void setDiagnosis(final DiagnosisInfo diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public final int compareTo(final ArchivedAttribute aa) {
        return this.diagnosis.compareTo(aa.getDiagnosis());
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ArchivedAttribute other = (ArchivedAttribute) obj;
        if (this.archiver == null) {
            if (other.archiver != null) {
                return false;
            }
        } else if (!this.archiver.equals(other.archiver)) {
            return false;
        }
        if (this.archivingStatus != other.archivingStatus) {
            return false;
        }

        if (this.completeName == null) {
            if (other.completeName != null) {
                return false;
            }
        } else if (!this.completeName.equals(other.completeName)) {
            return false;
        }
        if (this.device == null) {
            if (other.device != null) {
                return false;
            }
        } else if (!this.device.equals(other.device)) {
            return false;
        }
        if (this.diagnosis == null) {
            if (other.diagnosis != null) {
                return false;
            }
        } else if (!this.diagnosis.equals(other.diagnosis)) {
            return false;
        }
        if (this.lastInsert == null) {
            if (other.lastInsert != null) {
                return false;
            }
        } else if (!this.lastInsert.equals(other.lastInsert)) {
            return false;
        }
        if (this.lastInsertRequest == null) {
            if (other.lastInsertRequest != null) {
                return false;
            }
        } else if (!this.lastInsertRequest.equals(other.lastInsertRequest)) {
            return false;
        }
        if (this.period != other.period) {
            return false;
        }
        return true;
    }

}

package fr.soleil.tango.server.watcher;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.tango.DeviceState;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.AttributeProperties;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.DynamicManagement;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.Status;
import org.tango.server.annotation.TransactionType;
import org.tango.server.attribute.log.LogAttribute;
import org.tango.server.dynamic.DynamicManager;
import org.tango.utils.DevFailedUtils;
import org.tango.utils.TangoUtil;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Group.Group;
import fr.soleil.archiving.common.api.ConnectionFactory;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.utils.GetConf;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.server.watcher.controller.AttributeSelection;
import fr.soleil.tango.server.watcher.controller.cyclemanager.CycleManager;
import fr.soleil.tango.server.watcher.controller.cyclemanager.CycleManagerFromDb;
import fr.soleil.tango.server.watcher.controller.cyclemanager.ICycleManager;
import fr.soleil.tango.server.watcher.controller.diagnosis.Diagnosis;
import fr.soleil.tango.server.watcher.controller.diagnosis.DiagnosisInfo;
import fr.soleil.tango.server.watcher.db.DBReader;
import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;
import fr.soleil.tango.server.watcher.dto.attribute.ModeData;
import fr.soleil.tango.server.watcher.dto.result.Archiver;
import fr.soleil.tango.server.watcher.dto.result.Result;
import fr.soleil.tango.statecomposer.StateResolver;

/**
 * This device is in charge of controlling archiving.It does so by comparing the
 * theoretical archiving (the attributes that have been registered for
 * archiving) with the effective archiving (are records being added for the
 * registered attributes). If the effective archiving matches the theoretical
 * archiving, the attribute is called "OK", otherwise "KO".
 *
 * @author fourneau
 *
 */
@Device(transactionType = TransactionType.NONE)
public class HDBTDBArchivingWatcher {

    // reactivate following code to test PROBLEM-1014
    // protected static volatile long globalStart;

    private static final String DATABASE_ERROR = "No connection to the database.\nThe database may be down or malfunctioning.";
    private static final String ERROR_SEPARATOR = "------------\n";
    private static final int NUMBER_LEGENDS_DIFF_REPORT = 7;
    private static final int DEPTH_LOG_ATTRIBUTE = 1000;

    /**
     * A subjective duration before start the thread of periodic report. There
     * is a initial delay because we have to wait that the data base connection
     * is open.
     */
    private static final long INITIAL_DELAY_FOR_REPORT = 20L * 1000L;

    /**
     * The regular expression for validate email address. This will work with 99% of valid
     * mail address. But the real regular expression to validate all good email address is to
     * complicated
     */
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    /**
     * The regular expression for validate device name
     */
    private static final String DEVICE_PATTERN = "^[-_\\.A-Za-z0-9]+/[-_\\.A-Za-z0-9]+/[-_\\.A-Za-z0-9]+/?";
    /**
     * The response of report attribute where no attributes are KO.
     */
    private static final String[][] NO_KO_ATTRIBUTES = new String[][] { { "No KO attributes" } };

    /**
     * String use for data display
     */
    private static final String STR_RESULT_PRES = "=> ";

    /**
     * The default threshold notification.
     */
    private static final long NOTIFICATION_THRESHOLD_DEFAULT = 50;

    /**
     * A SimpleDateFormat for format date in local thread for thread safety.
     */
    private static final DateThreadLocal SIMPLE_DATE_FORMAT = new DateThreadLocal();
    private static final String DEFAULT_ARCHIVER_CLASS = ConfigConst.HDB_CLASS_DEVICE;
    private static final boolean DEFAULT_CHECK_LAST_VALID = false;
    private static String version = "UNKNOWN";

    /**
     * The Logger
     */
    private final Logger logger = LoggerFactory.getLogger(HDBTDBArchivingWatcher.class);
    /**
     * The XLogger
     */
    private final XLogger xlogger = XLoggerFactory.getXLogger(HDBTDBArchivingWatcher.class);

    /**
     * A boolean to tell if the read of archiver state group has failed
     */
    private final boolean archiverFailed = false;

    /**
     * Tango types to select attributes to monitor. This is fill after parsing
     * onlyIncludeList device property
     */
    private Integer[] attrsType = new Integer[0];
    /**
     * Formats to select attributes to monitor. This is fill after parsing
     * onlyIncludeList device property
     */
    private AttrDataFormat[] attrsDataFormat = new AttrDataFormat[0];
    /**
     * Writable types to select attributes to monitor. This is fill after
     * parsing onlyIncludeList device property
     */
    private AttrWriteType[] attrsWriteType = new AttrWriteType[0];
    /**
     * Regex to select attributes to monitor. This is fill after parsing
     * onlyIncludeList device property
     */
    private List<String> onlyIncludeRegex = new ArrayList<String>();

    /**
     * Boolean use to tell if the perAttributeReport must be sort by diagnos or
     * by attribute name
     */
    private boolean sortedResultByType = true;
    /**
     * Object for communication with the database
     *
     * @see DBReader
     */
    private DBReader dbReader;
    /**
     * Configure and manage the cycle life of the watcher
     *
     * @see CycleManager
     */
    private ICycleManager cycleManager;
    /**
     * The thread to manage the CycleManager
     *
     * @see CycleManager
     */
    private CycleManagerThread cycleThread;
    /**
     * The thread to manage notifications sending
     *
     * @see NotificationRunnable
     */
    private Thread notificationThread;
    /**
     * The real safety duration convert from the device property safetyPeriod
     */
    private int safetyDuration;
    /**
     * The manager to select attribute to monitor according to excludList and
     * onlyIncludList device properties
     *
     * @see AttributeSelection
     */
    private AttributeSelection attributeSelection;
    /**
     * The list of recipients who wants to be notified when the threshold of KO
     * attributes is exceeded. This list is extract from the notificationContact
     * device property
     */
    private final List<String> thresholdRecipient = new ArrayList<String>();
    /**
     * The list of recipients who wants to periodically get a report of the
     * watcher. This list is extract from the notificationContact device
     * property
     */
    private final List<String> periodicRecipient = new ArrayList<String>();

    private final List<String> talkerRecipient = new ArrayList<String>();
    /**
     * An executor service for periodically execute @DiffReportRunnable
     */
    private ScheduledExecutorService executor;
    /**
     * The return of @see ScheduledExecutorService
     */
    private ScheduledFuture<?> future;

    /**
     * The @StateComposer permit to get the state of all the archivers. Moreover
     * he will give us the global state of all the archiver.
     */
    private StateResolver stateResolver;

    /**
     * The time between each report convert from the device property
     * periodicReport
     */
    private int periodicDuration;
    /**
     * The report create by @DiffReportRunnable and use by @getDiffReport
     */
    private String[][] diffReport = null;
    /**
     * A runnable for create a difference report
     */
    private DiffReportRunnable diffReportRunnable = null;

    private Locale textTalkerLocale;
    private String lastConnectionError;

    /**
     * The dynamic manager
     */
    @DynamicManagement
    private DynamicManager dynamicManager;

    // ////////// //
    // Properties //
    // ////////// //

    /**
     * Used to tell the device if it should diagnose KO archivers (archivers
     * that have at least 1 KO attribute). If set to true, the archivers part of
     * the generated control report will contain specific archiver information
     * such as scalar load etc..<br />
     * If set to false, those details will remain blank.<br />
     * Setting the property to true will increase the database load. <b>Default
     * value : </b>false
     */
    @DeviceProperty(description = "Boolean used to tell the device if it should diagnose KO archivers (archivers that have at least 1 KO attribute)")
    private boolean doArchiverDiagnosis = false;

    /**
     * Used to tell the device if it should diagnose KO attributes. If set to
     * true, the archivers part of the generated attribute report will contain
     * specific attribute diagnosis information<br />
     * If set to false, those details will remain blank.<br />
     * Setting the property to true will increase the database load. <b>Default
     * value : </b>false
     */
    @DeviceProperty(description = "Boolean used to tell the device if it should diagnose KO attributes ")
    private boolean doDiagnosis = true;

    /**
     * Used to tell the device if it should ask the archiver to retry archiving
     * KO attributes. If set to true, the device automatically calls the
     * HDBArchiver command "retryForAttributes" for KO attributes.<br />
     * If set to false, it doesn't.<br />
     * <b>Default value : </b>false
     */
    @DeviceProperty(description = "Boolean used to tell the device if it should ask the archiver to retry archiving KO attributes")
    private boolean doRetry = false;

    /**
     * When the Watcher retrieves the attributes, it will check that none of
     * them satisfies one of the regular expressions. If this is the case, then
     * the attribute will be ignored. Otherwise, the attribute will be
     * monitored.<br/>
     * Example:<br/>
     * ".*\/EI/.*"<br/>
     * All the attributes of the family "EI" will not be monitored
     */
    @DeviceProperty(description = "Regex corresponding to attributes that watcher will not control")
    private String[] excludeList = new String[0];

    /**
     * Computer identifier on wich is settled the database DB. The identifier
     * can be the computer name or its IP address. <br />
     * <b>Default value : </b> db
     */
    @DeviceProperty(description = "Computer identifier on wich is settled the database DB")
    private String dbHost = "";

    /**
     * Database name.<br />
     * <b>Default value : </b> db
     */
    @DeviceProperty(description = "Database name")
    private String dbName = "";

    /**
     * Password used to connect the database. <br />
     * <b>Default value : </b> db
     */
    @DeviceProperty(description = "Password used to connect the database")
    private String dbPwd = "";

    /**
     * true if the ORACLE RAC connection is activated. This information is
     * appended to all device's (or attributes) name. false otherwise.<br />
     * <b>Default value : </b> false
     */
    // @DeviceProperty(description = "True if the ORACLE RAC connection is activated")
    // private Boolean dbRacConnection = null;

    /**
     * The schema of the data base (HDB or TDB)
     */
    // @DeviceProperty
    // private String dbSchema = "";

    /**
     * User identifier (name) used to connect the database. <br />
     * <b>Default value : </b> hdb
     */
    @DeviceProperty(description = "User identifier (name) used to connect the database")
    private String dbUser = "";

    /**
     * The notification device is the device in charge of sending emails, if not the watcher itself.
     */
    @DeviceProperty(description = "The notification is the device in charge of sending emails, if not the watcher itself.")
    private String notificationDevice = "";

    /**
     * The notification means of the recipient followed by the type of trigger
     * for the notification
     * <ul>
     * <li>NONE</li>
     * <li>THRESHOLD</li>
     * <li>PERIODIC</li>
     * <li>FULL = (THRESHOLD + PERIODIC)</li>
     * </ul>
     */
    @DeviceProperty(description = "The notification means of the recipient followed by the type of trigger for the notification")
    private String[] notificationContact = new String[0];

    /**
     * The number of ko attributes threshold to trigger a notification.
     * <b>Default value : </b> 50
     */
    @DeviceProperty(description = "The number of ko attributes threshold to trigger a notification")
    private long notificationThreshold = NOTIFICATION_THRESHOLD_DEFAULT;

    /**
     * Regex corresponding to devices that watcher will control
     */
    @DeviceProperty(description = "Regex corresponding to devices that watcher will control")
    private String[] onlyIncludeList = new String[0];

    /**
     * A number for define the minimal time without data in database for detect
     * a gap
     */
    @DeviceProperty
    private int gapDuration = 900000;

    /**
     * Given a theoretical archiving period, computes an effective, "safer"
     * period by which the archiving controls will be performed. This
     * saferPeriod is longer than the theoretical archiving period, to account
     * for the various delays introduced by network, database,... times and
     * delays. This way, one can avoid "false positives" controls, where fine
     * working attributes would be wrongfully marked KO because of those
     * physical delays.
     */

    /**
     * Permit to configure the language of the textTalker for permit him to
     * correctly talk.
     */
    @DeviceProperty
    private String textTalkerLanguage = "FRENCH";

    @DeviceProperty(description = "Given a theoretical archiving period, computes an effective, \"safer\" period by which the archiving controls will be performed.")
    private String safetyPeriod = "15 minutes";

    /**
     * A pause between each attribute check for not overload the database
     */
    @DeviceProperty(description = "A pause between each attribute check for not overload the database")
    private long unitaryPause = 2000;

    /**
     * A pause between each cycle for not overload the database
     */
    @DeviceProperty(description = "A pause between each cycle for not overload the database")
    private int cyclePause = 50000;

    /**
     * A duration for trigger periodic report
     */
    @DeviceProperty(description = "A duration for trigger periodic report")
    private String periodicReport = "86400 seconde";

    /**
     * Boolean used to tell the device if it should do a difference report.
     *
     * @see DiffReportRunnable
     */
    @DeviceProperty(description = "Boolean used to tell the device if it should do a difference report")
    private boolean doDiffReport = true;

    /**
     * Boolean use to tell the device if he has to really check the attribute or
     * if he can just read the information of the common table with ODA.
     */
    @DeviceProperty
    private boolean realCheck = true;

    /**
     * Archiver class. Used to recover database configuration.
     */
    @DeviceProperty(description = "arching device class from which to read database configuration")
    private String archiverClass = DEFAULT_ARCHIVER_CLASS;

    /**
     * Whether to check last valid insertion date for KO attributes.
     */
    @DeviceProperty(description = "Whether to check last valid insertion date for KO attributes")
    private boolean checkLastValidDate = DEFAULT_CHECK_LAST_VALID;

    // ////////// //
    // Attributes //
    // ////////// //

    /**
     * The state of the device
     */
    @State
    private DeviceState state = DeviceState.INIT;

    /**
     * The status of the device
     */
    @Status
    private String status = "Init on progress";

    private String[] exportedArchivers = new String[] {};

    /**
     * Set the state of the device
     *
     * @param state
     */
    public final void setState(final DeviceState state) {
        this.state = state;
    }

    /**
     * Set the status of the device
     *
     * @param status
     */
    public final void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Get the state of the device
     *
     * <ul>
     * <li>ALARM: If the threshold is exceeded, then the device take the alarm state to indicate there is an important
     * problem.</li>
     * <li>OFF: When the device is normally start but the first cycle of check is not yet finished. This state indicates
     * that not all attributes have been checked and so the report can be incomplete.</li>
     * <li>ON: It is the default state of the device. That means the device work nice and there is more than one cycle
     * to done.</li>
     * <li>FAULT: In case of communication problems with device archiver or with the data base, the device will be in
     * FAULT state.</li>
     * </ul>
     *
     * @return the state of the device
     */
    public final DeviceState getState() {
        refreshStateAndStatus();
        return state;
    }

    /**
     * Update the state and the status of the device according to the number of KO attributes and differents threads
     * errors.
     */
    private void refreshStateAndStatus() {
        final StringBuilder statusBuilder = new StringBuilder();
        if (cycleManager == null) {
            state = DeviceState.FAULT;
            statusBuilder.append(DATABASE_ERROR);
            final String error = lastConnectionError;
            if (error != null && !error.isEmpty()) {
                statusBuilder.append("\n").append(error);
            }
            final long startTime = cycleThread.getLastLoopStartTime();
            final long stopTime = cycleThread.getLastLoopStopTime();
            if (stopTime < startTime) {
                statusBuilder.append("Still trying to reconnect to the database.");
            } else {
                DateUtil.elapsedTimeToStringBuilder(statusBuilder.append("\nNext connection attempt in "),
                        stopTime + cycleThread.getCyclePause() - System.currentTimeMillis(), false);
            }
        } else {
            final long nullAttributes = cycleManager.getResult().getNumberOfNullAttributes();
            final long koAttributes = cycleManager.getResult().getNumberOfKoAttributes() + nullAttributes;
            final long cycleStartTime = cycleManager.getCycleStartTime();
            final long cycleStopTime = cycleManager.getCycleStopTime();
            final Map<String, String> cycleManagerErrorMap = new HashMap<String, String>();
            cycleManagerErrorMap.putAll(cycleManager.getErrors());
            final Map<String, String> diffReportErrorMap = new HashMap<String, String>();
            if (diffReportRunnable != null) {
                diffReportErrorMap.putAll(diffReportRunnable.getErrors());
            }
            if (koAttributes - nullAttributes >= notificationThreshold) {
                state = DeviceState.ALARM;
                statusBuilder.append("Too many KO ").append(koAttributes).append(" - max not null allowed is ")
                        .append(notificationThreshold).append("\n");
            } else if (archiverFailed) {
                state = DeviceState.FAULT;
                statusBuilder.append("Unable to select archivers.");
            } else if (!cycleManagerErrorMap.isEmpty()) {
                state = DeviceState.FAULT;
                appendErrorsToStringBuilder(cycleManagerErrorMap, "cycle manager", statusBuilder);
            } else if (!diffReportErrorMap.isEmpty()) {
                state = DeviceState.FAULT;
                appendErrorsToStringBuilder(diffReportErrorMap, "diffReport", statusBuilder);
            } else if (cycleManager.getNbrEndCycle() < 1) {
                state = DeviceState.OFF;
                statusBuilder.append("Init done, waiting for the first complete cycle.\n");
            } else {
                state = DeviceState.ON;
            }
            cycleManagerErrorMap.clear();
            diffReportErrorMap.clear();

            if (stateResolver != null) {
                final DevState archiversState = stateResolver.getState();
                statusBuilder.append("At least one archiver is ").append(archiversState);
                if (archiversState.equals(DevState.FAULT) || archiversState.equals(DevState.UNKNOWN)) {
                    state = DeviceState.FAULT;
                }
            }
            statusBuilder.append("\n").append(koAttributes).append(" KO attributes wherein ").append(nullAttributes)
                    .append(" NULL");
            final int checked = cycleManager.getNbrAttrMonitoredCurrentCycle();
            final int total = attributeSelection.getNbrAttributesToMonitor();
            final int percent = checked * 100 / total;
            statusBuilder.append("\n").append(percent).append("% checked (").append(checked).append("/").append(total)
                    .append(")");
            final long elapsedTime = cycleStopTime == 0 ? System.currentTimeMillis() - cycleStartTime
                    : cycleStopTime - cycleStartTime;
            DateUtil.elapsedTimeToStringBuilder(statusBuilder.append(" - duration: "), elapsedTime, false);
            statusBuilder.append(" - cycle started at ")
                    .append(SIMPLE_DATE_FORMAT.get().format(new Date(cycleStartTime)));
            if (cycleStopTime > cycleStartTime) {
                DateUtil.elapsedTimeToStringBuilder(statusBuilder.append(" - Next attributes check in "),
                        cycleStopTime + cycleThread.getCyclePause() - System.currentTimeMillis(), false);
            }
        }
        status = statusBuilder.toString();
    }

    protected StringBuilder appendErrorsToStringBuilder(final Map<String, String> errorMap, final String threadName,
            final StringBuilder statusBuilder) {
        statusBuilder.append(ERROR_SEPARATOR);
        final int count = errorMap.size();
        if (count > 1) {
            statusBuilder.append("There were ").append(count).append(" errors");
        } else {
            statusBuilder.append("There was ").append(count).append(" error");
        }
        statusBuilder.append(" in ").append(threadName).append(" thread:\n");
        int i = 1;
        for (final Entry<String, String> entry : errorMap.entrySet()) {
            statusBuilder.append("\n** Error ").append(i++).append(":\n").append(entry.getKey()).append(": ")
                    .append(entry.getValue()).append("\n");
        }
        statusBuilder.append(ERROR_SEPARATOR);
        return statusBuilder;
    }

    //
    // public final Result getResult() {
    // return cycleManager.getResult();
    // }

    /**
     * Get the status of the device
     *
     * @return the status of the device
     */
    public final String getStatus() {
        refreshStateAndStatus();
        return status;
    }

    /**
     * Set the dbSchema device property. It use for connect to the database.
     *
     * @param dbSchema
     */
    // public final void setDbSchema(final String dbSchema) {
    // this.dbSchema = dbSchema;
    // }

    /**
     * Set the doDiagnosis device property.
     *
     * @param doDiagnosis
     */
    public final void setDoDiagnosis(final boolean doDiagnosis) {
        this.doDiagnosis = doDiagnosis;
    }

    /**
     * Set the doArchiverDiagnosis device property.
     *
     * @param doArchiverDiagnosis
     */
    public final void setDoArchiverDiagnosis(final boolean doArchiverDiagnosis) {
        this.doArchiverDiagnosis = doArchiverDiagnosis;
    }

    /**
     * Set the doRetry device property
     *
     * @param doRetry
     */
    public final void setDoRetry(final boolean doRetry) {
        this.doRetry = doRetry;
    }

    /**
     * Set the doDiffReport device property
     *
     * @param doDiffReport
     */
    public final void setDoDiffReport(final boolean doDiffReport) {
        this.doDiffReport = doDiffReport;
    }

    /**
     * Set the excludeList device property
     *
     * @param excludeList
     */
    public final void setExcludeList(final String[] excludeList) {
        this.excludeList = getTrimmedProperty(excludeList);
    }

    /**
     * Set the dbHost device property
     *
     * @param dbHost
     */
    public final void setDbHost(final String dbHost) {
        this.dbHost = dbHost;
    }

    /**
     * Set the dbName device property
     *
     * @param dbName
     */
    public final void setDbName(final String dbName) {
        this.dbName = dbName;
    }

    /**
     * Set the dbPwd device property
     *
     * @param dbPwd
     */
    public final void setDbPwd(final String dbPwd) {
        this.dbPwd = dbPwd;
    }

    /**
     * Set the dbRacConnection device property
     *
     * @param dbRacConnection
     */
    // public final void setDbRacConnection(final Boolean dbRacConnection) {
    // this.dbRacConnection = dbRacConnection;
    // }

    /**
     * Set the dbUser device property
     *
     * @param dbUser
     */
    public final void setDbUser(final String dbUser) {
        this.dbUser = dbUser;
    }

    /**
     * Set the gapDuration device property
     *
     * @param gapDuration
     */
    public final void setGapDuration(final int gapDuration) {
        this.gapDuration = gapDuration;
    }

    /**
     * Sets the notificationDevice property
     *
     * @param notificationDevice
     */
    public void setNotificationDevice(final String notificationDevice) {
        this.notificationDevice = notificationDevice;
    }

    /**
     * Set the notificationContact device property
     *
     * @param notificationContact
     */
    public final void setNotificationContact(String[] notificationContact) {
        notificationContact = getTrimmedProperty(notificationContact);
        if (isValidProperty(notificationContact, -1, false)) {
            this.notificationContact = notificationContact;
        }
    }

    /**
     * Set the notificationThreshold device property
     *
     * @param notificationThreshold
     */
    public final void setNotificationThreshold(final long notificationThreshold) {
        this.notificationThreshold = notificationThreshold;
    }

    /**
     * Set the onlyIncludeList device property
     *
     * @param onlyIncludeList
     */
    public final void setOnlyIncludeList(final String[] onlyIncludeList) {
        this.onlyIncludeList = getTrimmedProperty(onlyIncludeList);
    }

    /**
     * Set the periodicReport device property
     *
     * @param periodicReport
     */
    public final void setPeriodicReport(final String periodicReport) {
        this.periodicReport = periodicReport;
    }

    /**
     * Set the safetyPeriod device property
     *
     * @param safetyPeriod
     */
    public final void setSafetyPeriod(final String safetyPeriod) {
        this.safetyPeriod = safetyPeriod;
    }

    /**
     * Set the unitaryPause device property
     *
     * @param unitaryPause
     */
    public final void setUnitaryPause(final long unitaryPause) {
        this.unitaryPause = unitaryPause;
    }

    /**
     * Set the cyclePause device property
     *
     * @param cyclePause
     */
    public final void setCyclePause(final int cyclePause) {
        this.cyclePause = cyclePause;
    }

    /**
     * Set the realCheck device property
     *
     * @param realCheck
     */
    public final void setRealCheck(final boolean realCheck) {
        this.realCheck = realCheck;
    }

    public final void setTextTalkerLanguage(final String textTalkerLanguage) {
        this.textTalkerLanguage = textTalkerLanguage;
    }

    /**
     * Sets the archiverClass device property
     *
     * @param archiverClass The device property to set
     */
    public void setArchiverClass(final String archiverClass) {
        this.archiverClass = archiverClass.trim();
    }

    /**
     * Sets the checkLastValidDate device property
     *
     * @param checkLastValidDate The device property to set
     */
    public void setCheckLastValidDate(final boolean checkLastValidDate) {
        this.checkLastValidDate = checkLastValidDate;
    }

    private boolean isValidProperty(final String[] property, final int expectedLength, final boolean avoidEmpty) {
        boolean valid;
        if (property == null || expectedLength > -1 && property.length != expectedLength) {
            valid = false;
        } else {
            valid = true;
            for (final String prop : property) {
                if (prop == null) {
                    valid = false;
                    break;
                } else if (avoidEmpty && prop.trim().isEmpty()) {
                    valid = false;
                    break;
                }
            }
        }
        return valid;
    }

    private String[] getTrimmedProperty(final String... property) {
        String[] result;
        List<String> tmpList;
        if (property == null) {
            tmpList = new ArrayList<String>();
        } else {
            tmpList = new ArrayList<String>(property.length);
            for (final String prop : property) {
                if (prop != null) {
                    final String tmpProp = prop.trim();
                    if (!tmpProp.isEmpty()) {
                        tmpList.add(tmpProp);
                    }
                }
            }
        }
        result = tmpList.toArray(new String[tmpList.size()]);
        tmpList.clear();
        return result;
    }

    /**
     * Get the value of the device attribute Report
     *
     * @return a global readeable report of the state of the archiving (ie : KO
     *         attributes)
     */
    @Attribute
    public final String[][] getReport() {
        xlogger.entry();
        String[][] argout = NO_KO_ATTRIBUTES;
        if (cycleManager != null) {
            final Result result = cycleManager.getResult();
            if (result != null) {
                argout = result.getSimpleGlobalReport();
                if (argout.length == 0) {
                    argout = NO_KO_ATTRIBUTES;
                }
            }
        }
        xlogger.exit();
        return argout;
    }

    /**
     * Get the value of the device attribute PerDomainReport
     *
     * @return a readeable report of the state of the archiving sort per domain
     */
    @Attribute
    public final String[][] getPerDomainReport() {
        xlogger.entry();
        String[][] argout = NO_KO_ATTRIBUTES;
        if (cycleManager != null) {
            final Result result = cycleManager.getResult();
            if (result != null) {
                argout = result.getSimpleReportPerDomain();
                if (argout.length == 0) {
                    argout = NO_KO_ATTRIBUTES;
                }
            }
        }
        xlogger.exit();
        return argout;
    }

    /**
     * Get the value of the device attribute PerAttributeReport This report
     * contains informations about the diagnosis of KO attribute
     *
     * @return a readable report of the state of the archiving sort per
     *         attribute
     */
    @Attribute
    public final String[][] getPerAttributeReport() {
        xlogger.entry();
        String[][] argout = NO_KO_ATTRIBUTES;
        if (cycleManager != null) {
            final Result result = cycleManager.getResult();
            if (result != null) {
                argout = result.getSimpleReportPerAttribute(sortedResultByType);
                if (argout.length == 0) {
                    argout = NO_KO_ATTRIBUTES;
                }
            }
        }
        xlogger.exit();
        return argout;
    }

    /**
     * Get the value of the device attribute PerArchiverReport
     *
     * @return a readeable report of the state of the archiving sort per
     *         archiver
     */
    @Attribute
    public final String[][] getPerArchiverReport() {
        xlogger.entry();
        String[][] argout = NO_KO_ATTRIBUTES;
        if (cycleManager != null) {
            final Result result = cycleManager.getResult();
            if (result != null) {
                argout = result.getSimpleReportPerArchiver();
                if (argout.length == 0) {
                    argout = NO_KO_ATTRIBUTES;
                }
            }
        }
        xlogger.exit();
        return argout;
    }

    /**
     * Get the value of the device attribute PerFamilyReport
     *
     * @return a readeable report of the state of the archiving sort per family
     */
    @Attribute
    public final String[][] getPerFamilyReport() {
        xlogger.entry();
        String[][] argout = NO_KO_ATTRIBUTES;
        if (cycleManager != null) {
            final Result result = cycleManager.getResult();
            if (result != null) {
                argout = result.getSimpleReportPerFamily();
                if (argout.length == 0) {
                    argout = NO_KO_ATTRIBUTES;
                }
            }
        }
        xlogger.exit();
        return argout;
    }

    /**
     * Get the value of the device attribute koAttributes
     *
     * @return the number of ko attributes
     */
    @Attribute
    @AttributeProperties(label = "KO", unit = "attributes", description = "number of KO attributes (includes NULL)")
    public final int getKoAttributes() {
        xlogger.entry();
        int ko = 0;
        if (cycleManager != null) {
            final Result result = cycleManager.getResult();
            ko = result.getNumberOfKoAttributes() + result.getNumberOfNullAttributes();
        }
        xlogger.exit();
        return ko;
    }

    /**
     * Get the value of the device attribute notNullAttributes
     *
     * @return the number of not null attributes
     */
    @Attribute
    @AttributeProperties(label = "Not NULL", unit = "attributes", description = "number of not NULL attributes")
    public final int getNotNullAttributes() {
        xlogger.entry();
        int nullNr = 0;
        if (cycleManager != null) {
            final Result result = cycleManager.getResult();
            nullNr = result.getNumberOfKoAttributes();
        }
        xlogger.exit();
        return nullNr;
    }

    /**
     * Get the value of the device attribute nullAttributes
     *
     * @return the number of null attributes
     */
    @Attribute
    @AttributeProperties(label = "NULL", unit = "attributes", description = "number of NULL attributes")
    public final int getNullAttributes() {
        xlogger.entry();
        int nullNr = 0;
        if (cycleManager != null) {
            final Result result = cycleManager.getResult();
            nullNr = result.getNumberOfNullAttributes();
        }
        xlogger.exit();
        return nullNr;
    }

    /**
     * Get monitored attributes koAttributes
     *
     * @return the number of monitored attributes
     */
    @Attribute
    @AttributeProperties(label = "Watched", unit = "attributes", description = "number of wacthed attributes")
    public final int getNrWatchedAttributes() {
        xlogger.entry();
        int nr = 0;
        if (cycleManager != null) {
            nr = attributeSelection.getNbrAttributesToMonitor();
        }
        xlogger.exit();
        return nr;
    }

    /**
     * Get the value of the device attribute Version
     *
     * @return the version of the device
     */
    @Attribute(displayLevel = DispLevel._EXPERT)
    public final String getVersion() {
        return version;
    }

    /**
     * Get the value of the device attribute WatchAttributes
     *
     * @return a list of attributes monitored by this device
     */
    @Attribute
    public final String[] getWatchAttributes() throws DevFailed {
        xlogger.entry();
        String[] argout;
        if (cycleManager == null) {
            argout = new String[0];
        } else {
            final Set<String> resList = cycleManager.getMonitoredAttributes();
            argout = new String[resList.size()];
            argout = resList.toArray(argout);
        }
        xlogger.exit();
        return argout;
    }

    /**
     * Get the value of the device attribute ArchivedAttribute
     *
     * @return a list of all the attributes archived in the connected database
     */
    @Attribute
    public final String[] getArchivedAttributes() throws DevFailed {
        xlogger.entry();
        String[] argout;
        if (cycleManager == null) {
            argout = new String[0];
        } else {
            argout = cycleManager.getAllAttributes();
        }
        xlogger.exit();
        return argout;
    }

    /**
     * Get the value of the device attribute DiffReport
     * <ul>
     * <li>Start</li>
     * <li>Stop (=no restart)</li>
     * <li>Restart without configuration changes</li>
     * <li>Restart with configuration changes</li>
     * </ul>
     *
     * @return a report of the archiving configuration changes
     */
    @Attribute
    public final String[][] getDiffReport() {
        xlogger.entry();
        String[][] argout;
        if (doDiffReport) {
            if (diffReport == null) {
                argout = new String[][] { { "Report in progress" } };
            } else {
                argout = new String[diffReport.length][];
                for (int i = 0; i < diffReport.length; i++) {
                    argout[i] = Arrays.copyOf(diffReport[i], diffReport[i].length);
                }
            }
        } else {
            argout = new String[][] { { "Is disable" } };
        }
        xlogger.exit();
        return argout;
    }

    /**
     * Get the value of the device attribute ArchiversStates
     *
     * @return the list of the archiver state using by monitored attributes
     */
    @Attribute
    public final String[] getArchiversStates() throws DevFailed {
        xlogger.entry();
        String[] argout;
        if (stateResolver == null) {
            argout = new String[0];
        } else {
            argout = stateResolver.getDeviceStateArray();
            Arrays.sort(argout);
        }
        xlogger.exit();
        return argout;
    }

    private AbstractDataBaseConnector connectArchivingDb() throws DevFailed {
        final DataBaseParameters params = new DataBaseParameters();
        params.setParametersFromTango(archiverClass);
        if (!dbUser.isEmpty()) {
            params.setUser(dbUser);
        }
        if (!dbPwd.isEmpty()) {
            params.setPassword(dbPwd);
        }
        // if (dbRacConnection != null) {
        // params.setRac(dbRacConnection);
        // }
        // if (!dbSchema.isEmpty()) {
        // params.setSchema(dbSchema);
        // }
        logger.info("init connection to archiving DB with params: {}", params);
        AbstractDataBaseConnector connector;
        try {
            connector = ConnectionFactory.connect(params);
            dbReader = new DBReader(connector);
            dbReader.openConnectionIfNecessary();
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
        return connector;
    }

    private void reconnect() throws DevFailed {
        stopAll();
        connectArchivingDb();
        startWatching();
    }

    /**
     * Initialize the device
     *
     * @throws DevFailed
     */
    @Init
    public final void initDevice() throws DevFailed {
        // reactivate following code to test PROBLEM-1014
        // long start = System.currentTimeMillis();
        xlogger.entry();
        dynamicManager.addAttribute(new LogAttribute(DEPTH_LOG_ATTRIBUTE, logger));
        logger.info("start init");
        checkProperties();
        // reactivate following code to test PROBLEM-1014
        // long startConnect = System.currentTimeMillis();
        connectArchivingDb();
        // reactivate following code to test PROBLEM-1014
        // long startWatch = System.currentTimeMillis();
        startWatching();
        state = DeviceState.OFF;
        status = "Init done, waiting for the first complete cycle.";
        logger.info("init OK");
        xlogger.exit();
        // reactivate following code to test PROBLEM-1014
        // long end = System.currentTimeMillis();
        // StringBuilder infoBuilder = new StringBuilder("************\nIt took:");
        // DateUtil.elapsedTimeToStringBuilder(infoBuilder.append("\n- "), start - globalStart)
        // .append(" to reach initialization");
        // DateUtil.elapsedTimeToStringBuilder(infoBuilder.append("\n- "), startConnect - start)
        // .append(" to ckeck properties");
        // DateUtil.elapsedTimeToStringBuilder(infoBuilder.append("\n- "), startWatch - startConnect)
        // .append(" to connect to database");
        // DateUtil.elapsedTimeToStringBuilder(infoBuilder.append("\n- "), end - startWatch)
        // .append(" to start monitoring");
        // DateUtil.elapsedTimeToStringBuilder(infoBuilder.append("\n- "), end - start)
        // .append(" to perform device initialization\n***********");
        // System.out.println(infoBuilder);
    }

    private void updateLastConnectionError(final Exception e) {
        String error;
        if (e == null) {
            error = null;
        } else if (e instanceof DevFailed) {
            error = DevFailedUtils.toString((DevFailed) e);
            if (error != null) {
                error = error.trim();
            }
        } else {
            error = e.getMessage();
        }
        if (error != null) {
            error = error.trim();
        }
        lastConnectionError = error;
    }

    private void startWatching() throws DevFailed {

        attributeSelection = new AttributeSelection(dbReader, onlyIncludeRegex, propertyToRegExList(excludeList),
                attrsDataFormat, attrsType, attrsWriteType);
        attributeSelection.compute();
        logger.info("will monitor {} attributes over {}", attributeSelection.getNbrAttributesToMonitor(),
                dbReader.getNumberOfArchivedAttributes());
        if (!realCheck) {
            logger.info("init cycleManager from ODA informations");
            cycleManager = new CycleManagerFromDb(dbReader, doDiagnosis, doArchiverDiagnosis, doRetry,
                    attributeSelection, safetyDuration, checkLastValidDate);
        } else {
            logger.info("init cycleManager with real check");
            cycleManager = new CycleManager(dbReader, unitaryPause, doDiagnosis, doArchiverDiagnosis, doRetry,
                    attributeSelection, safetyDuration, checkLastValidDate);
        }
        // start watch cycles
        cycleThread = new CycleManagerThread(cyclePause);
        cycleThread.start();

        if (periodicRecipient.size() != 0 || thresholdRecipient.size() != 0 || talkerRecipient.size() != 0) {
            logger.info("Notification service enabled ");
            try {
                final NotificationRunnable nr = new NotificationRunnable(cycleManager, periodicRecipient,
                        thresholdRecipient, talkerRecipient, notificationThreshold, notificationDevice,
                        textTalkerLocale);
                notificationThread = new Thread(nr);
                notificationThread.setName("Watcher - Notification");
                notificationThread.start();
            } catch (final DevFailed e) {
                DevFailedUtils.logDevFailed(e, logger);
            }
        } else {
            logger.info("Notification service disabled ");
        }

        if (doDiffReport) {
            logger.info("init diff report");
            executor = Executors.newScheduledThreadPool(1);
            diffReportRunnable = new DiffReportRunnable(periodicDuration);
            future = executor.scheduleAtFixedRate(diffReportRunnable, INITIAL_DELAY_FOR_REPORT, periodicDuration,
                    TimeUnit.MILLISECONDS);
        }

        stateResolver = new StateResolver(5000, false);
        // stateResolver.putStatePriority(DevState.DISABLE, 10);

        exportedArchivers = ApiUtil.get_db_obj().get_device_name("*", archiverClass);
        logger.info("watching states of {} archivers", exportedArchivers.length);
        stateResolver.setMonitoredDevices(3000, exportedArchivers);
        stateResolver.start();
    }

    /**
     * Delete the device. Interrupt all thread and clean all dynamic object.
     *
     * @throws DevFailed
     */
    @Delete
    public final void deleteDevice() throws DevFailed {
        xlogger.entry();
        stopAll();
        dbUser = "";
        dbPwd = "";
        // dbRacConnection = null;
        // dbSchema = "";
        dynamicManager.clearAll();
        xlogger.exit();
    }

    private void stopAll() {
        logger.info("Cycle thread stop");
        if (cycleThread != null) {
            cycleThread.interrupt();
            cycleThread = null;
        }
        logger.info("notification thread stop");
        if (notificationThread != null) {
            notificationThread.interrupt();
            notificationThread = null;
        }

        logger.info("rest cycleManager");
        if (cycleManager != null) {
            cycleManager = null;
        }
        logger.info("future cancel");
        if (future != null) {
            future.cancel(true);
        }
        logger.info("executor shutdown");
        if (executor != null) {
            executor.shutdown();
        }
        logger.info("State resolver stop");
        if (stateResolver != null) {
            stateResolver.stop();
            stateResolver = null;
        }
    }

    /**
     * Send command RetryForKOAttributes and forget to all archivers
     *
     * @throws DevFailed
     */
    @Command
    public void retryForKOAttributes() throws DevFailed {
        final Group group = new Group("");
        group.add(exportedArchivers);
        group.command_inout_asynch("RetryForKOAttributes", true, true);
    }

    /**
     * Check the archiving of an attribute
     *
     * @param attributeName
     * @return the archiving status
     * @throws DevFailed
     */
    @Command
    public String checkArchivingForAttribute(final String attributeName) throws DevFailed {
        final StringBuilder toReturn = new StringBuilder();
        if (cycleManager != null && cycleManager instanceof CycleManager) {
            toReturn.append(((CycleManager) cycleManager).checkArchivingForAttribute(attributeName).toString());
            toReturn.append("\nLast insert is: ").append(dbReader.getTimeOfLastInsert(attributeName).toString());
        }
        return toReturn.toString();
    }

    /**
     * Get ID and Archiver of an archived attribute
     *
     * @param attributeName
     * @return
     * @throws DevFailed
     */
    @Command
    public String getArchiverAndIdForAttribute(final String attributeName) throws DevFailed {
        final StringBuilder toReturn = new StringBuilder();
        final String attributeNameL = attributeName.toLowerCase();
        if (dbReader != null) {
            toReturn.append("id: ").append(Integer.toString(dbReader.getAttributeId(attributeNameL)));
            final Map<String, ModeData> modes = dbReader.getModeDataForAll();
            for (final Entry<String, ModeData> entry : modes.entrySet()) {
                if (entry.getKey().equalsIgnoreCase(attributeNameL)) {
                    toReturn.append("\narchiver: ").append(entry.getValue().getArchiver());
                    toReturn.append("\n").append(entry.getValue().getMode());
                    break;
                }
            }

        }

        return toReturn.toString();
    }

    /**
     * Execute command "GetAllArchivingAttributes" on device. Lists all HDB
     * archiving attributes (does a new request regardless of the watcher
     * cycle).
     *
     * @return The list of all archiving attributes.
     * @throws DevFailed
     */
    @Command(outTypeDesc = "A string array of all archiving attributes")
    public final String[] getAllArchivingAttributes() throws DevFailed {
        xlogger.entry();
        final Map<String, ModeData> resTable = dbReader.getArchivedAttributes();
        final Map<String, Mode> finalMap = new HashMap<String, Mode>();
        String[] argout = new String[resTable.size()];

        for (final String attr : resTable.keySet()) {
            final Mode mode = dbReader.getModeForAttribute(attr);
            finalMap.put(attr, mode);
        }

        argout = resTable.keySet().toArray(argout);
        xlogger.exit();
        return argout;
    }

    /**
     * Execute command "GetErrorArchiversCurrent" on device. Lists the archivers
     * that had at least one attribute incorrectly archived during the current
     * control cycle. Partially refreshed every time an attribute is controlled.
     *
     * @return The list of archivers that have at least one KO attribute.
     */
    @Command(outTypeDesc = "A string array wich represent the archiver and its KO attributes")
    public final String[] getErrorArchiversCurrent() {
        xlogger.entry();
        String[] argout;
        if (cycleManager == null) {
            argout = new String[0];
        } else {
            final Result result = cycleManager.getResult();
            final Map<String, Archiver> errorArchiver = result.getKoArchivers();
            argout = new String[errorArchiver.size()];
            argout = errorArchiver.keySet().toArray(argout);
        }
        xlogger.exit();
        return argout;
    }

    /**
     * Execute command "getErrorForRegex" on device. Lists the attributes that
     * are incorrectly archived during the current control cycle and that
     * corresponds to the argin regex
     *
     * @param argin
     *            a regex
     *
     * @return The list of KO attributes that correspond to the argin regex
     * @throws DevFailed
     */
    @Command(inTypeDesc = "An attribute regex ", outTypeDesc = "An array of all the KO attributes corresponding to the argin regex")
    public final String[] getErrorForRegex(final String regex) throws DevFailed {
        xlogger.entry();
        final List<String> tmp = new ArrayList<String>();
        final Map<String, ModeData> resTable = dbReader.getArchivedAttributes();
        for (final String attr : resTable.keySet()) {
            if (attr.matches(formatToGoodRegex(regex))) {
                tmp.add(attr);
            }
        }
        String[] argout = new String[tmp.size()];
        argout = tmp.toArray(argout);
        xlogger.exit();
        return argout;
    }

    /**
     * Check all the attributes of the archiving in accordance to
     * "onlyIncludeMist" and "excludeList" properties to find KO. This command
     * does a single request to the data base.
     *
     * @throws DevFailed
     */
    @Command(displayLevel = DispLevel._EXPERT, outTypeDesc = "The report of the check of monitored attributes")
    public final String[] checkAll() throws DevFailed {
        xlogger.entry();
		final Map<String, Boolean> attrsAreOk = dbReader.checkAllAttributes(safetyDuration);
        final Result result = new Result(doArchiverDiagnosis);
        final Diagnosis diagnosis = new Diagnosis(dbReader, new Result(true), checkLastValidDate);
        for (final Entry<String, Boolean> e : attrsAreOk.entrySet()) {
            if (!e.getValue()) {
                final String attrName = e.getKey();
                final ArchivedAttribute aa = new ArchivedAttribute(attrName);
                final ModeData md = dbReader.getModeDataForAttribute(attrName);
                aa.setArchiver(md.getArchiver());
                final DiagnosisInfo diagnos = diagnosis.diagnose(aa, null, checkLastValidDate);
                aa.setDiagnosis(diagnos);
                aa.setId(dbReader.getAttributeId(attrName));
                result.addKoAttribute(aa);
            }
        }
        final String[][] tmp = result.getSimpleReportPerAttribute(sortedResultByType);
        final LinkedList<String> list = new LinkedList<String>();
        for (int i = 0; i < tmp.length; i++) {
            for (int j = 0; j < tmp[i].length; j++) {
                list.add(tmp[i][j]);
            }
        }
        xlogger.exit();
        return list.toArray(new String[list.size()]);
    }

    /**
     * Execute command "IsAttributeCorrectlyArchivedCurrent" on device. Returns
     * whether the specified attribute was correctly archiving during the latest
     * check.
     *
     * @param argin
     *            The attribute complete name
     * @return True if archiving works correctly for this attribute
     * @throws DevFailed
     */
    @Command(inTypeDesc = "The attribute complete name", outTypeDesc = "True if archiving works correctly for this attribute, false if not")
    public final boolean isAttributeCorrectlyArchivedCurrent(final String attribute) throws DevFailed {
        xlogger.entry();
        boolean archived;
        if (cycleManager == null) {
            archived = false;
        } else {
            final Result result = cycleManager.getResult();
            if (!result.isKOAttribute(attribute)) {
                final StringBuilder error = new StringBuilder();
                error.append("The attribute \"").append(attribute).append("\" is not monitored by this watcher.");
				throw DevFailedUtils.newDevFailed(error.toString());
            }
            archived = !cycleManager.getResult().isKOAttribute(attribute);
        }
        xlogger.exit();
        return archived;
    }

    /**
     * Get a archiving configuration changes report between two dates
     * <ul>
     * <li>Start</li>
     * <li>Stop (=no restart)</li>
     * <li>Restart without configuration changes</li>
     * <li>Restart with configuration changes</li>
     * </ul>
     *
     * @return a report of the archiving configuration changes
     *
     * @param dates
     *            A String [2] :
     *            <ul>
     *            <li>String[0] : start date (Format : yyyy-MM-dd HH:mm:ss)</li>
     *            <li>String[1] : stop date (Format : yyyy-MM-dd HH:mm:ss)</li>
     *            </ul>
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = "String array with two date. String[0] = start date, String[1] = stop date.\n Date format : yyyy-MM-dd HH:mm:ss ", outTypeDesc = "The difference report :\nNewly attribute start\nattribut start with no conf changed\nattribute definitely stopped\nattribute conf changed")
    public final String[] differenceBetween(final String[] dates) throws DevFailed {
        xlogger.entry();
        final StringBuilder errorBuilder = new StringBuilder();
        if (dates.length != 2) {
            errorBuilder.append("Only 2 parameters needed.\n");
        }
        try {
            SIMPLE_DATE_FORMAT.get().parse(dates[0]);
        } catch (final ParseException e) {
            errorBuilder.append("The date : \"").append(dates[0]).append("\" is not correct.\n")
                    .append("Good format is : ").append(SIMPLE_DATE_FORMAT.get().toPattern()).append("\n");
        }
        try {
            SIMPLE_DATE_FORMAT.get().parse(dates[1]);
        } catch (final ParseException e) {
            errorBuilder.append("The date : \"").append(dates[1]).append("\" is not correct.\n")
                    .append("Good format is : ").append(SIMPLE_DATE_FORMAT.get().toPattern()).append("\n");
        }
        if (errorBuilder.length() != 0) {
			throw DevFailedUtils.newDevFailed(errorBuilder.toString());
        }

        // new attr
        final List<String> newAttrs = dbReader.newEntryADTBetween(dates[0], dates[1]);
        // attr start
        final Map<String, Integer> attrStart = dbReader.attrStartBetween(dates[0], dates[1]);
        // attr stopped
        final Map<String, Integer> attrStop = dbReader.attrStopBetween(dates[0], dates[1]);
        // attr conf changed
        final Map<String, Integer> attrConfChanged = dbReader.attrConfChangeBetween(dates[0], dates[1]);

        for (final Entry<String, Integer> e : attrConfChanged.entrySet()) {
            final Integer v = attrStart.get(e.getKey());
            if (v != null) {
                final int valueTmp = v - e.getValue();
                if (valueTmp > 0) {
                    attrStart.put(e.getKey(), valueTmp);
                } else {
                    attrStart.remove(e.getKey());
                }
            }
        }

        final int globalSize = newAttrs.size() + attrStart.size() * 2 + attrStop.size() * 2 + attrConfChanged.size() * 2
                + NUMBER_LEGENDS_DIFF_REPORT;
        final String[] result = new String[globalSize];
        int i = 0;
        result[i++] = "Attribute started :";
        for (final String newAttr : newAttrs) {
            result[i++] = newAttr;
        }
        result[i++] = "";
        result[i++] = "Attribute restarted whithout configuration change:";
        for (final Entry<String, Integer> e : attrStart.entrySet()) {
            final StringBuilder sb = new StringBuilder(STR_RESULT_PRES);
            result[i++] = e.getKey();
            result[i++] = sb.append(e.getValue().toString()).toString();
        }
        result[i++] = "";
        result[i++] = "Attribute definitively stopped :";
        for (final Entry<String, Integer> e : attrStop.entrySet()) {
            final StringBuilder sb = new StringBuilder(STR_RESULT_PRES);
            result[i++] = e.getKey();
            result[i++] = sb.append(e.getValue().toString()).toString();
        }
        result[i++] = "";
        result[i++] = "Attribute configuration changed  :";
        for (final Entry<String, Integer> e : attrConfChanged.entrySet()) {
            final StringBuilder sb = new StringBuilder(STR_RESULT_PRES);
            result[i++] = e.getKey();
            result[i++] = sb.append(e.getValue().toString()).toString();
        }
        xlogger.exit();
        return result;
    }

    /**
     * Get the different gap for an attribute.
     *
     *
     * @param attr
     * @return String[] representing the different gap of the attribute
     * @throws DevFailed
     */
    @Command(inTypeDesc = "String array with at least 1 parameter.\nString[0] = attribute name\n String[1] = gap duration | start date.\nString[2] = start date | stop date.\nString[3] = nothing | stop date.\n Date format : yyyy-MM-dd HH:mm:ss ", outTypeDesc = "A list of (start_date,stop_date) of gaps")
    public final String[] getGapForAttribute(final String[] attrs) throws DevFailed {
        xlogger.entry();
        boolean isDateStart = false;
        if (attrs.length > 4) {
			throw DevFailedUtils.newDevFailed("Too much arguments.");
        }
        final String attributeName = attrs[0];
        String startDate = null;
        String stopDate = SIMPLE_DATE_FORMAT.get().format(new Timestamp(System.currentTimeMillis()));
        if (attrs.length > 1) {
            try {
                gapDuration = Integer.parseInt(attrs[1]);
            } catch (final NumberFormatException e) {
                try {
                    SIMPLE_DATE_FORMAT.get().parse(attrs[1]);
                    startDate = attrs[1];
                    isDateStart = true;
                } catch (final ParseException e1) {
					throw DevFailedUtils.newDevFailed("The second argument is not a number nor a date !");
                }
            }
        }
        if (attrs.length > 2) {
            if (!isDateStart) {
                try {
                    SIMPLE_DATE_FORMAT.get().parse(attrs[2]);
                } catch (final ParseException e1) {
					throw DevFailedUtils.newDevFailed("The third argument is not a date !");
                }
                startDate = attrs[2];
            } else {
                try {
                    SIMPLE_DATE_FORMAT.get().parse(attrs[2]);
                } catch (final ParseException e1) {
					throw DevFailedUtils.newDevFailed("The third argument is not a date !");
                }
                stopDate = attrs[2];
            }
        }
        if (attrs.length > 3) {
            if (isDateStart) {
				throw DevFailedUtils
						.newDevFailed("You have already gave two dates before, the fourth argument is useless");
            }
            try {
                SIMPLE_DATE_FORMAT.get().parse(attrs[3]);
            } catch (final ParseException e1) {
				throw DevFailedUtils.newDevFailed("The fourth argument is not a date !");
            }
            stopDate = attrs[3];
        }

        logger.info("Start dbReader gap");
        final Map<String, String> mapRes = dbReader.getGaps(attributeName, gapDuration, startDate, stopDate);

        logger.info("Found {} gap(s) for attribute : {} ", mapRes.size(), attributeName);
        final List<String> listTmp = new ArrayList<String>();
        listTmp.add(new StringBuilder("Gap for attribute : ").append(attributeName).toString());
        if (mapRes.size() == 0) {
            listTmp.add("No gap for this attribute, maybe change the date or the gap duration to be sure.");
        } else {
            for (final Entry<String, String> e : mapRes.entrySet()) {
                listTmp.add(new StringBuilder(e.getKey()).append(STR_RESULT_PRES).append(e.getValue()).toString());
            }
        }

        xlogger.exit();
        return listTmp.toArray(new String[listTmp.size()]);
    }

    /**
     * Get a archiving configuration changes report between a date and now
     * <ul>
     * <li>Start</li>
     * <li>Stop (=no restart)</li>
     * <li>Restart without configuration changes</li>
     * <li>Restart with configuration changes</li>
     * </ul>
     *
     * @return a report of the archiving configuration changes
     *
     * @param date
     *            A String : start date (Format : yyyy-MM-dd HH:mm:ss)
     *
     * @return
     * @throws DevFailed
     */
    @Command(inTypeDesc = "String corresponding to the start date.\n Date format : yyyy-MM-dd HH:mm:ss ", outTypeDesc = "The difference report :\nNewly attribute start\nattribut start with no conf changed\nattribute definitely stopped\nattribute conf changed")
    public final String[] differenceSince(final String date) throws DevFailed {
        xlogger.entry();
        final String[] argin = new String[2];
        argin[0] = date;
        argin[1] = SIMPLE_DATE_FORMAT.get().format(new Timestamp(System.currentTimeMillis()));
        xlogger.exit();
        return differenceBetween(argin);
    }

    /**
     * Read a tango device attribute
     *
     * @param fullAttrName
     *            A String of full attribute name
     * @return A string representation of the attribute value
     * @throws DevFailed
     */
    @Command(inTypeDesc = "A full attribute name", outTypeDesc = "A string representation of the attribute value")
    public final String readAttribute(final String fullAttrName) {
        xlogger.entry();
        String result;
        try {
            final TangoAttribute ta = new TangoAttribute(fullAttrName);
            result = ta.readAsString(" ", "|");
        } catch (final DevFailed e) {
            result = DevFailedUtils.toString(e);
        }
        xlogger.exit();
        return result;
    }

    /**
     * Change the sort of the perAttributeReport :
     * <ul>
     * <li>true : sort by diagnosis</li>
     * <li>false : sort by name</li>
     * </ul>
     * by attribute name
     */
    @Command(inTypeDesc = "A boolean, true for sort the PerAttributeReport by the result(diagnosis), false for sort the attribut by their names ")
    public final void sortResultByType(final boolean argin) {
        xlogger.entry();
        this.sortedResultByType = argin;
        xlogger.exit();
    }

    /**
     * Verify if properties are crrectly fills.
     *
     * @throws DevFailed
     *             if a property is not correctly fill
     */
    private void checkProperties() throws DevFailed {
        xlogger.entry();
        try {
            checkPropertyNotification();
            checkPropertyInclude();
            checkPropertySafetyPeriod();
            checkPropertyPeriodicReport();
        } finally {
            xlogger.exit();
        }
    }

    /**
     * Verify that the device property NotificationContact is correctly fill and
     * extract different notification trigger in distinct java attribute
     *
     * The notification recipient followed by the type of trigger for the
     * notification (NONE | THRESHOLD | PERIODIC | FULL (THRESHOLD + PERIODIC)).
     * Each means of contact should be on a separate line. Example:
     * <ul>
     * <li>harry.potter@synchrotron-soleil.fr, THRESHOLD</li>
     * <li>luke.skywalker@synchrotron-soleil.fr, PERIODIC</li>
     * <li>frodo.baggins@synchrotron-soleil.fr, FULL</li>
     * </ul>
     *
     * @throws DevFailed
     *             if the property is not right
     */
    private void checkPropertyNotification() throws DevFailed {
        xlogger.entry();
        try {
            final StringBuilder errorBuilder = new StringBuilder();
            int i = 0;

            textTalkerLocale = new Locale(textTalkerLanguage);
            thresholdRecipient.clear();
            periodicRecipient.clear();
            for (; i < notificationContact.length; i++) {
                final String contact = notificationContact[i];
                if (contact != null && !contact.trim().isEmpty()) {
                    final String[] split = contact.split(",");
                    final String toCheck = split[0].trim();
                    if (toCheck.matches(DEVICE_PATTERN)) {
                        logger.info("A text talker has been found");
                        talkerRecipient.add(toCheck);
                    } else if (toCheck.matches(EMAIL_PATTERN)) {
                        final String argument = split[1].trim();
                        EmailNotification notification;
                        try {
                            notification = EmailNotification.valueOf(argument);
                        } catch (final IllegalArgumentException e) {
                            errorBuilder.append("the type of trigger for the notification ");
                            errorBuilder.append(argument);
                            errorBuilder.append(" line ");
                            errorBuilder.append(i + 1);
                            errorBuilder.append(" does not exists\n");
                            break;
                        }
                        switch (notification) {
                            case FULL:
                                thresholdRecipient.add(toCheck);
                                periodicRecipient.add(toCheck);
                                break;
                            case PERIODIC:
                                periodicRecipient.add(toCheck);
                                break;
                            case THRESHOLD:
                                thresholdRecipient.add(toCheck);
                                break;
                            default:
                                break;
                        }
                    } else {
                        errorBuilder.append("the address mail or device name ");
                        errorBuilder.append(split[0]);
                        errorBuilder.append(" line ");
                        errorBuilder.append(i + 1);
                        errorBuilder.append(" is not valid entry\n");
                    }
                }
            }
            if (errorBuilder.length() != 0 && i != 0) {
                errorBuilder.insert(0, "On the property NotificationContact : \n");
                final String error = errorBuilder.toString();
                logger.error(error);
				throw DevFailedUtils.newDevFailed(error);
            }
        } finally {
            xlogger.exit();
        }
    }

    /**
     * Verify that the device property PeriodicReport is correctly fill.<br/>
     * Good format : [number] [seconde|minute|hour|day]
     *
     * @throws DevFailed
     *             if the property is not right
     */
    private void checkPropertyPeriodicReport() throws DevFailed {
        xlogger.entry();
        try {
            if (!periodicReport.isEmpty()) {
                final StringBuilder errorBuilder = new StringBuilder();
                final String[] split = periodicReport.split(" ");
                if (split.length != 2) {
                    errorBuilder
                            .append("Device property \"periodicReport\" is not in good format : [number] [seconde|minute|hour|day]")
                            .append("\n");
                }
                int duration = 0;
                try {
                    duration = Integer.parseInt(split[0]);
                } catch (final NumberFormatException e) {
                    errorBuilder.append(e.getMessage()).append("\n");
                }
                final String type = split[1].trim().toLowerCase(Locale.ENGLISH);
                try {
                    duration = parseDuration(duration, type);
                } catch (final IllegalArgumentException e) {
                    errorBuilder.append("Unrecognized token ").append(type)
                            .append(" in device property \"periodicReport\"").append("\n");
                }
                if (errorBuilder.length() != 0) {
                    final String error = errorBuilder.toString();
                    logger.error(error);
					throw DevFailedUtils.newDevFailed(error);
                }
                this.periodicDuration = duration;
            }
        } finally {
            xlogger.exit();
        }
    }

    /**
     * Verify that the device property SafetyPeriod is correctly fill.<br/>
     * Good format : [number] [seconde|minute|hour]
     *
     * @throws DevFailed
     *             if the property is not right
     */
    private void checkPropertySafetyPeriod() throws DevFailed {
        xlogger.entry();
        try {
            final StringBuilder errorBuilder = new StringBuilder();
            if (!safetyPeriod.isEmpty()) {
                final String[] split = safetyPeriod.split(" ");
                if (split.length != 2) {
                    errorBuilder
                            .append("Device property \"SafetyPeriod\" is not in good format : [number] [seconde|minute|hour]")
                            .append("\n");
                }
                int duration = 0;
                try {
                    duration = Integer.parseInt(split[0]);
                } catch (final NumberFormatException e) {
                    errorBuilder.append(e.getMessage()).append("\n");
                }
                final String type = split[1].trim().toLowerCase(Locale.ENGLISH);
                try {
                    duration = parseDuration(duration, type);
                } catch (final IllegalArgumentException e) {
                    errorBuilder.append("Unrecognized token ").append(type)
                            .append(" in device property \"SafetyPeriod\"").append("\n");
                }
                if (errorBuilder.length() != 0) {
                    final String error = errorBuilder.toString();
                    logger.error(error);
					throw DevFailedUtils.newDevFailed(error);
                }
                this.safetyDuration = duration;
            }
        } finally {
            xlogger.exit();
        }
    }

    /**
     * Utility method to parse a duration.
     *
     * @param initialDuration
     *            a int that contains a initial duration
     * @param token
     *            a String that correspond to :
     *            <ul>
     *            <li>second</li>
     *            <li>minute</li>
     *            <li>hour</li>
     *            <li>day</li>
     *            </ul>
     *
     * @return A int corresponding to the initialDuration multiplicated by the
     *         value of token
     */
    private int parseDuration(final int initialDuration, final String token) {
        xlogger.entry();
        final int duration = GetConf.parseDuration(initialDuration, token, false);
        xlogger.exit();
        return duration;
    }

    /**
     * Verify that the device property OnlyIncludeList is correctly fill. And
     * extract differents type of selections in different java attribute <br/>
     *
     * This attribute set will be defined by a set of regular expressions. The
     * attribute will be monitored if it validates one or more of these regular
     * expressions.<br/>
     * <br/>
     * <u>Example:</u><br/>
     * ".*\/EI/.*":<br/>
     * only the attributes of the family "EI" will be monitored<br/>
     * <br/>
     * Moreover, it will be possible to select a subset of attributes by the
     * attribute characteristics. <br/>
     *
     * <br/>
     * <u>This selection can by:</u> <br/>
     * <ul>
     * <li>Type: double, integer...</li>
     * <li>Format: scalar, spectrum or image</li>
     * <li>Writable: Read only, read write �</li>
     * </ul>
     *
     * <br/>
     * <u>Example:</u><br/>
     * <ul>
     * <li>"Type(double)": only double attribute will be monitored</li>
     *
     * <li>"Format(scalar)<br/>
     * Type(int)"<br/>
     * only scalar integer will be monitored</li>
     * </ul>
     *
     * <br/>
     * <u>Global example:</u><br/>
     * "*\/EI/*<br/>
     * *\/DG/*<br/>
     * Format(scalar)"<br/>
     * Only scalar attributes of the families "EI" or "DG" will be monitored
     *
     * @throws DevFailed
     *             if the property is not right
     */
    private void checkPropertyInclude() throws DevFailed {
        xlogger.entry();
        try {
            final Map<Integer, List<String>> errorMap = new HashMap<Integer, List<String>>();
            final List<String> onlyIncludeRegexTmp = new ArrayList<String>();
            final String[] onlyIncludeList = this.onlyIncludeList;
            int i = 1;
            if (onlyIncludeList != null) {
                for (String line : onlyIncludeList) {
                    if (line != null) {
                        line = line.toLowerCase(Locale.ENGLISH).trim();
                        if (!line.isEmpty()) {
                            final List<String> errorList = new ArrayList<String>();
                            if (line.startsWith("type(")) {
                                attrsType = getRealValueFromString(Integer.class, TangoUtil.TYPE_MAP, line, errorList);
                            } else if (line.startsWith("format(")) {
                                attrsDataFormat = getRealValueFromString(AttrDataFormat.class, TangoUtil.FORMAT_MAP,
                                        line, errorList);
                            } else if (line.startsWith("writable(")) {
                                attrsWriteType = getRealValueFromString(AttrWriteType.class, TangoUtil.WRITABLE_MAP,
                                        line, errorList);
                            } else {
                                // regex normal
                                onlyIncludeRegexTmp.add(formatToGoodRegex(line));
                            }

                            if (!errorList.isEmpty()) {
                                errorMap.put(Integer.valueOf(i), errorList);
                            }
                        }
                        i++;
                    }
                }
            }
            if (!errorMap.isEmpty()) {
                final StringBuilder errorBuilder = new StringBuilder();
                errorBuilder.append("Property OnlyIncludeList :\n");
                for (final Entry<Integer, List<String>> entry : errorMap.entrySet()) {
                    errorBuilder.append("\t- has incorrect syntaxe at line ").append(entry.getKey()).append(" : \n");
                    for (final String error : entry.getValue()) {
                        errorBuilder.append("\t\t - ").append(error).append(" is not a correct value.\n");
                    }
                }

                final String error = errorBuilder.toString();
				throw DevFailedUtils.newDevFailed(error);
            }
            this.onlyIncludeRegex = onlyIncludeRegexTmp;
        } finally {
            xlogger.exit();
        }
    }

    /**
     * Cut a string with "," and try to get the string representation of each
     * token on the map
     *
     * @param c
     *            The return class and values map
     * @param map
     *            A Map<String,E>
     * @param line
     *            A String representation with token split up with coma
     * @param errorList
     *            A list for get error
     * @return An array of E
     */
    @SuppressWarnings("unchecked")
    private <E> E[] getRealValueFromString(final Class<E> c, final Map<String, E> map, final String line,
            final List<String> errorList) {
        final String newLine = line.substring(line.indexOf('(') + 1, line.lastIndexOf(')'));
        final String[] split = newLine.split(",");
        final E[] ee = (E[]) Array.newInstance(c, split.length);
        for (int j = 0; j < split.length; j++) {
            final String s = split[j].trim().toUpperCase(Locale.ENGLISH);
            final E e = map.get(s);
            if (e == null) {
                errorList.add(s);
            }
            ee[j] = e;
        }
        return ee;
    }

    /**
     * Transform a simple regex to a simple java regex Add "." before "*" or "?"
     * to simplify the user's regex
     *
     * @param regexLine
     *            A String user's regex
     * @return a String java regex
     */
    private String formatToGoodRegex(final String regexLine) {
        xlogger.entry();
        final String toRegex = regexLine == null ? null
                : regexLine.replace("*", ".*").replace("..*", ".*").replace("?", ".?").replace("..?", ".?");
        // StringBuilder finalLine = new StringBuilder();
        // char[] chars = regexLine.toCharArray();
        // for (int j = 0; j < chars.length; j++) {
        // if ((chars[j] == '*' || chars[j] == '?') && (j == 0 || chars[j - 1] != '.')) {
        // finalLine.append(".");
        // }
        // finalLine.append(chars[j]);
        // }
        xlogger.exit();
        // return finalLine.toString();
        return toRegex;
    }

    private List<String> propertyToRegExList(final String[] property) {
        final List<String> tmp = new ArrayList<String>();
        if (property != null) {
            for (final String prop : property) {
                final String regex = formatToGoodRegex(prop);
                if (regex != null) {
                    tmp.add(regex);
                }
            }
        }
        return tmp;
    }

    /**
     * Set the dynamicManager
     *
     * @see DynamicManager
     * @param dynamicManager
     */
    public final void setDynamicManager(final DynamicManager dynamicManager) {
        this.dynamicManager = dynamicManager;
    }

    /**
     * The main of the device
     *
     * @param args
     */
    public static void main(final String[] args) {
        final ResourceBundle rb = ResourceBundle.getBundle("fr.soleil.application");
        version = rb.getString("project.version");
        // reactivate following code to test PROBLEM-1014
        // globalStart = System.currentTimeMillis();
        ServerManager.getInstance().start(args, HDBTDBArchivingWatcher.class);
        // reactivate following code to test PROBLEM-1014
        // System.out.println(DateUtil.elapsedTimeToStringBuilder(new StringBuilder("************ It took: "),
        // System.currentTimeMillis() - globalStart).append("to start the device!"));
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * An enum for the type of trigger for the notification
     * <ul>
     * <li>NONE : no email send</li>
     * <li>THRESHOLD : email send when the threshold is exceeded</li>
     * <li>PERIODIC : email send one time per day</li>
     * <li>FULL : THRESHOLD + PERIODIC</li>
     * </ul>
     */
    public static enum EmailNotification {
        NONE, THRESHOLD, PERIODIC, FULL
    }

    /**
     * Runnable for get a periodic difference report
     *
     */
    private class DiffReportRunnable implements Runnable {
        private static final int NUMBER_OF_DIFFERENCE_TYPE = 5;
        private static final String NO_CHANGED = "-> no changed";
        private long lastTimeReport = -1;
        private final Map<String, String> errorMap;

        /**
         * Constructor
         *
         * @param period
         *            The date of the last report
         */
        public DiffReportRunnable(final int period) {
            this.lastTimeReport = System.currentTimeMillis() - period;
            errorMap = new ConcurrentHashMap<String, String>();
        }

        @Override
        public void run() {
            errorMap.remove(Long.toString(lastTimeReport));
            final String date = SIMPLE_DATE_FORMAT.get().format(new Timestamp(lastTimeReport));
            lastTimeReport = System.currentTimeMillis();
            final String now = SIMPLE_DATE_FORMAT.get().format(new Timestamp(lastTimeReport));
            try {
                // new attr
                final List<String> newAttrs = HDBTDBArchivingWatcher.this.dbReader.newEntryADTBetween(date, now);

                // attr start
                final Map<String, Integer> attrStart = HDBTDBArchivingWatcher.this.dbReader.attrStartBetween(date, now);
                // attr stopped
                final Map<String, Integer> attrStop = HDBTDBArchivingWatcher.this.dbReader.attrStopBetween(date, now);
                // attr conf changed
                final Map<String, Integer> attrConfChanged = HDBTDBArchivingWatcher.this.dbReader
                        .attrConfChangeBetween(date, now);

                final int newAttrSize = newAttrs.size() != 0 ? newAttrs.size() : 1;
                final int attrStartSize = attrStart.size() != 0 ? attrStart.size() : 1;
                final int attrStopSize = attrStop.size() != 0 ? attrStop.size() : 1;
                final int attrConfSize = attrConfChanged.size() != 0 ? attrConfChanged.size() : 1;
                final int globalSize = newAttrSize + attrStartSize + attrStopSize + attrConfSize
                        + NUMBER_OF_DIFFERENCE_TYPE;
                final String[][] result = new String[globalSize][2];
                int i = 0;
                // this in not pretty as well
                // i did not found how to nicely show the result
                result[i][0] = "Report date : ";
                result[i++][1] = SIMPLE_DATE_FORMAT.get().parse(new Timestamp(lastTimeReport).toString()).toString();
                result[i][0] = "";
                result[i++][1] = "";
                result[i][0] = "Newly attribute start : ";
                result[i++][1] = "";
                int j = i;
                for (final String newAttr : newAttrs) {
                    result[i][0] = newAttr;
                    result[i++][1] = "";
                }
                if (j == i) {
                    result[i - 1][1] = NO_CHANGED;
                }
                result[i][0] = "";
                result[i++][1] = "";
                result[i][0] = "Attribute start :";
                result[i++][1] = "";
                j = i;
                for (final Entry<String, Integer> e : attrStart.entrySet()) {
                    final StringBuilder sb = new StringBuilder(STR_RESULT_PRES);
                    result[i][0] = e.getKey();
                    result[i++][1] = sb.append(e.getValue().toString()).toString();
                }
                if (j == i) {
                    result[i - 1][1] = NO_CHANGED;
                }
                result[i][0] = "";
                result[i++][1] = "";
                result[i][0] = "Attribute stop  :";
                result[i++][1] = "";
                j = i;
                for (final Entry<String, Integer> e : attrStop.entrySet()) {
                    final StringBuilder sb = new StringBuilder(STR_RESULT_PRES);
                    result[i][0] = e.getKey();
                    result[i++][1] = sb.append(e.getValue().toString()).toString();
                }
                if (j == i) {
                    result[i - 1][1] = NO_CHANGED;
                }
                result[i][0] = "";
                result[i++][1] = "";
                result[i][0] = "Attribute configuration changed  :";
                result[i++][1] = "";
                j = i;
                for (final Entry<String, Integer> e : attrConfChanged.entrySet()) {
                    final StringBuilder sb = new StringBuilder(STR_RESULT_PRES);
                    result[i][0] = e.getKey();
                    result[i++][1] = sb.append(e.getValue().toString()).toString();
                }
                if (j == i) {
                    result[i - 1][1] = NO_CHANGED;
                }
                HDBTDBArchivingWatcher.this.diffReport = result;
                errorMap.clear();
            } catch (final DevFailed e) {
                DevFailedUtils.logDevFailed(e, HDBTDBArchivingWatcher.this.logger);
                errorMap.put(Long.toString(lastTimeReport), DevFailedUtils.toString(e));
            } catch (final ParseException e) {
                errorMap.put(Long.toString(lastTimeReport), e.getMessage());
            }

        }

        /**
         * Get error from this @Runnable
         *
         * @return
         */
        public Map<String, String> getErrors() {
            return errorMap;
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static final class DateThreadLocal extends ThreadLocal<SimpleDateFormat> {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    }

    /**
     * The thread that will regularly check KO attributes
     *
     * @author GIRARDOT
     */
    protected class CycleManagerThread extends Thread {
        private final int cyclePause;
        private boolean threadWasNull;
        private volatile boolean started;
        private volatile long lastLoopStartTime;
        private volatile long lastLoopStopTime;
        // logging, get context from parent thread
        @SuppressWarnings("unchecked")
        private final Map<String, String> contextMap = MDC.getCopyOfContextMap();

        public CycleManagerThread(final int cyclePause) {
            super("Watcher - Cycle manager");
            this.cyclePause = cyclePause;
            threadWasNull = true;
            started = false;
            lastLoopStartTime = 1;
            lastLoopStopTime = 0;
        }

        public int getCyclePause() {
            return cyclePause;
        }

        public boolean isStarted() {
            return started;
        }

        public long getLastLoopStartTime() {
            return lastLoopStartTime;
        }

        public long getLastLoopStopTime() {
            return lastLoopStopTime;
        }

        @Override
        public synchronized void start() {
            started = true;
            super.start();
        }

        @Override
        public void run() {
            // logging, get context from parent thread to retrieve MDC key serverName
            MDC.setContextMap(contextMap);
            while (true) {
                try {
                    if (isInterrupted()) {
                        break;
                    }
                    lastLoopStartTime = System.currentTimeMillis();
                    // ICycleManager cycleManager = HDBTDBArchivingWatcher.this.cycleManager;
                    if (cycleManager == null) {
                        threadWasNull = true;
                        try {
                            reconnect();
                            updateLastConnectionError(null);
                        } catch (final Exception e) {
                            updateLastConnectionError(e);
							logger.warn(appendErrorToStringBuilder(
                                    new StringBuilder("Failed to reconnect to the database:\n"), e).toString());
                        }
                    } else {
                        if (threadWasNull) {
                            threadWasNull = false;
                            logger.info("CycleManager - start diagnosis thread");
                            cycleManager.startDiagnosis();
                        }
                        logger.info("CycleManager - start checking archiving");
                        cycleManager.run();
                    }
                    lastLoopStopTime = System.currentTimeMillis();
                    if (isInterrupted()) {
                        break;
                    } else {
                        try {
                            logger.info(DateUtil
                                    .elapsedTimeToStringBuilder(new StringBuilder("CycleManager - cycle pause ("),
                                            cyclePause)
                                    .append(")").toString());
                            sleep(cyclePause);
                        } catch (final InterruptedException e) {
                            logger.info("CycleManager interrupted");
                            break;
                        }
                    }
                } catch (final Throwable e) {
                    logger.error("unexpected error", e);
                }
            }
            if (cycleManager != null) {
                cycleManager.stopDiagnosis();
                logger.info("CycleManager has been interrupted");
            }
        }
    }

	public StringBuilder appendErrorToStringBuilder(StringBuilder builder, Throwable error) {
		if (builder == null) {
			builder = new StringBuilder();
		}
		if (error != null) {
			if (error instanceof DevFailed) {
				builder.append(DevFailedUtils.toString((DevFailed) error));
			} else {
				StringWriter errors = new StringWriter();
				error.printStackTrace(new PrintWriter(errors));
				builder.append(errors.toString());
			}
		}
		return builder;
	}
}
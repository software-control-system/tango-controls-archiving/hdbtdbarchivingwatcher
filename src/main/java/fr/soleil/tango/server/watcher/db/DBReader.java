package fr.soleil.tango.server.watcher.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.utils.DateUtil;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.DataBaseManager;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.manager.ArchivingManagerApiRef;
import fr.soleil.archiving.hdbtdb.api.manager.ArchivingManagerApiRefFactory;
import fr.soleil.archiving.hdbtdb.api.manager.IArchivingManagerApiRef;
import fr.soleil.archiving.hdbtdb.api.manager.TangoAccess;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.archiving.hdbtdb.api.tools.mode.EventMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeSeuil;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.tango.server.watcher.controller.Controller.Control;
import fr.soleil.tango.server.watcher.controller.diagnosis.DiagnosisInfo;
import fr.soleil.tango.server.watcher.controller.diagnosis.ResDiagnosis;
import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;
import fr.soleil.tango.server.watcher.dto.attribute.ModeData;

/**
 * An implementation that really loads data from the physical database
 *
 * @author Fourneau
 */
public class DBReader {
    /**
     * Code for the total load of an HDB Archiver
     */
    public static final int LOAD_ALL = 0;
    /**
     * Code for the scalar load of an HDB Archiver
     */
    public static final int LOAD_SCALAR = 1;
    /**
     * Code for the spectrum load of an HDB Archiver
     */
    public static final int LOAD_SPECTRUM = 2;
    /**
     * Code for the image load of an HDB Archiver
     */
    public static final int LOAD_IMAGE = 3;
    private static final String HDB = "HDB";
    /**
     * Format of the date after a request
     */
    private static final String DB_DATE_FORMAT_EXIT = "yyyy-MM-dd HH:mm:ss.SSS";
    /**
     * Format of the date for insert on the database
     */
    private static final String DB_DATE_FORMAT = "YYYY-MM-DD hh24:mi:ss";
    /**
     * The logger
     */
    private final Logger logger = LoggerFactory.getLogger(DBReader.class);
    private final IArchivingManagerApiRef manager;
    /**
     * The {@link AbstractDataBaseConnector} that knows how to connect to the database
     */
    private final AbstractDataBaseConnector connector;
    /**
     * Whether the database is historic
     */
    private final boolean historic;
    /**
     * Object to access the database
     */
    private DataBaseManager database;

    public DBReader(final AbstractDataBaseConnector connector) {
        this.connector = connector;
        if (connector == null) {
            historic = true;
        } else {
            historic = isHDB(connector.getSchema()) || isHDB(connector.getName());
        }
        this.manager = ArchivingManagerApiRefFactory.getInstance(historic, connector);
    }

    protected boolean isHDB(final String value) {
        return value != null && HDB.equalsIgnoreCase(value.trim());
    }

    public boolean isHistoric() {
        return historic;
    }

    /**
     * Get the number of archived attribute in the connected database
     *
     * @return the number of archived attribute in the connected database
     * @throws DevFailed
     */
    public int getNumberOfArchivedAttributes() throws DevFailed {
        int count = 0;
        openConnectionIfNecessary();
        if (connector != null) {
            ResultSet res = null;
            PreparedStatement stmt = null;
            Connection conn = null;
            /**
             * Query :<br/>
             * select count(*) from amt where stop_date is null
             */
            final StringBuilder queryBuilder = new StringBuilder("select count(*) from ").append(connector.getSchema())
                    .append(".amt where stop_date is null");
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.prepareStatement(queryBuilder.toString(), ResultSet.TYPE_FORWARD_ONLY,
                            ResultSet.CONCUR_READ_ONLY);
                    res = stmt.executeQuery();
                    res.next();
                    count = res.getInt(1);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, queryBuilder.toString()).toTangoException();
            } finally {
                DbUtils.closeQuietly(res);
                DbUtils.closeQuietly(stmt);
                DbUtils.closeQuietly(conn);
                connector.closeConnection(conn);
            }
        }
        return count;
    }

    /**
     * Get all archived attributes names
     *
     * @return a list of archived attribute names with a empty archiving mode
     * @throws DevFailed
     */
    public Map<String, ModeData> getArchivedAttributes() throws DevFailed {
        openConnectionIfNecessary();

        String[] currentArchivedAtt = null;
        try {
            currentArchivedAtt = this.database.getMode().getCurrentArchivedAtt();
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
        if (currentArchivedAtt == null) {
            return null;
        }

        final int numberOfAttributes = currentArchivedAtt.length;
        final Map<String, ModeData> ret = new HashMap<String, ModeData>(numberOfAttributes);
        for (int i = 0; i < numberOfAttributes; i++) {
            final String completeName = currentArchivedAtt[i];
            ret.put(completeName, new ModeData()); // The ModeData will be
            // loaded when necessary ie.
            // at the beginning of each
            // step
        }

        return ret;
    }

    /**
     * Get all archived attributes names depending on theirs writables, types
     * and formats
     *
     * @param writes Different writables types to select
     * @param types Different data types to select
     * @param formats Different formats to select
     * @return a list of archived attribute names with a empty archiving mode
     *         corresponding to the parameters
     * @throws DevFailed
     */
    public Map<String, ModeData> getArchivedAttributes(final AttrWriteType[] writes, final Integer[] types,
            final AttrDataFormat[] formats) throws DevFailed {
        String[] currentArchivedAtt = null;
        openConnectionIfNecessary();
        final Map<String, ModeData> ret;
        if (connector == null) {
            ret = new HashMap<String, ModeData>();
        } else {
            PreparedStatement stmt = null;
            Connection conn = null;
            ResultSet res = null;
            /**
             * Query :<br/>
             * SELECT adt.full_name FROM adt WHERE (amt.stop_date IS NULL AND amt.ID
             * = adt.ID) AND ( WRITABLE = ... OR ...) AND ( DATA_TYPE = ... OR ...)
             * AND ( DATA_FORMAT = ... OR ...)
             */
            final StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("SELECT adt.full_name FROM ").append(connector.getSchema()).append(".adt, ")
                    .append(connector.getSchema()).append(".amt WHERE (amt.stop_date IS NULL AND amt.ID = adt.ID) ");
            if (writes.length > 0) {
                queryBuilder.append(" AND (");
                int i = 0;
                for (; i < writes.length - 1; i++) {
                    queryBuilder.append("WRITABLE = ").append(writes[i].value()).append(" OR ");
                }
                queryBuilder.append(" WRITABLE = ").append(writes[i].value());
                queryBuilder.append(")");
            }
            if (types.length > 0) {
                queryBuilder.append(" AND (");
                int i = 0;
                for (; i < types.length - 1; i++) {
                    queryBuilder.append("DATA_TYPE = ").append(types[i]).append(" OR ");
                }
                queryBuilder.append(" DATA_TYPE = ").append(types[i]);
                queryBuilder.append(")");
            }
            if (formats.length > 0) {
                queryBuilder.append(" AND (");
                int i = 0;
                for (; i < formats.length - 1; i++) {
                    queryBuilder.append("DATA_FORMAT = ").append(formats[i].value()).append(" OR ");
                }
                queryBuilder.append(" DATA_FORMAT = ").append(formats[i].value());
                queryBuilder.append(")");
            }

            try {
                conn = connector.getConnection();
                final Collection<String> nameVect = new ArrayList<String>();
                if (conn != null) {
                    stmt = conn.prepareStatement(queryBuilder.toString(), ResultSet.TYPE_FORWARD_ONLY,
                            ResultSet.CONCUR_READ_ONLY);
                    res = stmt.executeQuery();
                    while (res.next()) {
                        final String next = res.getString(1);
                        if (next != null) {
                            nameVect.add(next);
                        }
                    }
                }
                currentArchivedAtt = nameVect.toArray(new String[nameVect.size()]);
                final int numberOfAttributes = currentArchivedAtt.length;
                ret = new HashMap<String, ModeData>(numberOfAttributes);
                for (int i = 0; i < numberOfAttributes; i++) {
                    final String completeName = currentArchivedAtt[i];
                    ret.put(completeName, new ModeData());
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, queryBuilder.toString()).toTangoException();
            } finally {
                DbUtils.closeQuietly(res);
                DbUtils.closeQuietly(stmt);
                DbUtils.closeQuietly(conn);
                connector.closeConnection(conn);
            }
        }
        return ret;
    }

    /**
     * Get the archiving configuration of an attribute
     *
     * @param completeName of the attribute
     * @return @see{ModeData} the archiving configuration of the attribute given
     *         on parameter
     * @throws ArchivingException
     */
    public ModeData getModeDataForAttribute(final String completeName) throws DevFailed {
        openConnectionIfNecessary();

        final ModeData ret = new ModeData();

        final String archiver = this.getArchiverForAttribute(completeName);
        ret.setArchiver(archiver);

        final Mode mode = this.getModeForAttribute(completeName);
        ret.setMode(mode);

        if (mode.getModeP() == null) {
            this.logger.debug("DBReaderAdapter/getModeDataForAttribute/mode.getModeP() == null for attribute|"
                    + completeName + "|");
        }

        return ret;
    }

    /**
     * Get the archiver in charge of an atribute
     *
     * @param completeName of the attribute
     * @return the archiver inc harge of the attribute given in parameter
     * @throws DevFailed
     */
    private String getArchiverForAttribute(final String completeName) throws DevFailed {
        openConnectionIfNecessary();

        String device = null;

        try {
            device = this.database.getMode().getDeviceInCharge(completeName);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }

        return device;
    }

    /**
     * Get the different gap for an attribut. A gap is a period during which no
     * valid data is inserted
     *
     * @param attribute the attribute to check
     * @param gapDuration the minimum duration with no valid data for detect a gap
     * @return a pair of date : the start and the and of the gap
     * @throws DevFailed
     */
    public Map<String, String> getGaps(final String attribute, final int gapDuration, final String startDate,
            final String stopDate) throws DevFailed {
        openConnectionIfNecessary();
        PreparedStatement stmt = null;
        Connection conn = null;
        ResultSet res = null;
        final Map<String, String> argout = new HashMap<String, String>();

        if (connector != null) {
            final int id = this.getAttributeId(attribute);
            final int writable = this.getAttWritableCriterion(attribute);
            final StringBuilder queryBuilder = new StringBuilder();
            /**
             * Query :<br/>
             * select TIME from att_X where value is not null and value is not nan
             * order by time ASC
             */
            queryBuilder.append("select TIME from ").append(connector.getSchema()).append(".att_").append(id)
                    .append(" where ");
            if (writable == AttrWriteType._READ || writable == AttrWriteType._WRITE) {
                queryBuilder.append("value");
            } else {
                queryBuilder.append("read_value");
            }
            queryBuilder.append(" is not null AND ");
            if (writable == AttrWriteType._READ || writable == AttrWriteType._WRITE) {
                queryBuilder.append("value");
            } else {
                queryBuilder.append("read_value");
            }
            queryBuilder.append(" is not nan ");

            if (startDate != null) {
                queryBuilder.append(" AND TIME between to_date('");
                queryBuilder.append(startDate).append("','").append(DB_DATE_FORMAT).append("') AND to_date('")
                        .append(stopDate).append("','").append(DB_DATE_FORMAT).append("') ");
            }

            queryBuilder.append(" order by time ASC");
            try {
                conn = connector.getConnection();
                if (conn != null) {

                    stmt = conn.prepareStatement(queryBuilder.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_READ_ONLY);
                    this.logger.info("query on string and prepared");
                    res = stmt.executeQuery();
                    this.logger.info("query executed");
                    DateTime previous = null;
                    final DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                    final DateTimeFormatter parser = DateTimeFormat.forPattern(DB_DATE_FORMAT_EXIT);

                    while (res.next()) {
                        DateTime actual;
                        final String time = res.getString(1);
                        // XXX : is not very beautiful, but it's look likes date on
                        // db
                        // have different formats at least on test database
                        try {
                            actual = parser.withZone(DateTimeZone.UTC).parseDateTime(time);
                        } catch (final IllegalArgumentException e) {
                            this.logger.debug("error {}", e);
                            actual = fmt.withZone(DateTimeZone.UTC).parseDateTime(time);
                        }
                        if (previous != null) {
                            final Duration d = new Duration(previous, actual);
                            // gap duration is in millisecond !!
                            if (d.getStandardSeconds() * 1000 > gapDuration) {
                                argout.put(fmt.print(previous), fmt.print(actual));
                            }
                        }
                        previous = actual;
                    }
                    // }
                }
                this.logger.info("end gap !");
            } catch (final SQLException e) {
                this.logger.error("error ", e);
                throw new ArchivingException(e, queryBuilder.toString()).toTangoException();
            } catch (final IllegalArgumentException e) {
                this.logger.error("error ", e);
                DevFailedUtils.throwDevFailed(e);
            } finally {
                DbUtils.closeQuietly(res);
                DbUtils.closeQuietly(stmt);
                DbUtils.closeQuietly(conn);
                connector.closeConnection(conn);
            }
        }
        return argout;
    }

    /**
     * Check all the attribute
     *
     * 
     * @param safetyPeriod the period for avoid false-positive
     * @return a Map with each attribute and a boolean for define if the attribute
     *         is ko or not
     * @throws DevFailed
     */
    public Map<String, Boolean> checkAllAttributes(final int safetyPeriod) throws DevFailed {
        openConnectionIfNecessary();
        PreparedStatement stmt = null;
        Connection conn = null;
        final PreparedStatement stmt2 = null;
        final Map<String, Boolean> attrsAreOk = new HashMap<String, Boolean>();
        if (connector != null) {
            // final SimpleDateFormat dateFormat = new SimpleDateFormat(DB_DATE_FORMAT_EXIT);
            ResultSet res = null;
            final ResultSet res2 = null;
            /**
             * Query :<br/>
             * select
             * ADT.ID,TMP.START_DATE,TMP.PER_PER_MOD,ADT.FULL_NAME,ADT.WRITABLE FROM
             * ADT INNER JOIN (SELECT AMT_TMP.ID, AMT_TMP.START_DATE ,
             * AMT_TMP.PER_PER_MOD FROM AMT AMT_TMP INNER JOIN (SELECT ID,
             * MAX(START_DATE) AS MAX_START_DATE FROM AMT WHERE STOP_DATE IS NULL
             * GROUP BY ID) MAX_START_PER_ID on AMT_TMP.START_DATE =
             * MAX_START_PER_ID.MAX_START_DATE AND MAX_START_PER_ID.ID =
             * AMT_TMP.ID)TMP ON ADT.ID = TMP.ID AND FULL_NAME IN ('...','...')
             */
            final StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("SELECT ").append(connector.getSchema())
                    .append(".ADT.ID,TMP.START_DATE,TMP.PER_PER_MOD, ").append(connector.getSchema())
                    .append(".ADT.FULL_NAME, ").append(connector.getSchema()).append(".ADT.WRITABLE FROM ")
                    .append(connector.getSchema())
                    .append(".ADT INNER JOIN (SELECT AMT_TMP.ID, AMT_TMP.START_DATE , AMT_TMP.PER_PER_MOD FROM ")
                    .append(connector.getSchema())
                    .append(".AMT AMT_TMP INNER JOIN (SELECT ID, MAX(START_DATE) AS MAX_START_DATE FROM ")
                    .append(connector.getSchema())
                    .append(".AMT WHERE STOP_DATE IS NULL GROUP BY ID) MAX_START_PER_ID on AMT_TMP.START_DATE = MAX_START_PER_ID.MAX_START_DATE AND MAX_START_PER_ID.ID = AMT_TMP.ID)TMP ON ")
                    .append(connector.getSchema()).append(".ADT.ID = TMP.ID");

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.prepareStatement(queryBuilder.toString(), ResultSet.TYPE_FORWARD_ONLY,
                            ResultSet.CONCUR_READ_ONLY);
                    res = stmt.executeQuery();
                    while (res.next()) {
                        final long perMod = res.getLong(3);
                        final String attrName = res.getString(4);
                        final int writable = res.getInt(5);

                        final String[] timeAndValue = this.getTimeValueNullOrNotOfLastInsert(attrName,
                                this.getAttFormatCriterion(attrName), writable);
                        final String time = timeAndValue[0];
                        final String value = timeAndValue[1];
                        final Timestamp timeTimestamp = new Timestamp(DateUtil.stringToMilli(time));
                        final Timestamp nowT = this.now();
                        if (value.trim().equalsIgnoreCase("null")
                                || timeTimestamp.getTime() < nowT.getTime() - (perMod + safetyPeriod)) {
                            // KO
                            attrsAreOk.put(attrName, false);
                        } else {
                            attrsAreOk.put(attrName, true);
                        }
                    }
                }
            } catch (final ArchivingException e) {
                this.logger.error(e.getMessage());
                throw e.toTangoException();
            } catch (final SQLException e) {
                this.logger.error(e.getMessage());
                throw new ArchivingException(e, queryBuilder.toString()).toTangoException();
            } finally {
                DbUtils.closeQuietly(res);
                DbUtils.closeQuietly(res2);
                DbUtils.closeQuietly(stmt);
                DbUtils.closeQuietly(stmt2);
                DbUtils.closeQuietly(conn);
                connector.closeConnection(conn);
            }
        }
        return attrsAreOk;
    }

    /**
     * Get the archiving mode of an attribute
     *
     * @param completeName the attribute complete name
     * @return the archiving mode of the attribute
     * @throws ArchivingException
     */

    public Mode getModeForAttribute(final String completeName) throws DevFailed {
        openConnectionIfNecessary();

        Mode mode = null;

        try {
            mode = this.database.getMode().getCurrentArchivingMode(completeName);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }

        return mode;
    }

    /**
     * Get the number of record on the database for the attribute
     *
     * @param completeName the attribute complete name
     * @return the number of record on database for the given attribute
     * @throws DevFailed
     */
    public int getRecordCount(final String completeName) throws DevFailed {
        openConnectionIfNecessary();
        int res = 0;
        try {
            res = this.database.getAttribute().getAttRecordCount(completeName);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
        return res;
    }

    /**
     * Get the time of the last insert for an attribute
     *
     * @param completeName the attribute complete name
     * @return the time of last insert for the given attribute
     * @throws DevFailed
     */
    public Timestamp getTimeOfLastInsert(final String completeName) throws DevFailed {
        openConnectionIfNecessary();

        Timestamp ret = null;
        try {
            ret = this.database.getDbUtil().getTimeOfLastInsert(completeName, true);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }

        return ret;
    }

    /**
     * Get the id on database for an attribute
     *
     * @param completeName the attribute complete name
     * @return the id on database for the given attribute
     * @throws DevFailed
     */
    public int getAttributeId(final String completeName) throws DevFailed {
        openConnectionIfNecessary();
        int ret = -1;
        try {
            final IAdtAptAttributes adtAptAttributes = this.database.getAttribute();
            ret = adtAptAttributes.getIds().getAttID(completeName, adtAptAttributes.isCaseSentitiveDatabase());
            if (ret == 0) {
                throw DevFailedUtils.newDevFailed("The attribute does not exists");
            }
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }

        return ret;

    }

    /**
     * This method returns a String array which contains as first element the
     * time value and the second element indicates if the corresponding value is
     * null or not
     *
     * @param completeName : attribute full_name
     * @param dataFormat : AttrDataFormat._SCALAR or AttrDataFormat._SPECTRUM ot
     *            AttrDataFormat._IMAGE
     * @param writable AttrWriteType._READ_WRITE or AttrWriteType._READ or
     *            AttrWriteType._WRITE
     * @return couple of string with the maximum time value and the value equal
     *         to null or notnull
     * @throws DevFailed
     */
    public String[] getTimeValueNullOrNotOfLastInsert(final String completeName, final int dataFormat,
            final int writable) throws DevFailed {
        openConnectionIfNecessary();

        String ret[] = null;
        try {
            ret = this.database.getDbUtil().getTimeValueNullOrNotOfLastInsert(completeName, dataFormat, writable);

        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }

        return ret;
    }

    /**
     * Get the time of now
     *
     * @return the time of now
     * @throws DevFailed
     */
    public Timestamp now() throws DevFailed {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * Get the number of record for an attribute between now and a period
     *
     * @param completeName the atribute complete name
     * @param period before now
     * @return the number record between now and period parameter for the given
     *         attribute
     * @throws DevFailed
     */
    public final int getRecordCount(final String completeName, final int period) throws DevFailed {

        openConnectionIfNecessary();

        final String afterS = this.now().toString();
        final String beforeS = new Timestamp(System.currentTimeMillis() - period).toString();

        try {
            final DbData[] dbData = this.database.getExtractor().getDataGettersBetweenDates().getAttDataBetweenDates(
                    SamplingType.getSamplingType(SamplingType.ALL), completeName, beforeS, afterS);
            DbData refData = DbData.getFirstDbData(dbData);
            int length;
            if ((refData == null) || (refData.getTimedData() == null)) {
                this.logger.debug("DBReaderAdapter/case ( dbData == null || dbData.getData() == null )");
                length = 0;
            } else {
                length = refData.getTimedData().length;
            }
            return length;
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    /**
     * Return true of the attribute is a scalar, otherwise false
     *
     * @param completeName the attribute complete name
     * @return true of the attribute is a scalar, otherwise false
     * @throws DevFailed
     */
    public final boolean isScalar(final String completeName) throws DevFailed {
        openConnectionIfNecessary();

        int format;
        try {
            format = this.database.getAttribute().getAttDataFormat(completeName);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
        final boolean ret = format == AttrDataFormat._SCALAR;

        return ret;

    }

    /**
     * Get the format of the attribute save in database
     *
     * @param completeName the attribute complete name
     * @return the format of the attribute save in database
     * @throws DevFailed
     */
    public int getAttFormatCriterion(final String completeName) throws DevFailed {

        openConnectionIfNecessary();

        try {
            return this.database.getAttribute().getAttDataFormat(completeName);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    /**
     * Get the writable of the attribute save in database
     *
     * @param completeName the attribute complete name
     * @return the writable of the attribute save in database
     * @throws DevFailed
     */
    public int getAttWritableCriterion(final String completeName) throws DevFailed {

        openConnectionIfNecessary();

        try {
            return this.database.getAttribute().getAttDataWritable(completeName);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }

    }

    /**
     * Get the type of the attribute save in database
     *
     * @param completeName the attribute complete name
     * @return the type of the attribute save in database
     * @throws DevFailed
     */
    public int getAttTypeCriterion(final String completeName) throws DevFailed {

        openConnectionIfNecessary();
        try {
            return this.database.getAttribute().getAttDataType(completeName);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    /**
     * Get the archiver load
     *
     * @param archiverName the archiver complete name
     * @param loadType the type of load (all, scalar, spectrum or image)
     * @return the load of the archiver for the required type
     * @throws DevFailed
     */
    public final int getArchiverLoad(final String archiverName, final int loadType) throws DevFailed {
        openConnectionIfNecessary();
        try {
            switch (loadType) {
                case LOAD_ALL:
                    return ArchivingManagerApiRef.get_charge(archiverName);
                case LOAD_SCALAR:
                    return ArchivingManagerApiRef.get_scalar_charge(archiverName);
                case LOAD_SPECTRUM:
                    return ArchivingManagerApiRef.get_spectrum_charge(archiverName);
                case LOAD_IMAGE:
                    return ArchivingManagerApiRef.get_image_charge(archiverName);
                default:
                    throw DevFailedUtils
                            .newDevFailed("Expected LOAD_ALL(0), LOAD_SCALAR(1), LOAD_SPECTRUM(2), LOAD_IMAGE(3) got "
                                    + loadType + " instead.");
            }
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    /**
     * Get the status of the device in charge of the attribute
     *
     * @param attributeName the attribute complete name
     * @return the status of the device in charge of the attribute
     * @throws DevFailed
     */
    public final String getDeviceStatus(final String attributeName) throws DevFailed {
        openConnectionIfNecessary();
        try {
            return TangoAccess.getDeviceState(attributeName);
        } catch (final ArchivingException e) {
            this.logger.error("Error: ", e);
            throw e.toTangoException();
        }
    }

    /**
     * Check if the connection with the database is still alive
     *
     * @return true if the connection is still alive otherwise false
     * @throws DevFailed
     */
    public final boolean isDBConnectionAlive() throws DevFailed {
        boolean alive;
        if (database == null) {
            alive = false;
        } else {
            try {
                openConnectionIfNecessary();
                ConnectionCommands.minimumRequest(connector);
            } catch (final ArchivingException e) {
                this.logger.error("Error: ", e);
                try {
                    openConnectionIfNecessary();
                    ConnectionCommands.minimumRequest(connector);
                } catch (final ArchivingException e1) {
                    this.logger.error("Error: ", e1);
                    throw e1.toTangoException();
                }
            }
            alive = true;
        }
        return alive;
    }

    /**
     * Check if the last insert for the given attribute is NULL
     *
     * @param completeName the attribute complete name
     * @return true if the last insert for the given attribute is NULL otherwise
     *         false
     * @throws DevFailed
     */
    public final boolean isLastValueNull(final String completeName) throws DevFailed {
        openConnectionIfNecessary();
        try {
            return this.database.getDbUtil().isLastDataNull(completeName);
        } catch (final ArchivingException e) {
            this.logger.error("Error: ", e);
            throw e.toTangoException();
        }
    }

    /**
     * Open the connection with the database if it not already opened
     *
     * @throws DevFailed
     */
    public void openConnectionIfNecessary() throws DevFailed {
        if (!manager.isDbConnected()) {
            logger.debug(connector.getParams().toString());
            try {
                manager.archivingConfigureWithoutArchiverListInit();
            } catch (final ArchivingException e) {
                logger.error("error", e);
                throw e.toTangoException();
            }
            database = manager.getDataBase();
            // try {
            // database.getDbConn().setAutoConnect(false);
            // } catch (final ArchivingException e) {
            // logger.error("Error: ", e);
            // throw e.toTangoException();
            // }
        }
    }

    /**
     * Get the database
     *
     * @return @See{DataBaseManager} get the database
     */
    public final DataBaseManager getDatabase() {
        return this.database;
    }

    /**
     * Get the list of all the archivers
     *
     * @return the list of all the archivers
     * @throws DevFailed
     */
    public final List<String> getArchivers() throws DevFailed {
        final List<String> result = new ArrayList<String>();
        openConnectionIfNecessary();
        if (connector != null) {
            PreparedStatement stmt = null;
            Connection conn = null;
            ResultSet res = null;
            /**
             * Query :<br/>
             * SELECT DISTINCT archiver FROM amt
             */
            final StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("SELECT DISTINCT archiver FROM ").append(connector.getSchema()).append(".amt");
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.prepareStatement(queryBuilder.toString(), ResultSet.TYPE_FORWARD_ONLY,
                            ResultSet.CONCUR_READ_ONLY);
                    res = stmt.executeQuery();
                    while (res.next()) {
                        final String next = res.getString(1);
                        if (next != null) {
                            result.add(next);
                        }
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, queryBuilder.toString()).toTangoException();
            } finally {
                DbUtils.closeQuietly(res);
                DbUtils.closeQuietly(stmt);
                DbUtils.closeQuietly(conn);
                connector.closeConnection(conn);
            }
        }
        return result;
    }

    /**
     * Get attributes added on database between two dates
     *
     * @param startDate a date with the format YYYY-MM-DD hh24:mi:ss
     * @param stopDate a date with the format YYYY-MM-DD hh24:mi:ss, it must be
     *            earlier than the first date
     * @return a list of all attribute added between the two dates given in
     *         parameters
     * @throws DevFailed
     */
    public final List<String> newEntryADTBetween(final String startDate, final String stopDate) throws DevFailed {
        final List<String> argout = new ArrayList<String>();
        openConnectionIfNecessary();
        if (connector != null) {
            PreparedStatement stmt = null;
            Connection conn = null;
            ResultSet res = null;
            /**
             * Query :<br/>
             * select FULL_NAME from adt where TIME between
             * to_date('date1','YYYY-MM-DD hh24:mi:ss') AND
             * to_date('date2','YYYY-MM-DD hh24:mi:ss') group by full_name order by
             * full_name ASC
             */
            final StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("select FULL_NAME from ").append(connector.getSchema())
                    .append(".adt where TIME between to_date('").append(startDate).append("','").append(DB_DATE_FORMAT)
                    .append("') AND to_date('").append(stopDate).append("','").append(DB_DATE_FORMAT).append("')");

            queryBuilder.append("  group by full_name order by full_name ASC");
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.prepareStatement(queryBuilder.toString(), ResultSet.TYPE_FORWARD_ONLY,
                            ResultSet.CONCUR_READ_ONLY);
                    res = stmt.executeQuery();
                    while (res.next()) {
                        final String next = res.getString(1);
                        if (next != null) {
                            argout.add(next);
                        }
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, queryBuilder.toString()).toTangoException();
            } finally {
                DbUtils.closeQuietly(res);
                DbUtils.closeQuietly(stmt);
                DbUtils.closeQuietly(conn);
                connector.closeConnection(conn);
            }
        }
        return argout;
    }

    /**
     * Get attributes that have been restarted whithout configuration change
     * between two dates
     *
     * @param startDate a date with the format YYYY-MM-DD hh24:mi:ss
     * @param stopDate a date with the format YYYY-MM-DD hh24:mi:ss, it must be
     *            earlier than the first date
     * @return a Map of all attribute restarted between the two dates given in
     *         parameters with the number of restart
     * @throws DevFailed
     */
    public final Map<String, Integer> attrStartBetween(final String startDate, final String stopDate) throws DevFailed {
        final Map<String, Integer> argout = new HashMap<String, Integer>();
        openConnectionIfNecessary();
        if (connector != null) {
            PreparedStatement stmt = null;
            Connection conn = null;
            ResultSet res = null;
            /**
             * Query :<br/>
             * select full_name, count(full_name) as nbr from adt INNER join (
             * select id from amt where start_date between
             * to_date('date1','YYYY-MM-DD hh24:mi:ss') AND
             * to_date('date2','YYYY-MM-DD hh24:mi:ss') ) tmp2 on tmp2.ID=adt.ID
             * group by full_name order by full_name ASC
             */
            final StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("select full_name, count(full_name) as nbr from ").append(connector.getSchema())
                    .append(".adt INNER join ( select id from ").append(connector.getSchema())
                    .append(".amt where start_date between  to_date('").append(startDate).append("','")
                    .append(DB_DATE_FORMAT).append("') AND to_date('").append(stopDate).append("','")
                    .append(DB_DATE_FORMAT).append("')").append(") tmp2 on tmp2.ID=adt.ID ");

            queryBuilder.append("  group by full_name order by full_name ASC");
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.prepareStatement(queryBuilder.toString(), ResultSet.TYPE_FORWARD_ONLY,
                            ResultSet.CONCUR_READ_ONLY);
                    res = stmt.executeQuery();
                    while (res.next()) {
                        final String attrName = res.getString(1);
                        final int nbrStart = res.getInt(2);
                        if (attrName != null) {
                            argout.put(attrName, nbrStart);
                        }
                    }
                }

            } catch (final SQLException e) {
                throw new ArchivingException(e, queryBuilder.toString()).toTangoException();
            } finally {
                DbUtils.closeQuietly(res);
                DbUtils.closeQuietly(stmt);
                DbUtils.closeQuietly(conn);
                connector.closeConnection(conn);
            }
        }
        return argout;
    }

    /**
     * Get attributes that have their archiving definitely stopped between two
     * dates
     *
     * @param startDate a date with the format YYYY-MM-DD hh24:mi:ss
     * @param stopDate a date with the format YYYY-MM-DD hh24:mi:ss, it must be
     *            earlier than the first date
     * @return a Map of all attribute definitely stopped between the two dates
     *         given in parameters with the number of stop (it's obviously 1 for
     *         all)
     * @throws DevFailed
     */
    public final Map<String, Integer> attrStopBetween(final String startDate, final String stopDate) throws DevFailed {
        final Map<String, Integer> argout = new HashMap<String, Integer>();
        openConnectionIfNecessary();
        if (connector != null) {
            PreparedStatement stmt = null;
            Connection conn = null;
            ResultSet res = null;
            /**
             * Query :<br/>
             * select full_name, count(full_name) from adt INNER JOIN ( select
             * amt.id, max(stop_date) as stop_date FROM amt inner join ( select id,
             * max(start_date) as start_date from amt where start_date <
             * to_date('date2','YYYY-MM-DD hh24:mi:ss') group by(id) ) tmp on tmp.id
             * = amt.id AND amt.stop_date > tmp.start_date where stop_date between
             * to_date() AND to_date('date2','YYYY-MM-DD hh24:mi:ss') group by(
             * amt.id)) tmp2 on tmp2.ID=adt.ID group by full_name order by full_name
             * ASC
             */
            final StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("select full_name, count(full_name) from ").append(connector.getSchema())
                    .append(".adt INNER JOIN ( select amt.id, max(stop_date) as stop_date FROM ")
                    .append(connector.getSchema())
                    .append(".amt inner join ( select id, max(start_date) as start_date from ")
                    .append(connector.getSchema()).append(".amt where start_date < to_date('");
            queryBuilder.append(stopDate);
            queryBuilder.append("','").append(DB_DATE_FORMAT).append(
                    "') group by(id) ) tmp on tmp.id = amt.id AND amt.stop_date > tmp.start_date where stop_date between to_date('");
            queryBuilder.append(startDate).append("','").append(DB_DATE_FORMAT).append("') AND to_date('")
                    .append(stopDate).append("','").append(DB_DATE_FORMAT).append("')");
            queryBuilder.append("group by( amt.id)) tmp2 on tmp2.ID=adt.ID ");

            queryBuilder.append("  group by full_name order by full_name ASC");
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.prepareStatement(queryBuilder.toString(), ResultSet.TYPE_FORWARD_ONLY,
                            ResultSet.CONCUR_READ_ONLY);
                    res = stmt.executeQuery();
                    while (res.next()) {
                        final String attrName = res.getString(1);
                        final int nbrStart = res.getInt(2);
                        if (attrName != null) {
                            argout.put(attrName, nbrStart);
                        }
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, queryBuilder.toString()).toTangoException();
            } finally {
                DbUtils.closeQuietly(res);
                DbUtils.closeQuietly(stmt);
                DbUtils.closeQuietly(conn);
                connector.closeConnection(conn);
            }
        }
        return argout;
    }

    /**
     * Get attributes that have been restarted whith configuration change
     * between two dates
     *
     * @param startDate a date with the format YYYY-MM-DD hh24:mi:ss
     * @param stopDate a date with the format YYYY-MM-DD hh24:mi:ss, it must be
     *            earlier than the first date
     * @return a Map of all attribute restarted between the two dates given in
     *         parameters with the number of restart
     * @throws DevFailed
     */
    public final Map<String, Integer> attrConfChangeBetween(final String startDate, final String stopDate)
            throws DevFailed {
        final Map<String, Integer> argout = new HashMap<String, Integer>();
        openConnectionIfNecessary();
        if (connector != null) {
            PreparedStatement stmt = null;
            Connection conn = null;
            ResultSet res = null;
            /**
             * Query :<br/>
             * select full_name, per_mod, per_per_mod, abs_mod, per_abs_mod,
             * dec_del_abs_mod, gro_del_abs_mod, rel_mod, per_rel_mod,
             * n_percent_rel_mod, p_percent_rel_mod, thr_mod, per_thr_mod,
             * min_val_thr_mod, max_val_thr_mod, cal_mod, per_cal_mod, val_cal_mod,
             * type_cal_mod, algo_cal_mod, dif_mod, per_dif_mod, ext_mod from adt
             * inner join (select id, start_date, per_mod, per_per_mod, abs_mod,
             * per_abs_mod, dec_del_abs_mod, gro_del_abs_mod, rel_mod, per_rel_mod,
             * n_percent_rel_mod, p_percent_rel_mod, thr_mod, per_thr_mod,
             * min_val_thr_mod, max_val_thr_mod, cal_mod, per_cal_mod, val_cal_mod,
             * type_cal_mod, algo_cal_mod, dif_mod, per_dif_mod, ext_mod from amt
             * WHERE START_DATE BETWEEN to_date('2013-01-01 00:00:00','YYYY-MM-DD
             * hh24:mi:ss') AND to_date('2013-06-01 00:00:00','YYYY-MM-DD
             * hh24:mi:ss') ) tmp on tmp.id=adt.id order by start_date
             */
            final StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append(
                    "select   full_name, per_mod,      per_per_mod,      abs_mod,      per_abs_mod,      dec_del_abs_mod,      gro_del_abs_mod,      rel_mod,      per_rel_mod,      n_percent_rel_mod,      p_percent_rel_mod,      thr_mod,      per_thr_mod,      min_val_thr_mod,      max_val_thr_mod,      cal_mod,      per_cal_mod,      val_cal_mod,      type_cal_mod,      algo_cal_mod,      dif_mod,      per_dif_mod,      ext_mod  from ")
                    .append(connector.getSchema())
                    .append(".adt inner join (select id,per_mod, start_date, per_per_mod, abs_mod, per_abs_mod, dec_del_abs_mod, gro_del_abs_mod, rel_mod, per_rel_mod, n_percent_rel_mod, p_percent_rel_mod, thr_mod, per_thr_mod, min_val_thr_mod, max_val_thr_mod, cal_mod, per_cal_mod, val_cal_mod, type_cal_mod, algo_cal_mod, dif_mod, per_dif_mod, ext_mod from ")
                    .append(connector.getSchema()).append(".amt WHERE START_DATE BETWEEN to_date ('");
            queryBuilder.append(startDate).append("','").append(DB_DATE_FORMAT).append("') AND to_date('")
                    .append(stopDate).append("','").append(DB_DATE_FORMAT)
                    .append("')) tmp on tmp.id=adt.id order by start_date");

            final String query = queryBuilder.toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                    res = stmt.executeQuery();
                    final Map<String, List<Mode>> tmpMap = new HashMap<String, List<Mode>>();
                    while (res.next()) {
                        final String attrName = res.getString(1);
                        ModeAbsolu ma = null;
                        try {
                            if (res.getInt("abs_mod") == 1) {
                                ma = new ModeAbsolu(res.getInt("per_abs_mod"), res.getDouble("dec_del_abs_mod"),
                                        res.getDouble("gro_del_abs_mod"));
                            }
                        } catch (final SQLException e) {
                            this.logger.error("Error: {}", e);
                            throw new ArchivingException(e, query).toTangoException();
                        }
                        ModeCalcul mc = null;
                        try {
                            if (res.getInt("cal_mod") == 1) {
                                mc = new ModeCalcul(res.getInt("per_cal_mod"), res.getInt("val_cal_mod"),
                                        res.getInt("type_cal_mod"));
                            }
                        } catch (final SQLException e) {
                            this.logger.error("Error: {}", e);
                            throw new ArchivingException(e, query).toTangoException();
                        }
                        ModeDifference md = null;
                        try {
                            if (res.getInt("dif_mod") == 1) {
                                md = new ModeDifference(res.getInt("per_dif_mod"));
                            }
                        } catch (final SQLException e) {
                            this.logger.error("Error: {}", e);
                            throw new ArchivingException(e, query).toTangoException();
                        }
                        EventMode me = null;
                        try {
                            if (res.getInt("ext_mod") == 1) {
                                me = new EventMode();
                            }
                        } catch (final SQLException e) {
                            this.logger.error("Error: {}", e);
                            throw new ArchivingException(e, query).toTangoException();
                        }
                        ModePeriode mp = null;
                        try {
                            if (res.getInt("per_mod") == 1) {
                                mp = new ModePeriode(res.getInt("per_per_mod"));
                            }
                        } catch (final SQLException e) {
                            this.logger.error("Error: {}", e);
                            throw new ArchivingException(e, query).toTangoException();
                        }
                        ModeRelatif mr = null;
                        try {
                            if (res.getInt("rel_mod") == 1) {
                                mr = new ModeRelatif(res.getInt("per_rel_mod"), res.getDouble("n_percent_rel_mod"),
                                        res.getDouble("p_percent_rel_mod"));
                            }
                        } catch (final SQLException e) {
                            this.logger.error("Error: {}", e);
                            throw new ArchivingException(e, query).toTangoException();
                        }
                        ModeSeuil ms = null;
                        try {
                            if (res.getInt("thr_mod") == 1) {
                                ms = new ModeSeuil(res.getInt("per_thr_mod"), res.getDouble("min_val_thr_mod"),
                                        res.getDouble("max_val_thr_mod"));
                            }
                        } catch (final SQLException e) {
                            this.logger.error("Error: {}", e);
                            throw new ArchivingException(e, query).toTangoException();
                        }
                        final Mode mode = new Mode(mp, ma, mr, ms, mc, md, me);
                        List<Mode> tmpList = tmpMap.get(attrName);
                        if (tmpList == null) {
                            tmpList = new LinkedList<Mode>();
                            tmpMap.put(attrName, tmpList);
                        }
                        tmpList.add(mode);
                    }
                    for (final Entry<String, List<Mode>> e : tmpMap.entrySet()) {
                        int nbrChange = 0;
                        final List<Mode> list = e.getValue();
                        for (int i = 1; i < list.size(); i++) {
                            if (!list.get(i - 1).equals(list.get(i))) {
                                nbrChange++;
                            }
                        }
                        if (nbrChange > 0) {
                            argout.put(e.getKey(), nbrChange);
                        }
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query).toTangoException();
            } finally {
                DbUtils.closeQuietly(res);
                DbUtils.closeQuietly(stmt);
                DbUtils.closeQuietly(conn);
                connector.closeConnection(conn);
            }
        }
        return argout;
    }

    /**
     * Get the date of the last correct insert for an attribute
     *
     * @param attributeFullName the attribute complete name
     * @return the date of the last correct insert for the attribute given in
     *         parameter
     * @throws DevFailed
     */
    public String getLastNotNullInsert(final String attributeFullName) throws DevFailed {
        PreparedStatement stmt = null;
        ResultSet resSet = null;
        Connection conn = null;
        String argout = "unknown";
        if (connector != null) {
            String req = null;
            final int res = this.getAttributeId(attributeFullName);
            if (res <= 0) {
                final StringBuilder error = new StringBuilder();
                error.append("The attribute \"");
                error.append(attributeFullName);
                error.append("\" does not exists.");
                throw DevFailedUtils.newDevFailed(error.toString());
            }
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    final int writable = this.getAttWritableCriterion(attributeFullName);
                    /**
                     * Query :<br/>
                     * SELECT MAX(TIME) AS LAST_OK FROM ATT_X WHERE VALUE IS NOT NULL
                     */
                    final StringBuilder reqBuilder = new StringBuilder("SELECT MAX(TIME) AS LAST_OK  FROM ")
                            .append(connector.getSchema()).append(".ATT_").append(res).append(" WHERE ");
                    if (writable == AttrWriteType._READ || writable == AttrWriteType._WRITE) {
                        reqBuilder.append("value");
                    } else {
                        reqBuilder.append("read_value");
                    }
                    reqBuilder.append(" IS NOT NULL");
                    req = reqBuilder.toString();

                    stmt = conn.prepareStatement(req, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

                    resSet = stmt.executeQuery();
                    if (resSet.next()) {
                        argout = resSet.getString("LAST_OK");
                    }
                    if (argout == null) {
                        throw DevFailedUtils.newDevFailed(attributeFullName + " has never been inserted");
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, req).toTangoException();
            } finally {
                DbUtils.closeQuietly(resSet);
                DbUtils.closeQuietly(stmt);
                DbUtils.closeQuietly(conn);
                connector.closeConnection(conn);
            }
        }
        return argout;
    }

    /**
     * Oracle specific
     *
     * @return
     * @throws DevFailed
     */
    public final List<ArchivedAttribute> getODAInformation() throws DevFailed {
        List<ArchivedAttribute> res = new ArrayList<ArchivedAttribute>();
        if (connector != null) {
            PreparedStatement stmt = null;
            PreparedStatement stmt2 = null;
            ResultSet resSet = null;
            ResultSet resSet2 = null;
            Connection conn = null;
            String req = null;
            final String req2 = "select state from DBA_SCHEDULER_JOBS where job_name='FEEDALIVE_JOB'";
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    // On verifie que ODA n'est pas en train de remplir la table
                    stmt2 = conn.prepareStatement(req2, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                    resSet2 = stmt2.executeQuery();
                    resSet2.next();
                    final String state = resSet2.getString("state");
                    if ("RUNNING".equalsIgnoreCase(state)) {
                        res = null;
                    } else {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("select * from admin.ISALIVED");
                        req = sb.toString();
                        stmt = conn.prepareStatement(req, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                        resSet = stmt.executeQuery();
                        while (resSet.next()) {
                            final String name = resSet.getString("full_name");
                            final String archiver = resSet.getString("archiver");
                            final int id = resSet.getInt("id");
                            final String status = resSet.getString("status");
                            final Boolean isNull = BooleanUtils.toBoolean(resSet.getInt("isnul"));
                            // String lastInsert = resSet.getString("");
                            // int period = resSet.getInt("period");

                            final ArchivedAttribute aa = new ArchivedAttribute(name);
                            aa.setArchiver(archiver);
                            aa.setId(id);
                            aa.setDiagnosis(new DiagnosisInfo(ResDiagnosis.DIS_DIAG));
                            Control control = Control.CONTROL_OK;
                            if (status.equalsIgnoreCase("KO")) {
                                control = Control.CONTROL_KO;
                            } else if (isNull) {
                                control = Control.CONTROL_NULL;
                            }
                            aa.setArchivingStatus(control);
                            res.add(aa);
                        }
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, req).toTangoException();
            } finally {
                DbUtils.closeQuietly(resSet);
                DbUtils.closeQuietly(stmt);
                DbUtils.closeQuietly(resSet2);
                DbUtils.closeQuietly(stmt2);
                DbUtils.closeQuietly(conn);
                connector.closeConnection(conn);
            }
        }
        return res;
    }

    /**
     * Get the archiving configuration (mode & archiver) of all attributes
     * currently archived
     *
     * @return @see{ModeData} the archiving configuration (mode & archiver) of
     *         all attributes currently archived
     * @throws DevFailed
     */
    public final Map<String, ModeData> getModeDataForAll() throws DevFailed {
        ResultSet resSet = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        String req = null;
        final Map<String, ModeData> argout = new HashMap<String, ModeData>();
        if (connector != null) {
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    /**
                     * Query :<br/>
                     * SELECT adt.full_name, amt.archiver, amt.per_mod, amt.per_per_mod,
                     * amt.abs_mod, amt.per_abs_mod, amt.dec_del_abs_mod,
                     * amt.gro_del_abs_mod, amt.rel_mod, amt.per_rel_mod,
                     * amt.n_percent_rel_mod, amt.p_percent_rel_mod, amt.thr_mod,
                     * amt.per_thr_mod, amt.min_val_thr_mod, amt.max_val_thr_mod,
                     * amt.cal_mod, amt.per_cal_mod, amt.val_cal_mod, amt.type_cal_mod,
                     * amt.algo_cal_mod, amt.dif_mod, amt.per_dif_mod, amt.ext_mod FROM
                     * HDB.amt, HDB.adt WHERE (amt.ID = adt.ID) AND amt.stop_date IS
                     * NULL
                     */
                    final StringBuilder sb = new StringBuilder();
                    sb.append(
                            "SELECT adt.full_name, amt.archiver, amt.per_mod, amt.per_per_mod, amt.abs_mod, amt.per_abs_mod, amt.dec_del_abs_mod, amt.gro_del_abs_mod, amt.rel_mod, amt.per_rel_mod, amt.n_percent_rel_mod, amt.p_percent_rel_mod, amt.thr_mod, amt.per_thr_mod, amt.min_val_thr_mod, amt.max_val_thr_mod, amt.cal_mod, amt.per_cal_mod, amt.val_cal_mod, amt.type_cal_mod, amt.algo_cal_mod, amt.dif_mod, amt.per_dif_mod, amt.ext_mod FROM ")
                            .append(connector.getSchema()).append(".amt, ").append(connector.getSchema())
                            .append(".adt WHERE (amt.ID = adt.ID) AND amt.stop_date IS NULL ");
                    req = sb.toString();
                    stmt = conn.prepareStatement(req, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

                    resSet = stmt.executeQuery();
                    while (resSet.next()) {
                        final Mode mode = new Mode();
                        if (resSet.getInt(ConfigConst.PERIODIC_MODE) == 1) {
                            final ModePeriode modePeriode = new ModePeriode(
                                    resSet.getInt(ConfigConst.PERIODIC_MODE_PERIOD));
                            mode.setModeP(modePeriode);
                        }
                        if (resSet.getInt(ConfigConst.ABSOLUTE_MODE) == 1) {
                            final ModeAbsolu modeAbsolu = new ModeAbsolu(
                                    resSet.getInt(ConfigConst.ABSOLUTE_MODE_PERIOD),
                                    resSet.getDouble(ConfigConst.ABSOLUTE_MODE_INF),
                                    resSet.getDouble(ConfigConst.ABSOLUTE_MODE_SUP), false);
                            mode.setModeA(modeAbsolu);
                        } else if (resSet.getInt(ConfigConst.ABSOLUTE_MODE) == 2) {
                            final ModeAbsolu modeAbsolu = new ModeAbsolu(
                                    resSet.getInt(ConfigConst.ABSOLUTE_MODE_PERIOD),
                                    resSet.getDouble(ConfigConst.ABSOLUTE_MODE_INF),
                                    resSet.getDouble(ConfigConst.ABSOLUTE_MODE_SUP), true);
                            mode.setModeA(modeAbsolu);
                        }
                        if (resSet.getInt(ConfigConst.RELATIVE_MODE) == 1) {
                            final ModeRelatif modeRelatif = new ModeRelatif(
                                    resSet.getInt(ConfigConst.RELATIVE_MODE_PERIOD),
                                    resSet.getDouble(ConfigConst.RELATIVE_MODE_INF),
                                    resSet.getDouble(ConfigConst.RELATIVE_MODE_SUP), false);
                            mode.setModeR(modeRelatif);
                        } else if (resSet.getInt(ConfigConst.RELATIVE_MODE) == 2) {
                            final ModeRelatif modeRelatif = new ModeRelatif(
                                    resSet.getInt(ConfigConst.RELATIVE_MODE_PERIOD),
                                    resSet.getDouble(ConfigConst.RELATIVE_MODE_INF),
                                    resSet.getDouble(ConfigConst.RELATIVE_MODE_SUP), true);
                            mode.setModeR(modeRelatif);
                        }
                        if (resSet.getInt(ConfigConst.THRESHOLD_MODE) == 1) {
                            final ModeSeuil modeSeuil = new ModeSeuil(resSet.getInt(ConfigConst.THRESHOLD_MODE_PERIOD),
                                    resSet.getDouble(ConfigConst.THRESHOLD_MODE_INF),
                                    resSet.getDouble(ConfigConst.THRESHOLD_MODE_SUP));
                            mode.setModeT(modeSeuil);
                        }
                        if (resSet.getInt(ConfigConst.CALC_MODE) == 1) {
                            final ModeCalcul modeCalcul = new ModeCalcul(resSet.getInt(ConfigConst.CALC_MODE_PERIOD),
                                    resSet.getInt(ConfigConst.CALC_MODE_VAL),
                                    resSet.getInt(ConfigConst.CALC_MODE_TYPE));
                            mode.setModeC(modeCalcul);
                            // Warning Field 18 is not used yet ...
                        }
                        if (resSet.getInt(ConfigConst.DIFF_MODE) == 1) {
                            final ModeDifference modeDifference = new ModeDifference(
                                    resSet.getInt(ConfigConst.DIFF_MODE_PERIOD));
                            mode.setModeD(modeDifference);
                        }
                        if (resSet.getInt(ConfigConst.EXT_MODE) == 1) {
                            final EventMode eventMode = new EventMode();
                            mode.setEventMode(eventMode);
                        }

                        final ModeData modeData = new ModeData();
                        modeData.setMode(mode);
                        modeData.setArchiver(resSet.getString(ConfigConst.ARCHIVER));
                        argout.put(resSet.getString("full_name"), modeData);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, req).toTangoException();
            } finally {
                DbUtils.closeQuietly(resSet);
                DbUtils.closeQuietly(stmt);
                DbUtils.closeQuietly(conn);
                connector.closeConnection(conn);
            }
        }
        return argout;
    }
}

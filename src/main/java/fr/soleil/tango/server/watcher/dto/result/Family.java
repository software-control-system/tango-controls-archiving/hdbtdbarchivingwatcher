package fr.soleil.tango.server.watcher.dto.result;

import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;

/**
 * An implementation of {@link AbstractElement} for a tango family
 *
 * @author fourneau
 *
 */
public class Family extends AbstractElement {

    /**
     * Constructor
     *
     * @param name
     *            the name of the family
     */
    public Family(final String name) {
        super(name);
    }

    /**
     * Add a KO attribute to this group only if the family of the attribute
     * corresponding (ie : is equals ) to the name of the family
     *
     * @param attribute
     *            the ArchivedAttribute of the attribute
     */
    @Override
    public final void addKOAttribute(final ArchivedAttribute attribute) {
        if (attribute != null && attribute.getFamily().equalsIgnoreCase(this.getName())) {
            this.getKOAttributes().add(attribute.getCompleteName());
        }
    }

    @Override
    public final void addNullAttribute(final ArchivedAttribute attribute) {
        if (attribute != null && attribute.getFamily().equalsIgnoreCase(this.getName())) {
            getNullAttributes().add(attribute.getCompleteName());
        }
    }
}

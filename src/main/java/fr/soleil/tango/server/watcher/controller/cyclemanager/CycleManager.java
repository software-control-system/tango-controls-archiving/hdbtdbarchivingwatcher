package fr.soleil.tango.server.watcher.controller.cyclemanager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.tango.server.watcher.controller.AttributeSelection;
import fr.soleil.tango.server.watcher.controller.Controller;
import fr.soleil.tango.server.watcher.controller.Controller.Control;
import fr.soleil.tango.server.watcher.controller.diagnosis.Diagnosis;
import fr.soleil.tango.server.watcher.controller.diagnosis.DiagnosisInfo;
import fr.soleil.tango.server.watcher.controller.diagnosis.ResDiagnosis;
import fr.soleil.tango.server.watcher.db.DBReader;
import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;
import fr.soleil.tango.server.watcher.dto.attribute.ModeData;

/**
 * Execute cycle to periodicly check archived attribute from their real value
 *
 * @author fourneau
 *
 */
public class CycleManager extends AdapterCycleManager {

    private static final int RETRY_EVERY_ATTR = 500;

    private final Logger logger = LoggerFactory.getLogger(CycleManager.class);
    /**
     * A pause between each attribute check for not overload the database
     */
    private final long unitaryPause;

    /**
     * @see Controller
     */
    private final Controller controller;

    /**
     * Constructor
     *
     * @param dbReader
     * @param unitaryPause
     * @param cyclePause
     * @param doDiagnosis
     * @param doArchiverDiagnosis
     * @param doRetry
     * @param attributeSelection
     * @param safetyPeriod
     */
    public CycleManager(final DBReader dbReader, final long unitaryPause, final boolean doDiagnosis,
            final boolean doArchiverDiagnosis, final boolean doRetry, final AttributeSelection attributeSelection,
            final int safetyPeriod, final boolean checkLastIsert) {
        super(dbReader, doDiagnosis, doArchiverDiagnosis, doRetry, attributeSelection, safetyPeriod, checkLastIsert);
        this.controller = new Controller(this.dbReader, safetyPeriod);
        this.unitaryPause = unitaryPause;

    }

    /**
     * Execute cycles.<br/>
     *
     * A cycle is :
     * <ul>
     * <li>Select attribute to monitor ({@link AttributeSelection})</li>
     * <li>For each attribute :
     * <ul>
     * <li>Execute the control ({@link Controller})</li>
     * <li>Try to get the diagnosis from attributes ({@link Diagnosis})</li>
     * <li>Unitary pause</li>
     * </ul>
     * </li>
     * <li>Do retry for KO archiver</li>
     * <li>Cycle pause</li>
     * </ul>
     */
    @Override
    public final void run() {
        cycleStopTime = 0;
        cycleStartTime = System.currentTimeMillis();
        logger.info("CycleManager - new cycle started");
        try {
            logger.info("cycle manager get attributes to monitor");
            attributes = attributeSelection.getAttributesToMonitor();
            errorMap.remove("AttributesToMonitor");
        } catch (final DevFailed e) {
            DevFailedUtils.logDevFailed(e, logger);
            errorMap.put("AttributesToMonitor", DevFailedUtils.toString(e));
        }
        try {
            putAllAttributesToResult();
            errorMap.remove("AllAttributes");
        } catch (final DevFailed e) {
            DevFailedUtils.logDevFailed(e, logger);
            errorMap.put("AllAttributes", DevFailedUtils.toString(e));
        }
        // this.result.addAllAttribute(attributes);
        nbrAttrMonitoredCurrentCycle = 0;

        logger.info("start checking attributes");
        if (attributes != null) {
            for (final String attribute : attributes) {
                try {
                    final long start = System.currentTimeMillis();
                    checkArchivingForAttribute(attribute);
                    errorMap.remove(attribute);
                    final long duration = System.currentTimeMillis() - start;
                    final long sleep = duration >= unitaryPause ? 0 : unitaryPause - duration;
                    nbrAttrMonitoredCurrentCycle++;
                    // retry launch archiving every N attributes
                    if (nbrAttrMonitoredCurrentCycle % RETRY_EVERY_ATTR == 0) {
                        retryForKOAttributes();
                    }
                    Thread.sleep(sleep);
                } catch (final InterruptedException e) {
                    logger.info("CycleManager interrupted");
                    Thread.currentThread().interrupt();
                    break;
                }
            }
        }
        cycleStopTime = System.currentTimeMillis();
        endCycle++;
        logger.info("end checking attributes");
        retryForKOAttributes();
    }

    private void retryForKOAttributes() {
        if (doRetry && !Thread.currentThread().isInterrupted()) {
            try {
                logger.info("sending retry for KO attributes");
                controller.doRetry(result.getKoArchivers().values(), result.getKOAttributesMap());
                logger.info("send retry done");
                errorMap.remove(DO_RETRY);
            } catch (final DevFailed e) {
                DevFailedUtils.logDevFailed(e, logger);
                errorMap.put(DO_RETRY, DevFailedUtils.toString(e));
            }
        }
    }

    /**
     * Execute the control of an attribute and add it to the result if it is KO
     * or NULL, or logged the error if the control has failed or try to remove
     * the attribute to the result if the control is OK
     *
     * @param attribute
     *            the attribute name
     * @throws DevFailed
     * @throws InterruptedException
     */
    public synchronized Control checkArchivingForAttribute(final String attribute) {
        Control control = Control.CONTROL_KO;
        try {
            final ModeData modeData = result.getAllAttributes().get(attribute);
            final Control res = controller.control(attribute, modeData);
            control = res;
            logger.debug("{} is {}", attribute, res);
            if (res.equals(Control.CONTROL_KO) || res.equals(Control.CONTROL_NULL)) {
                // attribute archiving is NULL or KO
                final ArchivedAttribute a = result.addNewAttributeResult(attribute, res,
                        dbReader.getAttributeId(attribute), doDiagnosis);
                if (doDiagnosis) {
                    diagnosis.addAttribute(a, res);
                }
                errorMap.remove(attribute);
            } else {
                // archiving OK, remove attribute from failed attributes
                final ArchivedAttribute aa = new ArchivedAttribute(attribute);
                if (modeData != null) {
                    aa.setArchiver(modeData.getArchiver());
                }
                result.deleteKoAttribute(aa);
                // remove the attribute if it was on the queue
                if (doDiagnosis) {
                    diagnosis.removeAttribute(aa);
                }
            }
        } catch (final ArchivingException e) {
            // impossible to get archiving status
            if (doDiagnosis) {
                result.addKoDiagnosis(attribute, new DiagnosisInfo(ResDiagnosis.FAIL_DIAG, e.getMessage(), ""));
            }
            errorMap.put(attribute, "The control of the attribute failed.");
        } catch (final InterruptedException e) {
            logger.warn("execution interrupted", e);
            Thread.currentThread().interrupt();
        } catch (final DevFailed e) {
            if (doDiagnosis) {
                result.addKoDiagnosis(attribute, new DiagnosisInfo(ResDiagnosis.FAIL_DIAG, e.getMessage(), ""));
            }
            errorMap.put(attribute, "The control of the attribute failed.");
        }
        return control;
    }
}

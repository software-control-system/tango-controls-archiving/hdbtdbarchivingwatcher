/*
 * Synchrotron Soleil
 * 
 * File : ModeData.java
 * 
 * Project : archiving_watcher
 * 
 * Description :
 * 
 * Author : CLAISSE
 * 
 * Original : 28 nov. 2005
 * 
 * Revision: Author:
 * Date: State:
 * 
 * Log: ModeData.java,v
 */
/*
 * Created on 28 nov. 2005
 * 
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.tango.server.watcher.dto.attribute;

import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;

/**
 * Models a line from the TDB/HDB table AMT. Describes a given attribute's
 * archiving informations:
 * <UL>
 * <LI>Its modes
 * <LI>The name of the archiver in charge of this attribute
 * </UL>
 * 
 * @author FOURNEAU
 */
public class ModeData {

    /**
     * The mode of archiving
     */
    private Mode mode;
    private String archiver;

    /**
     * Default constructor
     */
    public ModeData() {
        super();
    }

    /**
     * @return Returns the archiver.
     */
    public final String getArchiver() {
        return this.archiver;
    }

    /**
     * @param archiver
     *            The archiver to set.
     */
    public final void setArchiver(String archiver) {
        this.archiver = archiver;
    }

    /**
     * @return Returns the mode.
     */
    public final Mode getMode() {
        return this.mode;
    }

    /**
     * @param mode
     *            The mode to set.
     */
    public final void setMode(Mode mode) {
        this.mode = mode;
    }
}

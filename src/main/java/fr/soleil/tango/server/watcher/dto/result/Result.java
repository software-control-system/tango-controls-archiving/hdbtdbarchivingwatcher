package fr.soleil.tango.server.watcher.dto.result;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.soleil.tango.clientapi.TangoGroupAttribute;
import fr.soleil.tango.server.watcher.controller.Controller.Control;
import fr.soleil.tango.server.watcher.controller.diagnosis.DiagnosisInfo;
import fr.soleil.tango.server.watcher.controller.diagnosis.ResDiagnosis;
import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;
import fr.soleil.tango.server.watcher.dto.attribute.ModeData;

public class Result {
    /**
     * The logger
     */
    private final Logger logger = LoggerFactory.getLogger(Result.class);
    /**
     * An ordered map for save each archiver depending on their name
     */
    private final Map<String, Archiver> koArchivers = Collections.synchronizedMap(new TreeMap<String, Archiver>(
            String.CASE_INSENSITIVE_ORDER));
    /**
     * An ordered map for save each family depending on their name
     */
    private final Map<String, Family> koFamilies = Collections.synchronizedMap(new TreeMap<String, Family>(
            String.CASE_INSENSITIVE_ORDER));
    /**
     * An ordered map for save each domain depending on their name
     */
    private final Map<String, Domain> koDomains = Collections.synchronizedMap(new TreeMap<String, Domain>(
            String.CASE_INSENSITIVE_ORDER));
    /**
     * An ordered map for save each attribute depending on their name
     */
    private final Map<String, ArchivedAttribute> koAttributes = Collections
            .synchronizedMap(new TreeMap<String, ArchivedAttribute>(String.CASE_INSENSITIVE_ORDER));

    /**
     * An ordered map for save each archiver depending on their name
     */
    private final Map<String, Archiver> nullArchivers = Collections.synchronizedMap(new TreeMap<String, Archiver>(
            String.CASE_INSENSITIVE_ORDER));
    /**
     * An ordered map for save each family depending on their name
     */
    private final Map<String, Family> nullFamilies = Collections.synchronizedMap(new TreeMap<String, Family>(
            String.CASE_INSENSITIVE_ORDER));
    /**
     * An ordered map for save each domain depending on their name
     */
    private final Map<String, Domain> nullDomains = Collections.synchronizedMap(new TreeMap<String, Domain>(
            String.CASE_INSENSITIVE_ORDER));
    /**
     * An ordered map for save each attribute depending on their name
     */
    private final Map<String, ArchivedAttribute> nullAttributes = Collections
            .synchronizedMap(new TreeMap<String, ArchivedAttribute>(String.CASE_INSENSITIVE_ORDER));
    /**
     * An ordered map for save each attribute and their archiving mode depending
     * on their name
     */
    private Map<String, ModeData> attributes = new TreeMap<String, ModeData>(String.CASE_INSENSITIVE_ORDER);

    /**
     * If set to true, the archivers part of the generated control report will
     * contain specific archiver information such as scalar load etc..<br>
     * If set to false, those details will remain blank.
     */
    private final boolean doArchiverDiagnosis;

    /**
     * Constructor
     *
     * @param doArchiverDiagnosis
     */
    public Result(final boolean doArchiverDiagnosis) {
        this.doArchiverDiagnosis = doArchiverDiagnosis;
    }

    /**
     * Get a Set of KO attributes
     *
     * @return Set<String> of KO attributes
     */
    public final Set<String> getKoAttributes() {
        return this.koAttributes.keySet();
    }

    /**
     * Get a Set of NULL attributes
     *
     * @return Set<String> of NULL attributes
     */
    public final Set<String> getNullAttributes() {
        return this.nullAttributes.keySet();
    }

    /**
     * Get a Map<String,ArchivingAttribute> of all KO attributes
     *
     * @return a Map<String,ArchivingAttribute> of all KO attributes
     */
    public final Map<String, ArchivedAttribute> getKOAttributesMap() {
        return this.koAttributes;
    }

    public ArchivedAttribute addNewAttributeResult(final String attr, final Control control, final int attId,
            final boolean isDiagEnable) {
        final ArchivedAttribute a = new ArchivedAttribute(attr);
        final ModeData md = attributes.get(attr);
        a.setArchiver(md.getArchiver());
        a.setArchivingStatus(control);
        a.setId(attId);
        if (control.equals(Control.CONTROL_KO)) {
             addKoAttribute(a);
        } else if (control.equals(Control.CONTROL_NULL)) {
            addNullAttribute(a);
        }
        if (!isDiagEnable) {
            a.setDiagnosis(new DiagnosisInfo(ResDiagnosis.DIS_DIAG));
        }
        return a;
    }

    /**
     * Add an null attribute to the result and so to the dffrent report
     *
     * @param attribute
     *            the full attribute name
     */
    public final void addNullAttribute(final ArchivedAttribute attribute) {
        final String domainName = attribute.getDomain();
        final String familyName = attribute.getFamily();
        final String archiverName = attribute.getArchiver();
        final ArchivedAttribute aa = this.nullAttributes.get(attribute.getCompleteName());

        if (this.nullArchivers.containsKey(archiverName)) {
            this.nullArchivers.get(archiverName).addNullAttribute(attribute);
        } else {
            final Archiver archiver = new Archiver(archiverName, this.doArchiverDiagnosis);
            archiver.addNullAttribute(attribute);
            this.nullArchivers.put(archiverName, archiver);
        }

        if (this.nullFamilies.containsKey(familyName)) {
            this.nullFamilies.get(familyName).addNullAttribute(attribute);
        } else {
            final Family familly = new Family(familyName);
            familly.addNullAttribute(attribute);
            this.nullFamilies.put(familyName, familly);
        }

        if (this.nullDomains.containsKey(domainName)) {
            this.nullDomains.get(domainName).addNullAttribute(attribute);
        } else {
            final Domain domain = new Domain(domainName);
            domain.addNullAttribute(attribute);
            this.nullDomains.put(domainName, domain);
        }
        if (aa != null) {
            attribute.setDiagnosis(aa.getDiagnosis());
        }
        this.nullAttributes.put(attribute.getCompleteName(), attribute);
    }

    /**
     * Add an KO attribute to the result and so to the dffrent report
     *
     * @param attribute
     *            the full attribute name
     */
    public final void addKoAttribute(final ArchivedAttribute attribute) {
        final String domainName = attribute.getDomain();
        final String familyName = attribute.getFamily();
        final String archiverName = attribute.getArchiver();
        final ArchivedAttribute aa = this.koAttributes.get(attribute.getCompleteName());

        if (this.koArchivers.containsKey(archiverName)) {
            this.koArchivers.get(archiverName).addKOAttribute(attribute);
        } else {
            final Archiver archiver = new Archiver(archiverName, this.doArchiverDiagnosis);
            archiver.addKOAttribute(attribute);
            this.koArchivers.put(archiverName, archiver);
        }

        if (this.koFamilies.containsKey(familyName)) {
            this.koFamilies.get(familyName).addKOAttribute(attribute);
        } else {
            final Family familly = new Family(familyName);
            familly.addKOAttribute(attribute);
            this.koFamilies.put(familyName, familly);
        }

        if (this.koDomains.containsKey(domainName)) {
            this.koDomains.get(domainName).addKOAttribute(attribute);
        } else {
            final Domain domain = new Domain(domainName);
            domain.addKOAttribute(attribute);
            this.koDomains.put(domainName, domain);
        }
        if (aa != null) {
            attribute.setDiagnosis(aa.getDiagnosis());
        }
        this.koAttributes.put(attribute.getCompleteName(), attribute);
    }

    /**
     * Add the diagnosis to an attribute
     *
     * @param attribute
     *            the full attribute name
     * @param diag
     *            the diagnosis of the attribute
     */
    public final void addKoDiagnosis(final String attribute, final DiagnosisInfo diag) {
        final ArchivedAttribute aa = this.koAttributes.get(attribute);
        if (aa != null) {
            aa.setDiagnosis(diag);
            this.koAttributes.put(attribute, aa);
        }
    }

    public final void addNullDiagnosis(final String attribute, final DiagnosisInfo diag) {
        final ArchivedAttribute aa = this.nullAttributes.get(attribute);
        if (aa != null) {
            aa.setDiagnosis(diag);
            this.nullAttributes.put(attribute, aa);
        }
    }

    /**
     * True is the attribute is KO, otherwise false
     *
     * @param attrName
     *            the full attribute name
     * @return true is the attribute is KO, otherwise false
     */
    public final boolean isKOAttribute(final String attrName) {
        return this.koAttributes.containsKey(attrName);
    }

    /**
     * Try to delete an attribute from the result
     *
     * @param attribute
     *            the full attribute name
     */
    public final void deleteKoAttribute(final ArchivedAttribute attribute) {
        final String completeName = attribute.getCompleteName();
        final String domainName = attribute.getDomain();
        final String familyName = attribute.getFamily();
        final String archiverName = attribute.getArchiver();

        if (this.koAttributes.remove(completeName) != null) {
            this.koArchivers.get(archiverName).removeKOAttribute(completeName);
            if (this.koArchivers.get(archiverName).isKOEmpty()) {
                this.koArchivers.remove(archiverName);
            }
            this.koDomains.get(domainName).removeKOAttribute(completeName);
            if (this.koDomains.get(domainName).isKOEmpty()) {
                this.koDomains.remove(domainName);
            }
            this.koFamilies.get(familyName).removeKOAttribute(completeName);
            if (this.koFamilies.get(familyName).isKOEmpty()) {
                this.koFamilies.remove(familyName);
            }
        }
        if (this.nullAttributes.remove(completeName) != null) {
            this.nullArchivers.get(archiverName).removeNullAttribute(completeName);
            if (this.nullArchivers.get(archiverName).isNullEmpty()) {
                this.nullArchivers.remove(archiverName);
            }
            this.nullDomains.get(domainName).removeNullAttribute(completeName);
            if (this.nullDomains.get(domainName).isNullEmpty()) {
                this.nullDomains.remove(domainName);
            }
            this.nullFamilies.get(familyName).removeNullAttribute(completeName);
            if (this.nullFamilies.get(familyName).isNullEmpty()) {
                this.nullFamilies.remove(familyName);
            }
        }
    }

    /**
     * Get the number of attributes monitored
     *
     * @return get the number of attributes monitored
     */
    // public final int getNumberOfAttrMonitored() {
    // return this.attributes.size();
    // }

    /**
     * Get the number of KO attributes
     *
     * @return the number of KO attributes
     */
    public final int getNumberOfKoAttributes() {
        return this.koAttributes.size();
    }

    /**
     * Get the number of NULL attributes
     *
     * @return the number of NULL attributes
     */
    public final int getNumberOfNullAttributes() {
        return this.nullAttributes.size();
    }

    /**
     * Merge two images (String[][] on one image (String[])
     *
     * @param a1
     *            String[][]
     * @param a2
     *            String[][]
     * @return null if a1 and a2 are null<br/>
     *         a1 if a2 is null<br/>
     *         a2 if a1 is null</br> a new array with length = a1.length +
     *         a2.length and containing all the values from a1 and a2
     *
     */
    public static final String[][] deepMerge(final String[][] a1, final String[][] a2) {
        if (a1 == null && a2 == null) {
            return null;
        }
        if (a1 == null && a2 != null) {
            return a2;
        }
        if (a1 != null && a2 == null) {
            return a1;
        }

        final int totalSize = a1.length + a2.length;
        final String[][] result = new String[totalSize][];
        int i = 0;
        for (; i < a1.length; i++) {
            result[i] = Arrays.copyOf(a1[i], a1[i].length);
        }
        for (int j = 0; j < a2.length; i++, j++) {
            result[i] = Arrays.copyOf(a2[j], a2[j].length);
        }
        return result;
    }

    /**
     * Move an array[X][Y] to an array[Y][x]
     *
     * @param an
     *            Array[X][Y]
     * @return an Array[Y][X]
     */
    public static final String[][] changePositioning(final String[][] basis) {
        int jMax = 0;
        for (int i = 0; i < basis.length; i++) {
            if (basis[i].length > jMax) {
                jMax = basis[i].length;
            }
        }
        final String[][] argout = new String[jMax][basis.length];
        for (int i = 0; i < basis.length; i++) {
            int j = 0;
            for (; j < basis[i].length; j++) {
                argout[j][i] = basis[i][j];
            }
            while (j < jMax) {
                argout[j++][i] = "";
            }
        }
        return argout;
    }

    /**
     * Fill the null value of an Array[][] with empty string (ie : "")
     *
     * @param base
     *            an Array[][]
     * @return a new Array[][] without null values
     */
    public static final String[][] fillBlank(final String[][] base) {
        int jMax = 0;
        for (int i = 0; i < base.length; i++) {
            if (base[i].length > jMax) {
                jMax = base[i].length;
            }
        }
        final String[][] argout = new String[base.length][jMax];
        for (int i = 0; i < base.length; i++) {
            int j = 0;
            for (; j < base[i].length; j++) {
                argout[i][j] = base[i][j];
            }
            for (; j < jMax; j++) {
                argout[i][j] = "";
            }
        }
        return argout;
    }

    /**
     * Get a simple report per domain.<br/>
     * Exemple :<br/>
     * [0][0] : domain1 | [0][1] : domain2<br/>
     * [1][0] : attr11 | [1][1] : attr21<br/>
     * [2][0] : attr12 | [2][1] : attr22<br/>
     * ...<br/>
     *
     * @return a simple report per domain.
     */
    public final String[][] getSimpleReportPerDomain() {
        final String[][] report = new String[this.koDomains.size() + nullDomains.size()][];
        int i = 0;
        for (final Domain domain : this.koDomains.values()) {
            report[i++] = domain.getSimpleReport();
        }
        for (final Domain domain : this.nullDomains.values()) {
            report[i++] = domain.getSimpleReport();
        }
        return changePositioning(report);
    }

    /**
     * Get a simple report per archiver.<br/>
     * Exemple :<br/>
     * [0][0] : archiver1 | [0][1] : archiver2<br/>
     * [1][0] : attr11 | [1][1] : attr21<br/>
     * [2][0] : attr12 | [2][1] : attr22<br/>
     * ...<br/>
     *
     * @return a simple report per archiver.
     */
    public final String[][] getSimpleReportPerArchiver() {
        final List<Archiver> archivers = new ArrayList<Archiver>();
        archivers.addAll(this.koArchivers.values());
        archivers.addAll(this.nullArchivers.values());
        // get archivers load
        final String[][] report = new String[archivers.size()][];
        int i = 0;
        String[] attributesToCheck;
        short[] charges;
        if (doArchiverDiagnosis) {
            attributesToCheck = new String[archivers.size() * 3];
            charges = new short[attributesToCheck.length];
            for (final Archiver archiver : archivers) {
                final String archiverName = archiver.getName();
                attributesToCheck[3 * i] = archiverName + "/scalar_charge";
                attributesToCheck[3 * i + 1] = archiverName + "/spectrum_charge";
                attributesToCheck[3 * i + 2] = archiverName + "/image_charge";
                i++;
            }
            try {
                final TangoGroupAttribute groupAttribute = new TangoGroupAttribute(false, attributesToCheck);
                final DeviceAttribute[] attrs = groupAttribute.getReadAsyncReplies();
                for (int index = 0; index < attrs.length; index++) {
                    final DeviceAttribute attr = attrs[index];
                    if (attr == null) {
                        charges[index] = 0;
                    } else {
                        try {
                            charges[index] = attr.extractShort();
                        } catch (final DevFailed df) {
                            this.logger.error("Failed to read " + attributesToCheck[index], df);
                            charges[index] = 0;
                        }
                    }
                }
            } catch (final DevFailed df) {
                this.logger.error("Failed to check archivers load", df);
            }
            i = 0;
        } else {
            attributesToCheck = null;
            charges = null;
        }
        for (final Archiver archiver : archivers) {
            if (this.doArchiverDiagnosis) {
                final int scalarLoad = charges[3 * i];
                final int spectrumLoad = charges[3 * i + 1];
                final int imageLoad = charges[3 * i + 2];
                final int totalLoad = scalarLoad + spectrumLoad + imageLoad;
                archiver.setScalarLoad(scalarLoad);
                archiver.setSpectrumLoad(spectrumLoad);
                archiver.setImageLoad(imageLoad);
                archiver.setTotalLoad(totalLoad);
            }
            report[i++] = archiver.getSimpleReport();
        }
        return changePositioning(report);
    }

    /**
     * Get a simple report per family.<br/>
     * Exemple :<br/>
     * [0][0] : family1 | [0][1] : family2<br/>
     * [1][0] : attr11 | [1][1] : attr21<br/>
     * [2][0] : attr12 | [2][1] : attr22<br/>
     * ...<br/>
     *
     * @return a simple report per family.
     */
    public final String[][] getSimpleReportPerFamily() {
        final String[][] report = new String[this.koFamilies.size() + nullFamilies.size()][];
        int i = 0;
        for (final Family family : this.koFamilies.values()) {
            report[i++] = family.getSimpleReport();
        }
        for (final Family family : this.nullFamilies.values()) {
            report[i++] = family.getSimpleReport();
        }
        return changePositioning(report);
    }

    /**
     * Sort a map by its values
     *
     * @param map
     *            The map to sort
     * @return A Set of Entry of value / key
     */
    private static <K, V extends Comparable<? super V>> SortedSet<Map.Entry<K, V>> entriesSortedByValues(
            final Map<K, V> map) {
        final SortedSet<Map.Entry<K, V>> sortedEntries = new TreeSet<Map.Entry<K, V>>(
                new Comparator<Map.Entry<K, V>>() {
                    @Override
                    public int compare(final Map.Entry<K, V> e1, final Map.Entry<K, V> e2) {
                        final int res = e1.getValue().compareTo(e2.getValue());
                        return res != 0 ? res : 1; // Special fix to preserve
                        // items with equal values
                    }
                });
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

    /**
     * Get a simple report per attribut.<br/>
     * Exemple :<br/>
     * [0][0] : attr1 | [0][1] : {@link DiagnosisInfo}1<br/>
     * [1][0] : attr2 | [1][1] : {@link DiagnosisInfo}2<br/>
     * [2][0] : attr3 | [2][1] : {@link DiagnosisInfo}3<br/>
     * ...<br/>
     *
     * @return a simple report per attribut.
     */
    public final String[][] getSimpleReportPerAttribute(final boolean sorted) {
        Set<Map.Entry<String, ArchivedAttribute>> tmpSet;
        if (sorted) {
            final Map<String, ArchivedAttribute> m = new TreeMap<String, ArchivedAttribute>(
                    String.CASE_INSENSITIVE_ORDER);
            m.putAll(this.koAttributes);
            m.putAll(nullAttributes);
            tmpSet = entriesSortedByValues(m);
        } else {
            final Map<String, ArchivedAttribute> m = new TreeMap<String, ArchivedAttribute>(
                    String.CASE_INSENSITIVE_ORDER);
            m.putAll(this.koAttributes);
            m.putAll(nullAttributes);
            tmpSet = m.entrySet();
        }
        final int size = tmpSet.size();
        final String[][] result = new String[size][3];
        int i = 0;
        for (final Entry<String, ArchivedAttribute> e : tmpSet) {
            final ArchivedAttribute value = e.getValue();
            final StringBuilder sb = new StringBuilder(e.getKey()).append(" (id:").append(value.getId()).append(")");
            result[i][0] = sb.toString();
            result[i][1] = value.getArchiver();
            result[i][2] = value.getDiagnosis().toString();
            i++;
        }
        return result;
    }

    /**
     * Get a global report composed to :
     * <ul>
     * <li>Simple report per {@see Domain}</li>
     * <li>Simple report per {@see Archiver}</li>
     * <li>Simple report per {@see Family}</li>
     * </ul>
     *
     * @return
     */
    public final String[][] getSimpleGlobalReport() {
        final String[][] domains = this.getSimpleReportPerDomain();
        final String[][] archivers = this.getSimpleReportPerArchiver();
        final String[][] families = this.getSimpleReportPerFamily();

        return fillBlank(deepMerge(deepMerge(archivers, domains), families));
    }

    /**
     * Get the name of tarchivers that get attributs KO and their associated
     * object {@link Archiver}
     *
     * @return a Map<br/>
     *         keys : archiver name<br/>
     *         value : associated object {@link Archiver}<br/>
     */
    public final Map<String, Archiver> getKoArchivers() {
        return this.koArchivers;
    }

    /**
     * Export the global report to a file
     *
     * @param filename
     *            the filename to export the report
     * @throws DevFailed
     */
    public final void exportToFile(final String filename) throws DevFailed {
        BufferedWriter bw = null;
        try {
            final File file = new File(filename);
            // if file doesnt exists, then create it
            if (!file.exists()) {
                if (!file.createNewFile()) {
					throw DevFailedUtils.newDevFailed(new StringBuilder("The file ").append(filename)
                            .append(" already exists").toString());
                }
            }
            final FileWriter fw = new FileWriter(file.getAbsoluteFile(), false);
            bw = new BufferedWriter(fw);
            bw.write(Arrays.toString(this.getSimpleGlobalReport()));
        } catch (final IOException e) {
			throw DevFailedUtils.newDevFailed(e.getMessage());
        } finally {
            try {
                bw.close();
            } catch (final IOException e) {
                this.logger.error("Unable to close file {} ", filename);
            }
        }
    }

    /**
     * Put all the attribute archived on the database and their archiving mode
     *
     * @param finalMap
     */
    public final void putAllAttributes(final Map<String, ModeData> finalMap) {
        this.attributes = finalMap;
    }

    /**
     * Get all the attribute archived on the database and their archiving mode
     *
     * @return all the attribute archived on the database and their archiving
     *         mode
     */
    public final Map<String, ModeData> getAllAttributes() {
        return this.attributes;
    }
}

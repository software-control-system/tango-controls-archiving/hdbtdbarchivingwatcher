package fr.soleil.tango.server.watcher.dto.result;

import java.util.Collection;

import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;

/**
 * An interface for group tango attribute in function of a comparison point
 *
 * @author fourneau
 *
 */
public interface IElement {
    /**
     * Set the name of the group
     *
     * @param name
     *            the name of the group
     */
    void setName(final String name);

    /**
     * Get the name of the group
     *
     * @return the name of the group
     */
    String getName();

    /**
     * Get a simple formated report
     *
     * @return a simple formated report
     */
    String[] getSimpleReport();

    /**
     * Add a KO attribute to this group
     *
     * @param attribute
     *            the ArchivedAttribute of the attribute
     */
    void addKOAttribute(final ArchivedAttribute attribute);

    void addNullAttribute(final ArchivedAttribute attribute);

    /**
     * Remove an attribute to this group
     *
     * @param attribute
     *            the name of the attribute to remove
     */
    void removeKOAttribute(final String attribute);

    /**
     * Get all attribtues of the group
     *
     * @return a Collection<String> of all attributes of the group
     */
    Collection<String> getKOAttributes();

    /**
     * Returns true if, and only if, there is no attribute on this group
     *
     * @return true if there is no attribute on this group, otherwise false
     */
    // boolean isEmpty();

    Collection<String> getNullAttributes();

    void removeNullAttribute(String attribute);

    boolean isKOEmpty();

    boolean isNullEmpty();
}

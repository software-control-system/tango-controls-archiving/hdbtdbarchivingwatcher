package fr.soleil.tango.server.watcher.notification;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.TangoGroupCommand;

public class TextTalkerNotification implements INotification {
    private final Logger logger = LoggerFactory.getLogger(TextTalkerNotification.class);
    private static final String COMMAND = "DevTalk";
    private final TangoGroupCommand tangoGroupCommand;
    private String message = "";

    public TextTalkerNotification(List<String> fullDevicesNames) throws DevFailed {
	this.tangoGroupCommand = new TangoGroupCommand("TextTalkerNotification", COMMAND,
		fullDevicesNames.toArray(new String[fullDevicesNames.size()]));
    }

    @Override
    public void emit() {

	try {
	    this.tangoGroupCommand.insert(this.message);
	    this.tangoGroupCommand.execute();
	} catch (DevFailed e) {
	    DevFailedUtils.logDevFailed(e, this.logger);
	}
    }

    @Override
    public void setMessage(String message) {
	this.message = message;

    }
}

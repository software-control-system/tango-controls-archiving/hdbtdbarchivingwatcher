package fr.soleil.tango.server.watcher.controller.cyclemanager;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.server.watcher.controller.AttributeSelection;
import fr.soleil.tango.server.watcher.controller.Controller;
import fr.soleil.tango.server.watcher.controller.Controller.Control;
import fr.soleil.tango.server.watcher.db.DBReader;
import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;

/**
 * ! Oracle specific ! Execute cycle to periodicly check archived attribute from
 * ODA's table
 *
 * @author fourneau
 *
 */
public class CycleManagerFromDb extends AdapterCycleManager {

    private final Logger logger = LoggerFactory.getLogger(CycleManagerFromDb.class);
    /**
     * @see Controller
     */
    private final Controller controller;

    public CycleManagerFromDb(final DBReader dbReader, final boolean doDiagnosis, final boolean doArchiverDiagnosis,
            final boolean doRetry, final AttributeSelection attributeSelection, final int safetyPeriod,
            final boolean checkLastIsert) {
        super(dbReader, doDiagnosis, doArchiverDiagnosis, doRetry, attributeSelection, safetyPeriod, checkLastIsert);
        this.controller = new Controller(this.dbReader);
    }

    @Override
    public void run() {
        boolean canceled = false;
        Set<String> attributes = null;
        logger.info("cycle manager start diagnosis thread");
        nbrAttrMonitoredCurrentCycle = 0;
        try {
            logger.info("cycle manager get attributs to monitor");
            attributes = attributeSelection.getAttributesToMonitor();
            errorMap.remove("AttributesToMonitor");
        } catch (final DevFailed e) {
            DevFailedUtils.logDevFailed(e, logger);
            errorMap.put("AttributesToMonitor", DevFailedUtils.toString(e));
        }

        cycleStopTime = 0;
        cycleStartTime = System.currentTimeMillis();
        try {
            putAllAttributesToResult();
            errorMap.remove("AllAttributes");
        } catch (final DevFailed e) {
            DevFailedUtils.logDevFailed(e, logger);
            errorMap.put("AllAttributes", DevFailedUtils.toString(e));
        }

        try {
            List<ArchivedAttribute> archivedAttributes;
            do {
                archivedAttributes = dbReader.getODAInformation();
                if (archivedAttributes == null) {
                    // infos.put("ODA", "ODA is currently running.");
                    Thread.sleep(30000);
                }
            } while (archivedAttributes == null);
            // infos.remove("ODA");
            for (final ArchivedAttribute aa : archivedAttributes) {
                if (attributes.contains(aa.getCompleteName())) {
                    nbrAttrMonitoredCurrentCycle++;
                    final Control control = aa.getArchivingStatus();
                    if (control == Control.CONTROL_KO || control == Control.CONTROL_NULL) {

                        logger.info("{} is  {}", aa.getCompleteName(), control);
                        if (doDiagnosis) {
                            // aa.setDiagnosis(new DiagnosisInfo(ResDiagnosis.NOT_DIAG));
                            // nbOfDiagnosis++;
                            diagnosis.addAttribute(aa, control);
                        }
                        result.addKoAttribute(aa);
                    } else {
                        // remove the attribute if it was on the queue nor result
                        result.deleteKoAttribute(aa);
                        diagnosis.removeAttribute(aa);
                    }
                }
            }
            errorMap.remove("GetAttributes");
        } catch (final DevFailed e) {
            DevFailedUtils.logDevFailed(e, logger);
            errorMap.put("GetAttributes", DevFailedUtils.toString(e));
        } catch (final InterruptedException e) {
            logger.info("CycleManager interrupt ");
            Thread.currentThread().interrupt();
            canceled = true;
        }
        if (!canceled) {
            if (doRetry && !Thread.currentThread().isInterrupted()) {
                try {
                    controller.doRetry(result.getKoArchivers().values(), result.getKOAttributesMap());
                    errorMap.remove(DO_RETRY);
                } catch (final DevFailed e) {
                    DevFailedUtils.logDevFailed(e, logger);
                    errorMap.put(DO_RETRY, DevFailedUtils.toString(e));
                }
            }
            endCycle++;
        }
    }
}

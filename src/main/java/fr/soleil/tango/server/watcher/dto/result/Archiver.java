package fr.soleil.tango.server.watcher.dto.result;

import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;

/**
 * An implementation of {@link AbstractElement} for an archiver
 *
 * @author fourneau
 *
 */
public class Archiver extends AbstractElement {

    /**
     * If set to true, the archivers part of the generated control report will
     * contain specific archiver information such as scalar load etc..<br>
     * If set to false, those details will remain blank.
     */
    private final boolean doDiagnosis;
    /**
     * The scalar load of the archiver
     */
    private int scalarLoad;
    /**
     * The spectrum load of the archiver
     */
    private int spectrumLoad;
    /**
     * The image load of the archiver
     */
    private int imageLoad;
    /**
     * The total load of the archiver :<br/>
     * scalar + spectrum + image load
     *
     */
    private int totalLoad;

    /**
     * Constructor
     *
     * @param name
     *            the name of the archiver
     */
    public Archiver(final String name, final boolean doDiagnosis) {
        super(name);
        this.doDiagnosis = doDiagnosis;
    }

    /**
     * Add a KO attribute to this group only if the archiver of the attribute
     * corresponding (ie : is equals ) to the name of the archiver
     *
     * @param attribute
     *            the ArchivedAttribute of the attribute
     */
    @Override
    public final void addKOAttribute(final ArchivedAttribute attribute) {
        if (attribute != null && attribute.getArchiver().equalsIgnoreCase(this.getName())) {
            getKOAttributes().add(attribute.getCompleteName());
        }
    }

    @Override
    public final void addNullAttribute(final ArchivedAttribute attribute) {
        if (attribute != null && attribute.getArchiver().equalsIgnoreCase(this.getName())) {
            getNullAttributes().add(attribute.getCompleteName());
        }
    }

    /**
     * Get a simple formated report. If doDiagnosis is set to true, the report
     * will come with additional information about the load of the archiver.
     *
     * @return String[] a simple formated report :
     *         <ul>
     *         <li>[0] : name of the element</li>
     *         <li>[1] : scalar load : If doDiagnosis is set to true</li>
     *         <li>[2] : spectrum load : If doDiagnosis is set to true</li>
     *         <li>[3] : image load : If doDiagnosis is set to true</li>
     *         <li>[4] : total load : If doDiagnosis is set to true</li>
     *         <li>[(1 or 5)..X] : attributes</li>
     *         </ul>
     */
    @Override
    public final String[] getSimpleReport() {
        final String[] report = super.getSimpleReport();
        if (!this.doDiagnosis) {
            return report;
        }
        int i = 0;
        final String argout[] = new String[report.length + 4];
        // the name
        argout[i++] = report[0];
        // blablabla pour le load
        argout[i++] = new StringBuilder().append("Scalar : ").append(this.scalarLoad).toString();
        argout[i++] = new StringBuilder().append("Spectrum : ").append(this.spectrumLoad).toString();
        argout[i++] = new StringBuilder().append("Image : ").append(this.imageLoad).toString();
        argout[i++] = new StringBuilder().append("Total : ").append(this.totalLoad).toString();
        // Copy KO attributes
        for (; i < argout.length; i++) {
            argout[i] = report[i - 4];
        }
        return argout;
    }

    /**
     * @return Returns the imageLoad.
     */
    public final int getImageLoad() {
        return this.imageLoad;
    }

    /**
     * @param imageLoad
     *            The imageLoad to set.
     */
    public final void setImageLoad(final int imageLoad) {
        this.imageLoad = imageLoad;
    }

    /**
     * @return Returns the scalarLoad.
     */
    public final int getScalarLoad() {
        return this.scalarLoad;
    }

    /**
     * @param scalarLoad
     *            The scalarLoad to set.
     */
    public final void setScalarLoad(final int scalarLoad) {
        this.scalarLoad = scalarLoad;
    }

    /**
     * @return Returns the spectrumLoad.
     */
    public final int getSpectrumLoad() {
        return this.spectrumLoad;
    }

    /**
     * @param spectrumLoad
     *            The spectrumLoad to set.
     */
    public final void setSpectrumLoad(final int spectrumLoad) {
        this.spectrumLoad = spectrumLoad;
    }

    /**
     * @return Returns the totalLoad.
     */
    public final int getTotalLoad() {
        return this.totalLoad;
    }

    /**
     * @param totalLoad
     *            The totalLoad to set.
     */
    public final void setTotalLoad(final int totalLoad) {
        this.totalLoad = totalLoad;
    }
}

package fr.soleil.tango.server.watcher.dto.result;

import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;

/**
 * An implementation of {@link AbstractElement} for a tango domain
 *
 * @author fourneau
 *
 */
public class Domain extends AbstractElement {

    /**
     * Constructor
     *
     * @param name
     *            the name of the domain
     */
    public Domain(final String name) {
        super(name);
    }

    /**
     * Add a KO attribute to this group only if the domain of the attribute
     * corresponding (ie : is equals ) to the name of the group
     *
     * @param attribute
     *            the ArchivedAttribute of the attribute
     */
    @Override
    public final void addKOAttribute(final ArchivedAttribute attribute) {
        if (attribute != null && attribute.getDomain().equalsIgnoreCase(this.getName())) {
            this.getKOAttributes().add(attribute.getCompleteName());
        }
    }

    @Override
    public final void addNullAttribute(final ArchivedAttribute attribute) {
        if (attribute != null && attribute.getDomain().equalsIgnoreCase(this.getName())) {
            getNullAttributes().add(attribute.getCompleteName());
        }
    }
}

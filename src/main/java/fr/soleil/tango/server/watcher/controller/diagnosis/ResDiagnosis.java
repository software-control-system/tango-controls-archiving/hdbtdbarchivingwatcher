package fr.soleil.tango.server.watcher.controller.diagnosis;

/**
 * This enum regroup the different diagnosis of an KO diagnosis
 *
 * @author fourneau
 *
 */
public enum ResDiagnosis {
    DIS_DIAG("Diagnostic disable"), NOT_DIAG("Diagnostic in progress"), FAIL_DIAG("Diagnostic failed"), NOT_EXIST_DEVICE(
            "Device does not exist"), DEVICE_ERROR_DIAG("Device error"), NOT_EXIST_ATTR("Attribute does not exist"), ATTR_EXCEPTION_DIAG(
                    "Read failed"), ATTR_ARCHIVING_CHANGE_DIAG("Attribute archiving configuration changed"), ARCHIVER_ERROR_DIAG(
                            "Archiver error"), ARCHIVING_FAILURE_DIAG("Unknown error");

    /**
     * The String corresponding to the message of the enum value
     */
    private final String errorMessage;

    /**
     * The constructor
     *
     * @param errorMessage
     *            the string corresponding to the enum value
     */
    ResDiagnosis(final String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Get the string of an enum value
     */
    @Override
    public String toString() {
        return this.errorMessage;

    }
}
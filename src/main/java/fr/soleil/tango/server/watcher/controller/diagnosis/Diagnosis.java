package fr.soleil.tango.server.watcher.controller.diagnosis;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.TangoUtil;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.AttributeInfo;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.tango.clientapi.TangoCommand;
import fr.soleil.tango.server.watcher.controller.Controller;
import fr.soleil.tango.server.watcher.controller.Controller.Control;
import fr.soleil.tango.server.watcher.db.DBReader;
import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;
import fr.soleil.tango.server.watcher.dto.result.Result;

/**
 * This class permit to diagnosis an attribute
 *
 * @author fourneau
 *
 */
public class Diagnosis implements Runnable {
    private static final int PERIOD_BETWEEN_ATT = 3000;

    private static final int PERIOD_BETWEEN_DIAGS = 30000;

    private volatile boolean finished = false;

    /**
     * The logger
     */
    private final Logger logger = LoggerFactory.getLogger(Diagnosis.class);

    /**
     * To access the database
     */
    private final DBReader dbReader;

    private final Map<String, SimpleImmutableEntry<ArchivedAttribute, Control>> attributes = new ConcurrentHashMap<String, SimpleImmutableEntry<ArchivedAttribute, Control>>();

    private final boolean checkLastInsert;

    private final Queue<SimpleImmutableEntry<String, DiagnosisInfo>> isDiagnos = new ConcurrentLinkedQueue<SimpleImmutableEntry<String, DiagnosisInfo>>();

    private final Result result;

    /**
     * Constructor
     *
     * @param dbReader To access the database
     * @param toDiagnos A queue to get the attribut to diagnosis
     * @param isDiagnos
     */
    public Diagnosis(final DBReader dbReader, final Result result, final boolean checkLastInsert) {
        this.dbReader = dbReader;
        this.result = result;
        this.checkLastInsert = checkLastInsert;
    }

    public void addAttribute(final ArchivedAttribute aa, final Control control) throws InterruptedException {
        attributes.put(aa.getCompleteName(),
                new SimpleImmutableEntry<ArchivedAttribute, Controller.Control>(aa, control));
    }

    public void removeAttribute(final ArchivedAttribute aa) {
        attributes.remove(aa.getCompleteName());
        SimpleImmutableEntry<String, DiagnosisInfo> toRemove = null;
        for (SimpleImmutableEntry<String, DiagnosisInfo> simple : isDiagnos) {
            if (ObjectUtils.sameObject(simple.getKey(), aa.getCompleteName())) {
                toRemove = simple;
                break;
            }
        }
        if (toRemove != null) {
            isDiagnos.remove(toRemove);
        }
    }

    public List<SimpleImmutableEntry<String, DiagnosisInfo>> getDiagnosis() {
        return new ArrayList<SimpleImmutableEntry<String, DiagnosisInfo>>(isDiagnos);
    }

    /**
     * Get the information from the in queue, diagnosis it, and put the result
     * on the out queue
     */
    @Override
    public final void run() {
        try {
            while (!this.finished) {
                logger.debug("waiting for a new attribute to diagnose");
                Thread.sleep(PERIOD_BETWEEN_DIAGS);
                isDiagnos.clear();
                logger.debug("start diagnosis for {}", attributes.size());
                for (final SimpleImmutableEntry<ArchivedAttribute, Control> entry : attributes.values()) {
                    try {
                        final ArchivedAttribute attr = entry.getKey();
                        final DiagnosisInfo diagnose = diagnose(attr, entry.getValue(), checkLastInsert);
                        isDiagnos
                                .add(new SimpleImmutableEntry<String, DiagnosisInfo>(attr.getCompleteName(), diagnose));
                        result.addKoDiagnosis(attr.getCompleteName(), diagnose);
                        result.addNullDiagnosis(attr.getCompleteName(), diagnose);
                    } catch (final Exception e) {
                        logger.error("unexpected exception during diagnosis", e);
                    }
                    Thread.sleep(PERIOD_BETWEEN_ATT);
                }
                logger.debug("end diagnosis for {}", attributes.size());

            }
        } catch (final InterruptedException e) {
            logger.info("Diagnosis interrupt");
            Thread.currentThread().interrupt();
        }
        logger.info("Diagnosis runnable is interrupt");
    }

    /**
     * Stop the thread of diagnosis
     */
    public void stopDiagnosis() {
        this.finished = true;
    }

    /**
     * Diagnosis an attribute
     *
     * @param fullAttrName
     * @return
     */
    public final DiagnosisInfo diagnose(final ArchivedAttribute attr, final Control control,
            final boolean checkLastInsert) {
        logger.info("starting diagnose for attribute {}", attr.getCompleteName());
        // final String[] deviceAndAttribute;
        DiagnosisInfo info = null;
        final String fullAttrName = attr.getCompleteName();
        String lastInsertOk = "";
        if (checkLastInsert) {
            try {
                lastInsertOk = this.dbReader.getLastNotNullInsert(fullAttrName);
            } catch (final DevFailed e) {
                logger.error("error getting last insert date ", e);
                lastInsertOk = "Unknown";
            }
        }
        // 1 - test device name
        String deviceName;
        try {
            deviceName = TangoUtil.getfullDeviceNameForAttribute(fullAttrName);
        } catch (final DevFailed e) {
            info = new DiagnosisInfo(ResDiagnosis.FAIL_DIAG, "The full attribute name is not in good format",
                    lastInsertOk);
            deviceName = null;
        }

        if (info == null) {
            if (info == null) {
                // 2 - test archived device
                DeviceProxy dp = null;
                logger.info("Diagnosis - create DeviceProxy for attribute {}", attr.getCompleteName());
                try {
                    dp = DeviceProxyFactory.get(deviceName);
                    logger.info("Diagnosis - ping DeviceProxy for attribute {}", attr.getCompleteName());
                    try {
                        // dp.ping(); reconnection does not work with ping
                        dp.command_inout("state");
                    } catch (final DevFailed e) {
                        info = new DiagnosisInfo(ResDiagnosis.DEVICE_ERROR_DIAG, e, lastInsertOk);
                    }
                } catch (final DevFailed e) {
                    info = new DiagnosisInfo(ResDiagnosis.NOT_EXIST_DEVICE, lastInsertOk);
                }
                if (info == null) {
                    // 2 - test archived attribute
                    logger.info("Diagnosis -  test full name attribute {}", attr.getCompleteName());
                    String attributeName;
                    try {
                        attributeName = TangoUtil.getAttributeName(fullAttrName);
                    } catch (final DevFailed e) {
                        info = new DiagnosisInfo(ResDiagnosis.FAIL_DIAG,
                                "The full attribute name is not in good format", lastInsertOk);
                        attributeName = null;
                    }
                    if (info == null) {
                        logger.info("Diagnosis -  test is exists attribute {}", attr.getCompleteName());
                        try {
                            if (!this.isAttrExists(dp, attributeName)) {
                                info = new DiagnosisInfo(ResDiagnosis.NOT_EXIST_ATTR, lastInsertOk);
                            }
                        } catch (final DevFailed e1) {
                            info = new DiagnosisInfo(ResDiagnosis.FAIL_DIAG, "Unable to get the atributes list",
                                    lastInsertOk);
                        }
                        if (info == null) {
                            // 3 - read attribute
                            logger.info("Diagnosis - read the attribute {}", attr.getCompleteName());
                            try {
                                // final DeviceAttribute da = dp.read_attribute(attributeName);
                                final fr.soleil.tango.clientapi.TangoAttribute att = new fr.soleil.tango.clientapi.TangoAttribute(
                                        attr.getCompleteName());
                                att.read();

                                if (att.isScalar() && att.isNumber()) {
                                    final String readValue = att.extract(String.class);
                                    if (readValue.equalsIgnoreCase(Double.toString(Double.NaN))) {
                                        info = new DiagnosisInfo(ResDiagnosis.ATTR_EXCEPTION_DIAG,
                                                "attribute value is NaN.", lastInsertOk);
                                    }
                                } else {
                                    att.extract();
                                }
                            } catch (final DevFailed e) {
                                info = new DiagnosisInfo(ResDiagnosis.ATTR_EXCEPTION_DIAG, e, lastInsertOk);
                            }
                            if (info == null) {
                                // 4 - check archiving configuration
                                logger.info("Diagnosis - check the configuration of attribute {}",
                                        attr.getCompleteName());
                                try {
                                    final AttributeInfo attributeInfo = dp.get_attribute_info(attributeName);
                                    if (this.isAttrWriteArchivingChanged(fullAttrName, attributeInfo)) {
                                        info = new DiagnosisInfo(ResDiagnosis.ATTR_ARCHIVING_CHANGE_DIAG,
                                                "The writable criterion has changed", lastInsertOk);
                                    } else if (this.isAttrFormatArchivingChanged(fullAttrName, attributeInfo)) {
                                        info = new DiagnosisInfo(ResDiagnosis.ATTR_ARCHIVING_CHANGE_DIAG,
                                                "The format criterion has changed", lastInsertOk);
                                    } else if (this.isAttrTypeArchivingChanged(fullAttrName, attributeInfo)) {
                                        info = new DiagnosisInfo(ResDiagnosis.ATTR_ARCHIVING_CHANGE_DIAG,
                                                "The type criterion has changed", lastInsertOk);
                                    }
                                } catch (final DevFailed e) {
                                    info = new DiagnosisInfo(ResDiagnosis.FAIL_DIAG, e, lastInsertOk);
                                }

                            }
                            // 5 - test archiver
                            final String archiver = attr.getArchiver();
                            if (archiver != null) {
                                try {
                                    logger.info("Diagnosis - checking archiver health {}", archiver);
                                    final DevState state = DeviceProxyFactory.get(archiver).state();
                                    if (state.equals(DevState.FAULT)) {
                                        info = new DiagnosisInfo(ResDiagnosis.ARCHIVER_ERROR_DIAG,
                                                archiver + " is in FAULT state", lastInsertOk);
                                    }
                                    if (info == null) {
                                        final String errorFromArchiver = this.getErrorFromArchiver(attr);
                                        if (!errorFromArchiver.trim().isEmpty()) {
                                            info = new DiagnosisInfo(ResDiagnosis.ARCHIVER_ERROR_DIAG,
                                                    errorFromArchiver, lastInsertOk);
                                        }
                                    }
                                } catch (final DevFailed df) {
                                    info = new DiagnosisInfo(ResDiagnosis.ARCHIVER_ERROR_DIAG, df, lastInsertOk);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (info == null) {
            info = new DiagnosisInfo(ResDiagnosis.ARCHIVING_FAILURE_DIAG, fullAttrName, lastInsertOk);
        }
        // append diag for NULL value
        if (control != null && control.equals(Control.CONTROL_NULL)) {
            logger.info("Last insert is NULL");
            info.addInfo(" Last insert is NULL");
        }
        logger.info("diagnose for attribute {} is {} ", attr.getCompleteName(), info);
        return info;
    }

    /**
     * Try to know if the archiver get error about the attribut
     *
     * @param aa
     * @return empty string if there is no error about the attribute, otherwhise
     *         the error
     * @throws DevFailed
     */
    private String getErrorFromArchiver(final ArchivedAttribute aa) throws DevFailed {
        final String archiver = aa.getArchiver();
        final TangoCommand cmd = new TangoCommand(archiver, "GetErrorMessageForAttribute");
        Object result = cmd.executeExtract(aa.getCompleteName());
        logger.info("GetErrorMessageForAttribute result {}", result);
        if (result == null) {
            result = "";
        }
        return (String) result;
    }

    /**
     * Test if an attribute exists on the device
     *
     * @param dp The device proxy
     * @param attribute the attribute name
     * @return true if the attribute exists on the device, false else
     * @throws DevFailed
     */
    private boolean isAttrExists(final DeviceProxy dp, final String attribute) throws DevFailed {
        final String[] attrArray = dp.get_attribute_list();

        for (final String attr : attrArray) {
            if (attr.equalsIgnoreCase(attribute)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Test if the attribute write type has changed between the attribute
     * configuration and the database saved
     *
     * @param fullAttrName the full attribute name
     * @param attributeInfo the configuration of the attribute from the database
     * @return true if the attribute write type has changed
     * @throws DevFailed
     */
    private boolean isAttrWriteArchivingChanged(final String fullAttrName, final AttributeInfo attributeInfo)
            throws DevFailed {
        final AttrWriteType writableDb = AttrWriteType.from_int(this.dbReader.getAttWritableCriterion(fullAttrName));
        return !writableDb.equals(attributeInfo.writable);
    }

    /**
     * Test if the attribute format has changed between the attribute
     * configuration and the database saved
     *
     * @param fullAttrName the full attribute name
     * @param attributeInfo the configuration of the attribute from the database
     * @return true if the attribute format has changed
     * @throws DevFailed
     */
    private boolean isAttrFormatArchivingChanged(final String fullAttrName, final AttributeInfo attributeInfo)
            throws DevFailed {
        final AttrDataFormat dataFormatDb = AttrDataFormat.from_int(this.dbReader.getAttFormatCriterion(fullAttrName));
        return !dataFormatDb.equals(attributeInfo.data_format);
    }

    /**
     * Test if the attribute type has changed between the attribute
     * configuration and the database saved
     *
     *
     * @param fullAttrName the full attribute name
     * @param attributeInfo the configuration of the attribute from the database
     * @return true if the attribute type has changed
     * @throws DevFailed
     */
    private boolean isAttrTypeArchivingChanged(final String fullAttrName, final AttributeInfo attributeInfo)
            throws DevFailed {
        final int dataTypeDb = this.dbReader.getAttTypeCriterion(fullAttrName);
        return !(dataTypeDb == attributeInfo.data_type);
    }

}

package fr.soleil.tango.server.watcher.dto.result;

import java.util.Collection;
import java.util.Collections;
import java.util.TreeSet;

/**
 * An abstract implementation of {@link IElement}
 *
 * The attribute are saved in a Set<String>
 *
 * @author fourneau
 *
 */
public abstract class AbstractElement implements IElement {

    /**
     * The name of the group
     */
    private String name;
    /**
     * A collection to save KO attributes
     */
    private final Collection<String> koAttributes;

    /**
     * A collection to save KO attributes
     */
    private final Collection<String> nullAttributes;

    /**
     * Constructor
     *
     * @param name
     *            the name of the group
     */
    public AbstractElement(final String name) {
        this.name = name;
        this.koAttributes = Collections.synchronizedSet(new TreeSet<String>(String.CASE_INSENSITIVE_ORDER));
        this.nullAttributes = Collections.synchronizedSet(new TreeSet<String>(String.CASE_INSENSITIVE_ORDER));
    }

    @Override
    public final void setName(final String name) {
        this.name = name;
    }

    @Override
    public final String getName() {
        return this.name;
    }

    @Override
    public final Collection<String> getKOAttributes() {
        return this.koAttributes;
    }

    @Override
    public final void removeKOAttribute(final String attribute) {
        this.koAttributes.remove(attribute);
    }

    @Override
    public final Collection<String> getNullAttributes() {
        return this.nullAttributes;
    }

    @Override
    public final void removeNullAttribute(final String attribute) {
        this.nullAttributes.remove(attribute);
    }

    /**
     * Get a simple formated report
     *
     * @return String[] a simple formated report :
     *         <ul>
     *         <li>[0] : name of the element</li>
     *         <li>[1..X] : attributes</li>
     *         </ul>
     */
    @Override
    public String[] getSimpleReport() {
        final String[] result = new String[this.koAttributes.size() + nullAttributes.size() + 1];
        int i = 0;
        result[i++] = this.name;
        for (final String s : this.koAttributes) {
            result[i++] = s;
        }

        for (final String s : this.nullAttributes) {
            result[i++] = s;
        }
        return result;
    }

    @Override
    public final boolean isKOEmpty() {
        return this.koAttributes.isEmpty();
    }

    @Override
    public final boolean isNullEmpty() {
        return this.nullAttributes.isEmpty();
    }

    @Override
    public String toString() {
        return getName();
    }

}

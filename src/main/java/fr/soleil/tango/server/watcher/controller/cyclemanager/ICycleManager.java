package fr.soleil.tango.server.watcher.controller.cyclemanager;

import java.util.Map;
import java.util.Set;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.server.watcher.dto.result.Result;

/**
 *
 * @author fourneau
 *
 *         This interface describe the different required methods for
 *         implements a Watcher cycle manager
 */
public interface ICycleManager extends Runnable {
    /**
     * Get the result from this CycleManager
     *
     * @see Result
     * @return the result from the cycle
     */
    Result getResult();

    /**
     * Get all monitored attributes
     *
     * @return Set<String> all monitored attributes
     * @throws DevFailed
     */
    Set<String> getMonitoredAttributes() throws DevFailed;

    /**
     * Get the errors thats occured
     *
     * @return Map<String, String> errors from this object
     */
    Map<String, String> getErrors();

    /**
     * Get the start date of the current cycle ie : The difference, measured in
     * milliseconds, between the start time of the cycle and midnight, January
     * 1, 1970 UTC.
     *
     * @return the start date of the current cycle
     */
    long getCycleStartTime();

    /**
     * Get the stop date of the current cycle ie : The difference, measured in
     * milliseconds, between the stop time of the cycle and midnight, January 1,
     * 1970 UTC.
     *
     * @return the stop date of the current cycle
     */
    long getCycleStopTime();

    /**
     * Get the number of attribute monitored
     *
     * @return the number of attribute monitored
     */
    int getNbrAttrMonitoredCurrentCycle();

    /**
     * Get the number of cycle during the start of the CycleManager
     *
     * @return the number of cycle during the start of the CycleManager
     */
    int getNbrEndCycle();

    /**
     * Put all the attributes with their archiving modes on the result object
     *
     * @throws DevFailed
     */
    void putAllAttributesToResult() throws DevFailed;

    /**
     * Get all archived attributes with their archiving modes
     *
     * @return String[] each row representing an archived attribute and its
     *         archiving mode
     */
    String[] getAllAttributes();

    // Map<String, String> getInfos();

    /**
     * Start the diagnosis thread if necessary
     */
    void startDiagnosis();

    /**
     * Stop the diagnosis thread if is running
     */
    void stopDiagnosis();

}

package fr.soleil.tango.server.watcher.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.AttributeDataType;
import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.server.watcher.db.DBReader;
import fr.soleil.tango.server.watcher.dto.attribute.ModeData;

@RunWith(MockitoJUnitRunner.class)
public class AttributeSelectionTest {

    private static Hashtable<String, ModeData> dbNormalReply = new Hashtable<String, ModeData>();
    {
	dbNormalReply.put("tango/tangotest/1/status", new ModeData());
	dbNormalReply.put("tango/tangotest/1/double_scalar", new ModeData());
	dbNormalReply.put("tango/tangotest/1/ampli", new ModeData());
	dbNormalReply.put("tango/tangotest/1/boolean_scalar", new ModeData());
    }

    private static Hashtable<String, ModeData> dbTypeDoubleReply = new Hashtable<String, ModeData>();
    {
	dbTypeDoubleReply.put("tango/tangotest/1/double_scalar", new ModeData());
	dbTypeDoubleReply.put("tango/tangotest/1/ampli", new ModeData());
    }

    private static Hashtable<String, ModeData> dbWriteReadReply = new Hashtable<String, ModeData>();
    {
	dbWriteReadReply.put("tango/tangotest/1/status", new ModeData());
    }

    private static Hashtable<String, ModeData> dbFormatScalarReply = new Hashtable<String, ModeData>();
    {
	dbFormatScalarReply.put("tango/tangotest/1/status", new ModeData());
	dbFormatScalarReply.put("tango/tangotest/1/double_scalar", new ModeData());
	dbFormatScalarReply.put("tango/tangotest/1/ampli", new ModeData());
	dbFormatScalarReply.put("tango/tangotest/1/boolean_scalar", new ModeData());
    }

    private static Hashtable<String, ModeData> dbFullConstrainteReply = new Hashtable<String, ModeData>();
    {
	dbFullConstrainteReply.put("tango/tangotest/1/status", new ModeData());
    }

    private static Hashtable<String, ModeData> dbWriteTypeReply = new Hashtable<String, ModeData>();

    @Mock
    private DBReader dbReader;

    @Test
    public void exclusionOfAttributeTest() throws DevFailed {
	this.setGoodDbReader();
	List<String> exclusion = new ArrayList<String>();
	exclusion.add("tango/tangotest/1/s.*");
	AttributeSelection as = new AttributeSelection(this.dbReader, null, exclusion, null, null,
		null);
	Set<String> res = as.getAttributesToMonitor();
	List<String> expected = new ArrayList<String>();
	expected.add("tango/tangotest/1/double_scalar");
	expected.add("tango/tangotest/1/ampli");
	expected.add("tango/tangotest/1/boolean_scalar");

	assertEquals(true, res.containsAll(expected));
	res.removeAll(expected);
	assertEquals(true, res.isEmpty());
    }

    @Before
    public void setGoodDbReader() throws DevFailed {
	reset(this.dbReader);
	when(
		this.dbReader.getArchivedAttributes(new AttrWriteType[0], new Integer[0],
			new AttrDataFormat[0])).thenReturn(dbNormalReply);
	when(
		this.dbReader.getArchivedAttributes(new AttrWriteType[0],
			new Integer[] { AttributeDataType._ATT_DOUBLE }, new AttrDataFormat[0]))
		.thenReturn(dbTypeDoubleReply);
	when(
		this.dbReader.getArchivedAttributes(new AttrWriteType[0], new Integer[0],
			new AttrDataFormat[] { AttrDataFormat.SCALAR })).thenReturn(
		dbFormatScalarReply);
	when(
		this.dbReader.getArchivedAttributes(new AttrWriteType[] { AttrWriteType.READ },
			new Integer[0], new AttrDataFormat[0])).thenReturn(dbWriteReadReply);
	when(
		this.dbReader.getArchivedAttributes(new AttrWriteType[] { AttrWriteType.READ },
			new Integer[] { AttributeDataType._ATT_DOUBLE }, new AttrDataFormat[0]))
		.thenReturn(dbWriteTypeReply);
	when(
		this.dbReader.getArchivedAttributes(new AttrWriteType[] { AttrWriteType.READ },
			new Integer[] { AttributeDataType._ATT_STRING },
			new AttrDataFormat[] { AttrDataFormat.SCALAR })).thenReturn(
		dbFullConstrainteReply);
    }

    @Test
    public void inclusionWithSimpleRegexTest() throws DevFailed {
	List<String> inclusion = new ArrayList<String>();
	inclusion.add(".*scalar");
	AttributeSelection as = new AttributeSelection(this.dbReader, inclusion);
	Set<String> res = as.getAttributesToMonitor();
	List<String> expected = new ArrayList<String>();
	expected.add("tango/tangotest/1/double_scalar");
	expected.add("tango/tangotest/1/boolean_scalar");
	assertEquals(true, res.containsAll(expected));
	res.removeAll(expected);
	assertEquals(true, res.isEmpty());
    }

    @Test
    public void inclusionWithAttributeType() throws DevFailed {
	AttributeSelection as = new AttributeSelection(this.dbReader, null, null, null,
		new Integer[] { AttributeDataType._ATT_DOUBLE }, null);
	Set<String> res = as.getAttributesToMonitor();
	List<String> expected = new ArrayList<String>();
	expected.add("tango/tangotest/1/double_scalar");
	expected.add("tango/tangotest/1/ampli");
	assertEquals(true, res.containsAll(expected));
	res.removeAll(expected);
	assertEquals(true, res.isEmpty());
    }

    @Test
    public void inclusionWithAttributeFormat() throws DevFailed {
	AttributeSelection as = new AttributeSelection(this.dbReader, null, null,
		new AttrDataFormat[] { AttrDataFormat.SCALAR }, null, null);
	Set<String> res = as.getAttributesToMonitor();
	List<String> expected = new ArrayList<String>();
	expected.add("tango/tangotest/1/double_scalar");
	expected.add("tango/tangotest/1/ampli");
	expected.add("tango/tangotest/1/boolean_scalar");
	expected.add("tango/tangotest/1/status");
	assertEquals(true, res.containsAll(expected));
	res.removeAll(expected);
	assertEquals(true, res.isEmpty());
    }

    @Test
    public void inclusionWithAttributeWrite() throws DevFailed {
	AttributeSelection as = new AttributeSelection(this.dbReader, null, null, null, null,
		new AttrWriteType[] { AttrWriteType.READ });
	Set<String> res = as.getAttributesToMonitor();
	List<String> expected = new ArrayList<String>();
	expected.add("tango/tangotest/1/status");
	assertEquals(true, res.containsAll(expected));
	res.removeAll(expected);
	assertEquals(true, res.isEmpty());
    }

    @Test
    public void mixEclusionInclusionTest() throws DevFailed {
	List<String> exclusion = new ArrayList<String>();
	exclusion.add("tango/tangotest/1/double.*");
	List<String> inclusion = new ArrayList<String>();
	inclusion.add(".*ampli");
	AttributeSelection as = new AttributeSelection(this.dbReader, inclusion, exclusion, null,
		new Integer[] { AttributeDataType._ATT_DOUBLE }, null);
	Set<String> res = as.getAttributesToMonitor();
	List<String> expected = new ArrayList<String>();
	expected.add("tango/tangotest/1/ampli");
	assertEquals(true, res.containsAll(expected));
	res.removeAll(expected);
	assertEquals(true, res.isEmpty());

    }

    @Test
    public void mixEclusionInclusionTestFull() throws DevFailed {
	List<String> exclusion = new ArrayList<String>();
	exclusion.add("tango/tangotest/1/double.*");
	List<String> inclusion = new ArrayList<String>();
	inclusion.add("tango/tangotest/1/.*");
	AttributeSelection as = new AttributeSelection(this.dbReader, inclusion, exclusion,
		new AttrDataFormat[] { AttrDataFormat.SCALAR },
		new Integer[] { AttributeDataType._ATT_STRING },
		new AttrWriteType[] { AttrWriteType.READ });
	Set<String> res = as.getAttributesToMonitor();
	List<String> expected = new ArrayList<String>();
	expected.add("tango/tangotest/1/status");
	assertEquals(true, res.containsAll(expected));
	res.removeAll(expected);
	assertEquals(true, res.isEmpty());
    }

    @Test
    public void mixEclusionInclusionTestEmpty() throws DevFailed {
	List<String> exclusion = new ArrayList<String>();
	exclusion.add("tango/tangotest/1/double.*");
	List<String> inclusion = new ArrayList<String>();
	inclusion.add("tango/tangotest/1/.*");
	AttributeSelection as = new AttributeSelection(this.dbReader, inclusion, exclusion, null,
		new Integer[] { AttributeDataType._ATT_DOUBLE },
		new AttrWriteType[] { AttrWriteType.READ });
	Set<String> res = as.getAttributesToMonitor();
	assertEquals(true, res.isEmpty());
    }
}

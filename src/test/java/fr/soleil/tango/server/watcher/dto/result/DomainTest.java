package fr.soleil.tango.server.watcher.dto.result;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;

public class DomainTest {

    private static final String DOMAIN = "Domain";

    private static final String ATTR = "Domain/Family/Member/Attr";
    private static final String ATTR_2 = "Domain/Family/Member/Attr2";
    private static final String ATTR_WRONG = "Domain2/Family/Member/Attr";

    @Test
    public void initTest() {
        final Domain d = new Domain(DOMAIN);
        assertEquals(DOMAIN, d.getName());
        assertTrue(d.isKOEmpty());
    }

    @Test
    public void addTest() {
        final Domain d = new Domain(DOMAIN);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_WRONG);
        final ArchivedAttribute aa2 = new ArchivedAttribute(ATTR);
        final ArchivedAttribute aa3 = new ArchivedAttribute(ATTR_2);
        d.addKOAttribute(aa);
        assertTrue(d.isKOEmpty());
        d.addKOAttribute(aa2);
        assertEquals(1, d.getKOAttributes().size());
        d.addKOAttribute(aa3);
        assertEquals(2, d.getKOAttributes().size());
        d.addKOAttribute(aa3);
        assertEquals(2, d.getKOAttributes().size());
    }

    @Test
    public void removeTest() {
        final Domain d = new Domain(DOMAIN);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_WRONG);
        final ArchivedAttribute aa2 = new ArchivedAttribute(ATTR);
        final ArchivedAttribute aa3 = new ArchivedAttribute(ATTR_2);
        d.addKOAttribute(aa);
        assertTrue(d.isKOEmpty());
        d.addKOAttribute(aa2);
        assertEquals(1, d.getKOAttributes().size());
        d.addKOAttribute(aa3);
        assertEquals(2, d.getKOAttributes().size());
        d.addKOAttribute(aa3);
        assertEquals(2, d.getKOAttributes().size());
        d.removeKOAttribute(aa3.getCompleteName());
        assertEquals(1, d.getKOAttributes().size());
        d.removeKOAttribute(aa.getCompleteName());
        assertEquals(1, d.getKOAttributes().size());
        d.removeKOAttribute(aa2.getCompleteName());
        assertEquals(0, d.getKOAttributes().size());
        assertTrue(d.isKOEmpty());
    }

    @Test
    public void getTest() {
        final Domain d = new Domain(DOMAIN);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_WRONG);
        final ArchivedAttribute aa2 = new ArchivedAttribute(ATTR);
        final ArchivedAttribute aa3 = new ArchivedAttribute(ATTR_2);
        d.addKOAttribute(aa);
        assertTrue(d.isKOEmpty());

        d.addKOAttribute(aa2);
        Collection<String> attrs = d.getKOAttributes();
        assertEquals(1, attrs.size());
        attrs.contains(aa2.getCompleteName());

        d.addKOAttribute(aa3);
        attrs = d.getKOAttributes();
        assertEquals(2, attrs.size());
        attrs.contains(aa2.getCompleteName());
        attrs.contains(aa3.getCompleteName());

        d.addKOAttribute(aa3);
        attrs = d.getKOAttributes();
        attrs.contains(aa2.getCompleteName());
        attrs.contains(aa3.getCompleteName());
    }
}

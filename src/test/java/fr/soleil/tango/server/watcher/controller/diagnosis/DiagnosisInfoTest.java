package fr.soleil.tango.server.watcher.controller.diagnosis;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;

public class DiagnosisInfoTest {

    private static final String LAST_INSERT = "last insert";
    private static final String INFO = "info";

    @Test
    public void getterTest() {
        final DiagnosisInfo d = new DiagnosisInfo(ResDiagnosis.DIS_DIAG, INFO, LAST_INSERT);
        assertEquals(ResDiagnosis.DIS_DIAG, d.getDiagnosis());
        System.out.println(d.getInfo());
        // assertEquals(INFO, d.getInfo());
        assertThat(d.getInfo(), containsString(INFO));
        assertEquals(LAST_INSERT, d.getLastInsert());
    }

    @Test
    public void equalsTest() {
        final DiagnosisInfo d1 = new DiagnosisInfo(ResDiagnosis.DIS_DIAG, INFO, LAST_INSERT);
        final DiagnosisInfo d2 = new DiagnosisInfo(ResDiagnosis.DIS_DIAG, INFO, LAST_INSERT);
        final DiagnosisInfo d3 = new DiagnosisInfo(ResDiagnosis.DIS_DIAG, "", LAST_INSERT);
        final DiagnosisInfo d4 = new DiagnosisInfo(ResDiagnosis.DIS_DIAG, INFO, "");
        final DiagnosisInfo d5 = new DiagnosisInfo(ResDiagnosis.NOT_DIAG, INFO, LAST_INSERT);
        assertTrue(d1.equals(d1));
        assertTrue(d1.equals(d2));
        assertFalse(d1.equals(d3));
        assertFalse(d1.equals(d4));
        assertFalse(d1.equals(d5));
    }

    @Test
    public void compareTest() {
        final DiagnosisInfo d1 = new DiagnosisInfo(ResDiagnosis.DIS_DIAG);
        final DiagnosisInfo d2 = new DiagnosisInfo(ResDiagnosis.DIS_DIAG, INFO, LAST_INSERT);
        final DiagnosisInfo d3 = new DiagnosisInfo(ResDiagnosis.NOT_DIAG, INFO, LAST_INSERT);
        final DiagnosisInfo d4 = new DiagnosisInfo(ResDiagnosis.ARCHIVING_FAILURE_DIAG, INFO, LAST_INSERT);
        assertEquals(d1.compareTo(d1), 0);
        assertEquals(d2.compareTo(d3) < 0, true);
        assertEquals(d4.compareTo(d1) > 0, true);
    }

    @Test
    public void toStringTest() {
        final DiagnosisInfo d1 = new DiagnosisInfo(ResDiagnosis.DIS_DIAG);
        final DiagnosisInfo d2 = new DiagnosisInfo(ResDiagnosis.DIS_DIAG, LAST_INSERT);
        final DiagnosisInfo d3 = new DiagnosisInfo(ResDiagnosis.DIS_DIAG, INFO, LAST_INSERT);

        final DevFailed df = DevFailedUtils.newDevFailed("test");
        final DiagnosisInfo d4 = new DiagnosisInfo(ResDiagnosis.DIS_DIAG, df, LAST_INSERT);

        assertThat(d1.toString(), containsString(ResDiagnosis.DIS_DIAG.toString()));
        assertThat(d2.toString(), containsString(ResDiagnosis.DIS_DIAG.toString() + ". Last insert ok is : "
                + LAST_INSERT));
        assertThat(d3.toString(), containsString(ResDiagnosis.DIS_DIAG.toString() + ". " + INFO
                + ". Last insert ok is : " + LAST_INSERT));
        assertThat(d4.toString(), containsString(ResDiagnosis.DIS_DIAG.toString() + ". " + DevFailedUtils.toString(df)
                + ". Last insert ok is : " + LAST_INSERT));

    }
}

package fr.soleil.tango.server.watcher.dto.attribute;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;

import fr.soleil.tango.server.watcher.controller.Controller.Control;
import fr.soleil.tango.server.watcher.controller.diagnosis.DiagnosisInfo;
import fr.soleil.tango.server.watcher.controller.diagnosis.ResDiagnosis;

public class ArchivedAttributeTest {

    private static final String ARCHIVER = "Archiver";
    private static final String DOMAIN = "domain";
    private static final String FAMILY = "family";
    private static final String MEMBER = "member";
    private static final String DEVICE = DOMAIN + "/" + FAMILY + "/" + MEMBER;
    private static final String ATTRIBUTE = "attribute";

    private static final String ATTR_COMPLETE_NAME = DEVICE + "/" + ATTRIBUTE + "/";
//    private static final String ATTR_COMPLETE_NAME_2 = DEVICE + "/" + ATTRIBUTE;
    private ArchivedAttribute aa;
    private ArchivedAttribute aa2;
    private final DiagnosisInfo diagnosis = new DiagnosisInfo(ResDiagnosis.NOT_DIAG);
    private final Timestamp t = new Timestamp(System.currentTimeMillis());

    @Before
    public void setUp() {
        this.aa = new ArchivedAttribute(ATTR_COMPLETE_NAME);
        this.aa.setArchiver(ARCHIVER);
        this.aa.setArchivingStatus(Control.CONTROL_OK);
        this.aa.setDiagnosis(this.diagnosis);
        this.aa.setId(1);
        this.aa.setLastInsert(this.t);
        this.aa.setLastInsertRequest(this.t);
        this.aa.setPeriod(1);

        this.aa2 = new ArchivedAttribute(ATTR_COMPLETE_NAME);
        this.aa2.setArchiver(ARCHIVER);
        this.aa2.setArchivingStatus(Control.CONTROL_OK);
        this.aa2.setDiagnosis(this.diagnosis);
        this.aa2.setId(1);
        this.aa2.setLastInsert(this.t);
        this.aa2.setLastInsertRequest(this.t);
        this.aa2.setPeriod(1);
    }

    @Test
    public void getterTest() {
        assertEquals(ATTR_COMPLETE_NAME, this.aa.getCompleteName());
        assertEquals(DOMAIN, this.aa.getDomain());
        assertEquals(FAMILY, this.aa.getFamily());
        assertEquals(ATTRIBUTE, this.aa.getAttributeSubName());
        assertEquals(DEVICE, this.aa.getDeviceName());
        assertEquals(ARCHIVER, this.aa.getArchiver());
        assertEquals(this.diagnosis, this.aa.getDiagnosis());
        assertEquals(Control.CONTROL_OK, this.aa.getArchivingStatus());
        assertEquals(1, this.aa.getId());
        assertEquals(1, this.aa.getPeriod());
        assertEquals(this.t, this.aa.getLastInsert());
        assertEquals(this.t, this.aa.getLastInsertRequest());
    }

    @Test
    public void equalsTest() {
        ArchivedAttribute aa3 = new ArchivedAttribute(ATTR_COMPLETE_NAME);
        aa3.setArchiver(ARCHIVER + "!");
        aa3.setArchivingStatus(Control.CONTROL_OK);
        aa3.setDiagnosis(this.diagnosis);
        aa3.setId(1);
        aa3.setLastInsert(this.t);
        aa3.setLastInsertRequest(this.t);
        aa3.setPeriod(1);
        assertTrue(this.aa.equals(this.aa));
        assertTrue(this.aa.equals(this.aa2));
        assertFalse(this.aa.equals(aa3));
    }

    @Test
    public void compareTest() {
        assertTrue(this.aa.compareTo(this.aa2) == 0);
        this.aa2.setDiagnosis(new DiagnosisInfo(ResDiagnosis.DIS_DIAG));
        assertTrue(this.aa.compareTo(this.aa2) > 0);
        this.aa2.setDiagnosis(new DiagnosisInfo(ResDiagnosis.ARCHIVING_FAILURE_DIAG));
        assertTrue(this.aa.compareTo(this.aa2) < 0);
    }
}

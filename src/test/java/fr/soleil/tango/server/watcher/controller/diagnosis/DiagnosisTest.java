package fr.soleil.tango.server.watcher.controller.diagnosis;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.ServerSocket;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.tango.server.ServerManager;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.AttributeDataType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.tango.clientapi.TangoCommand;
import fr.soleil.tango.server.dynamictests.DynamicTester;
import fr.soleil.tango.server.watcher.db.DBReader;
import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;
import fr.soleil.tango.server.watcher.dto.result.Result;

@RunWith(MockitoJUnitRunner.class)
//@Ignore("No db device does not work yet with device proxy")
public class DiagnosisTest {

    // // Instance
    // private static final String INSTANCE_NAME = "test";

    private final DBReader dbReader = Mockito.mock(DBReader.class);
    private static String deviceName;
    private static final String ATTR_NAME = "double_scalar";
    private static final String ATTR_NAME_2 = "string_spectrum";
    private static final String ATTR_NAME_NULL = "null_attribute";
    private static final String WRONG_ATTR_NAME = "x";
    private static String ATTR_FULL_NAME;
    private static String ATTR_NAME_TO_DELETE = "attr_to_delete";
    private static String ATTR_FULL_NAME_TO_DELETE;
    private static String ATTR_FULL_NAME_WRONG;
    private static String ATTR_FULL_NAME_NULL;

    private static void startDynamicTesterNoDb() throws DevFailed, IOException {
        ServerSocket ss1 = null;
        int noDbGiopPort;
        try {
            ss1 = new ServerSocket(0);
            ss1.setReuseAddress(true);
            ss1.close();
            noDbGiopPort = ss1.getLocalPort();
        } finally {
            if (ss1 != null) {
                ss1.close();
            }
        }
        // final String deviceName2 = "1/2/3";
        deviceName = "tango://localhost:" + ss1.getLocalPort() + "/1/2/3#dbase=no";

        ATTR_FULL_NAME = deviceName + "/" + ATTR_NAME;
        ATTR_FULL_NAME_TO_DELETE = deviceName + "/" + ATTR_NAME_TO_DELETE;
        ATTR_FULL_NAME_WRONG = deviceName + "/" + WRONG_ATTR_NAME;
        ATTR_FULL_NAME_NULL = deviceName + "/" + ATTR_NAME_NULL;

        System.setProperty("OAPort", Integer.toString(noDbGiopPort));
        ServerManager.getInstance().addClass(DynamicTester.class.getCanonicalName(), DynamicTester.class);

        ServerManager.getInstance().start(new String[] { "1", "-nodb", "-dlist", "1/2/3" }, "DynamicTester");
        // test connection
        new DeviceProxy(deviceName).status();

    }

    @BeforeClass
    public static void setUp() throws DevFailed, IOException {
        // ServerManager.getInstance().addClass(DynamicTester.class.getSimpleName(),
        // DynamicTester.class);
        // ServerManager.getInstance().start(new String[] { INSTANCE_NAME },
        // DynamicTester.class.getSimpleName());
        startDynamicTesterNoDb();
        System.out.println(deviceName);

        // test its state
        final DeviceProxy proxy = new DeviceProxy(deviceName);
        DevState state;
        do {
            state = proxy.state();
            try {
                Thread.sleep(150);
            } catch (final InterruptedException e) {
            }
        } while (state.equals(DevState.INIT));

        TangoCommand cmd = new TangoCommand(deviceName, "RemoveAttributes");
        cmd.execute();

        cmd = new TangoCommand(deviceName, "AddAttribute");
        cmd.executeExtract(new String[] { ATTR_NAME_TO_DELETE, String.valueOf(TangoConst.Tango_DEV_DOUBLE),
                String.valueOf(AttrDataFormat._SCALAR), String.valueOf(AttrWriteType._READ_WRITE) });

        cmd = new TangoCommand(deviceName, "AddAttribute");
        cmd.executeExtract(new String[] { ATTR_NAME, String.valueOf(TangoConst.Tango_DEV_DOUBLE),
                String.valueOf(AttrDataFormat._SCALAR), String.valueOf(AttrWriteType._READ_WRITE) });

        cmd = new TangoCommand(deviceName, "AddAttribute");
        cmd.executeExtract(new String[] { ATTR_NAME_2, String.valueOf(TangoConst.Tango_DEV_STRING),
                String.valueOf(AttrDataFormat._SPECTRUM), String.valueOf(AttrWriteType._READ_WRITE) });

        // try {
        // Thread.sleep(1500000);
        // } catch (final InterruptedException e) {
        // }
    }

    @Test
    public void testAttrTypeChange() throws DevFailed {
        this.resetAttrDevice();
        final TangoCommand cmd = new TangoCommand(deviceName, "ChangeAttribute");
        cmd.executeExtract(new String[] { ATTR_NAME, String.valueOf(TangoConst.Tango_DEV_STRING),
                String.valueOf(AttrDataFormat._SCALAR), String.valueOf(AttrWriteType._READ_WRITE) });

        final Diagnosis diagnosis = new Diagnosis(this.dbReader, new Result(true), false);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_FULL_NAME);
        assertEquals(ResDiagnosis.ATTR_ARCHIVING_CHANGE_DIAG, diagnosis.diagnose(aa, null, false).getDiagnosis());
    }

    @Test
    public void testAttrFormatChange() throws DevFailed {
        this.resetAttrDevice();
        final TangoCommand cmd = new TangoCommand(deviceName, "ChangeAttribute");
        cmd.executeExtract(new String[] { ATTR_NAME, String.valueOf(TangoConst.Tango_DEV_DOUBLE),
                String.valueOf(AttrDataFormat._SPECTRUM), String.valueOf(AttrWriteType._READ_WRITE) });
        final Diagnosis diagnosis = new Diagnosis(this.dbReader, new Result(true), false);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_FULL_NAME);
        assertEquals(ResDiagnosis.ATTR_ARCHIVING_CHANGE_DIAG, diagnosis.diagnose(aa, null, false).getDiagnosis());
    }

    @Test
    public void testAttrWritableChange() throws DevFailed {
        this.resetAttrDevice();
        final TangoCommand cmd = new TangoCommand(deviceName, "ChangeAttribute");
        cmd.executeExtract(new String[] { ATTR_NAME, String.valueOf(TangoConst.Tango_DEV_DOUBLE),
                String.valueOf(AttrDataFormat._SCALAR), String.valueOf(AttrWriteType._READ) });
        final Diagnosis diagnosis = new Diagnosis(this.dbReader, new Result(true), false);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_FULL_NAME);
        assertEquals(ResDiagnosis.ATTR_ARCHIVING_CHANGE_DIAG, diagnosis.diagnose(aa, null, false).getDiagnosis());
    }

    @Test
    public void testAttrDoesNotExist() throws DevFailed {
        this.resetAttrDevice();
        final Diagnosis diagnosis = new Diagnosis(this.dbReader, new Result(true), false);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_FULL_NAME_WRONG);
        assertEquals(ResDiagnosis.NOT_EXIST_ATTR, diagnosis.diagnose(aa, null, false).getDiagnosis());
    }

    @Test
    public void testAttrReadFailed() throws DevFailed {
        this.resetAttrDevice();
        final TangoCommand cmd = new TangoCommand(deviceName, "AddNullAttribute");
        cmd.executeExtract(new String[] { ATTR_NAME_NULL, String.valueOf(TangoConst.Tango_DEV_DOUBLE),
                String.valueOf(AttrDataFormat._SCALAR), String.valueOf(AttrWriteType._READ_WRITE) });
        final Diagnosis diagnosis = new Diagnosis(this.dbReader, new Result(true), false);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_FULL_NAME_NULL);
        assertEquals(ResDiagnosis.ATTR_EXCEPTION_DIAG, diagnosis.diagnose(aa, null, false).getDiagnosis());
    }

    @Test
    public void testDiagFail() throws DevFailed {
        final Diagnosis diagnosis = new Diagnosis(this.dbReader, new Result(true), false);
        final ArchivedAttribute aa = new ArchivedAttribute(WRONG_ATTR_NAME);
        assertEquals(ResDiagnosis.FAIL_DIAG, diagnosis.diagnose(aa, null, false).getDiagnosis());
    }

    @Test
    public void testDbTypeChange() throws DevFailed {
        this.resetAttrDevice();
        when(this.dbReader.getAttTypeCriterion(ATTR_FULL_NAME)).thenReturn(AttributeDataType._ATT_STRING);
        final Diagnosis diagnosis = new Diagnosis(this.dbReader, new Result(true), false);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_FULL_NAME);
        assertEquals(ResDiagnosis.ATTR_ARCHIVING_CHANGE_DIAG, diagnosis.diagnose(aa, null, false).getDiagnosis());
    }

    @Test
    public void testDbWritableChange() throws DevFailed {
        this.resetAttrDevice();
        when(this.dbReader.getAttWritableCriterion(ATTR_FULL_NAME)).thenReturn(AttrWriteType._READ);
        final Diagnosis diagnosis = new Diagnosis(this.dbReader, new Result(true), false);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_FULL_NAME);
        assertEquals(ResDiagnosis.ATTR_ARCHIVING_CHANGE_DIAG, diagnosis.diagnose(aa, null, false).getDiagnosis());
    }

    @Test
    public void testDbFormatChange() throws DevFailed {
        this.resetAttrDevice();
        when(this.dbReader.getAttFormatCriterion(ATTR_FULL_NAME)).thenReturn(AttrDataFormat._IMAGE);
        final Diagnosis diagnosis = new Diagnosis(this.dbReader, new Result(true), false);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_FULL_NAME);
        assertEquals(ResDiagnosis.ATTR_ARCHIVING_CHANGE_DIAG, diagnosis.diagnose(aa, null, false).getDiagnosis());
    }

    @Test
    @Ignore("a an archiver with name must be started archiving/hdb/hdbarchiver.01_01")
    public void testOk() throws DevFailed {
        final Diagnosis diagnosis = new Diagnosis(this.dbReader, new Result(true), false);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_FULL_NAME);
        aa.setArchiver("archiving/hdb/hdbarchiver.01_01");
        assertEquals(ResDiagnosis.ARCHIVING_FAILURE_DIAG, diagnosis.diagnose(aa, null, false).getDiagnosis());
    }

    @Test
    @Ignore("Device shutdown error is not possible in no db tests")
    public void testDeviceNotExported() throws DevFailed {
        final Diagnosis diagnosis = new Diagnosis(this.dbReader, new Result(true), false);

        final ArchivedAttribute aa = new ArchivedAttribute("tango/tangotest/4/double_scalar");
        aa.setArchiver("archiver");
        assertEquals(ResDiagnosis.DEVICE_ERROR_DIAG, diagnosis.diagnose(aa, null, false).getDiagnosis());
    }

    @Test
    public void testDeviceNotExist() throws DevFailed {
        final Diagnosis diagnosis = new Diagnosis(this.dbReader, new Result(true), false);

        final ArchivedAttribute aa = new ArchivedAttribute("titi/toto/tata/double_scalar");
        assertEquals(ResDiagnosis.NOT_EXIST_DEVICE, diagnosis.diagnose(aa, null, false).getDiagnosis());
    }

    // @Test
    // public void testManyAttr() throws DevFailed {
    // this.setGoodDbReader();
    // this.resetAttrDevice();
    // final List<String> attrList = Arrays.asList(ATTR_FULL_NAME,
    // ATTR_FULL_NAME_2);
    // when(this.dbReader.getAttFormatCriterion(ATTR_FULL_NAME)).thenReturn(AttrDataFormat._IMAGE);
    // when(this.dbReader.getAttWritableCriterion(ATTR_FULL_NAME)).thenReturn(AttrWriteType._READ);
    // final Diagnosis diagnosis = new Diagnosis(this.dbReader, null, null);
    // final Map<String, DiagnosisInfo> map = diagnosis.diagnose(attrList);
    // assertEquals(attrList.size(), map.size());
    // for (final String s : attrList) {
    // assertEquals(ResDiagnosis.ATTR_ARCHIVING_CHANGE_DIAG,
    // map.get(s).getDiagnosis());
    // }
    // }

    private void resetAttrDevice() {
        try {
            TangoCommand cmd = new TangoCommand(deviceName, "ChangeAttribute");
            cmd.executeExtract(new String[] { ATTR_NAME, String.valueOf(TangoConst.Tango_DEV_DOUBLE),
                    String.valueOf(AttrDataFormat._SCALAR), String.valueOf(AttrWriteType._READ_WRITE) });

            cmd = new TangoCommand(deviceName, "ChangeAttribute");
            cmd.executeExtract(new String[] { ATTR_NAME_2, String.valueOf(TangoConst.Tango_DEV_STRING),
                    String.valueOf(AttrDataFormat._SPECTRUM), String.valueOf(AttrWriteType._READ_WRITE) });
        } catch (final DevFailed e) {
            e.printStackTrace();
        }
    }

    @Before
    public void setGoodDbReader() throws DevFailed {
        Mockito.reset(this.dbReader);
        //
        doReturn(AttributeDataType._ATT_DOUBLE).when(this.dbReader).getAttTypeCriterion(ATTR_FULL_NAME);

        doReturn(AttrWriteType._READ_WRITE).when(this.dbReader).getAttWritableCriterion(ATTR_FULL_NAME);

        doReturn(AttrDataFormat._SCALAR).when(this.dbReader).getAttFormatCriterion(ATTR_FULL_NAME);
        when(this.dbReader.getAttTypeCriterion(ATTR_FULL_NAME)).thenReturn(AttributeDataType._ATT_DOUBLE);
        when(this.dbReader.getAttWritableCriterion(ATTR_FULL_NAME)).thenReturn(AttrWriteType._READ_WRITE);
        when(this.dbReader.getAttFormatCriterion(ATTR_FULL_NAME)).thenReturn(AttrDataFormat._SCALAR);


        // doReturn("").when(this.dbReader).getLastNotNullInsert(anyString());

    }

    @Test
    public void testAttrRemoved() throws DevFailed {
        this.resetAttrDevice();
        final TangoCommand cmd = new TangoCommand(deviceName, "RemoveAttribute");
        cmd.executeExtract(ATTR_NAME_TO_DELETE);

        final Diagnosis diagnosis = new Diagnosis(this.dbReader, new Result(true), false);

        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_FULL_NAME_TO_DELETE);
        assertEquals(ResDiagnosis.NOT_EXIST_ATTR, diagnosis.diagnose(aa, null, false).getDiagnosis());
    }

}
package fr.soleil.tango.server.watcher.dto.result;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import fr.soleil.tango.server.watcher.controller.Controller.Control;
import fr.soleil.tango.server.watcher.controller.diagnosis.DiagnosisInfo;
import fr.soleil.tango.server.watcher.controller.diagnosis.ResDiagnosis;
import fr.soleil.tango.server.watcher.db.DBReader;
import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;

public class ResultTest {
    private static final String ARCHIVER = "Archiver";
    private static final String DOMAIN = "domain";
    private static final String FAMILY = "family";
    private static final String MEMBER = "member";
    private static final String DEVICE = DOMAIN + "/" + FAMILY + "/" + MEMBER;
    private static final String ATTRIBUTE = "attribute";
    private static final String ATTR_COMPLETE_NAME = DEVICE + "/" + ATTRIBUTE + "/";

    private static final String ARCHIVER_2 = "Archiver_2";
    private static final String DOMAIN_2 = "domain_2";
    private static final String FAMILY_2 = "family_2";
    private static final String MEMBER_2 = "member_2";
    private static final String DEVICE_2 = DOMAIN_2 + "/" + FAMILY_2 + "/" + MEMBER_2;
    private static final String ATTRIBUTE_2 = "attribute_2";
    private static final String ATTR_COMPLETE_NAME_2 = DEVICE_2 + "/" + ATTRIBUTE_2 + "/";

    private static final String MEMBER_3 = "member_3";
    private static final String DEVICE_3 = DOMAIN_2 + "/" + FAMILY_2 + "/" + MEMBER_3;
    private static final String ATTRIBUTE_3 = "attribute_3";
    private static final String ATTR_COMPLETE_NAME_3 = DEVICE_3 + "/" + ATTRIBUTE_3 + "/";

    private ArchivedAttribute aa;
    private ArchivedAttribute aa2;
    private ArchivedAttribute aa3;

    private final DiagnosisInfo diagnosis = new DiagnosisInfo(ResDiagnosis.ATTR_ARCHIVING_CHANGE_DIAG);
    private final DiagnosisInfo diagnosis2 = new DiagnosisInfo(ResDiagnosis.DIS_DIAG);
    private final DiagnosisInfo diagnosis3 = new DiagnosisInfo(ResDiagnosis.DIS_DIAG);

    private final Timestamp t = new Timestamp(System.currentTimeMillis());

    @Mock
    private DBReader dbReader;

    @Before
    public void setUp() {
        this.aa = new ArchivedAttribute(ATTR_COMPLETE_NAME);
        this.aa.setArchiver(ARCHIVER);
        this.aa.setArchivingStatus(Control.CONTROL_KO);
        this.aa.setDiagnosis(this.diagnosis);
        this.aa.setId(1);
        this.aa.setLastInsert(this.t);
        this.aa.setLastInsertRequest(this.t);
        this.aa.setPeriod(1);

        this.aa2 = new ArchivedAttribute(ATTR_COMPLETE_NAME_2);
        this.aa2.setArchiver(ARCHIVER_2);
        this.aa2.setArchivingStatus(Control.CONTROL_OK);
        this.aa2.setDiagnosis(this.diagnosis2);
        this.aa2.setId(1);
        this.aa2.setLastInsert(this.t);
        this.aa2.setLastInsertRequest(this.t);
        this.aa2.setPeriod(1);

        this.aa3 = new ArchivedAttribute(ATTR_COMPLETE_NAME_3);
        this.aa3.setArchiver(ARCHIVER_2);
        this.aa3.setArchivingStatus(Control.CONTROL_OK);
        this.aa3.setDiagnosis(this.diagnosis3);
        this.aa3.setId(1);
        this.aa3.setLastInsert(this.t);
        this.aa3.setLastInsertRequest(this.t);
        this.aa3.setPeriod(1);
    }

    @Test
    public void initTest() {
        final Result r = new Result(false);
        assertEquals(0, r.getAllAttributes().size());
        assertEquals(0, r.getKoArchivers().size());
        assertEquals(0, r.getKOAttributesMap().size());
        assertEquals(0, r.getKoAttributes().size());
        assertEquals(0, r.getNumberOfKoAttributes());
        assertEquals("{}", ArrayUtils.toString(r.getSimpleReportPerArchiver()));
        assertEquals("{}", ArrayUtils.toString(r.getSimpleGlobalReport()));
        assertEquals("{}", ArrayUtils.toString(r.getSimpleReportPerAttribute(false)));
        assertEquals("{}", ArrayUtils.toString(r.getSimpleReportPerAttribute(true)));
        assertEquals("{}", ArrayUtils.toString(r.getSimpleReportPerDomain()));
        assertEquals("{}", ArrayUtils.toString(r.getSimpleReportPerFamily()));
    }

    @Test
    public void addOneTest() {
        final Result r = new Result(false);
        r.addKoAttribute(this.aa);
        assertEquals(0, r.getAllAttributes().size());
        assertEquals(1, r.getKoArchivers().size());
        assertEquals(1, r.getKOAttributesMap().size());
        assertEquals(1, r.getNumberOfKoAttributes());
        assertEquals(1, r.getKoAttributes().size());
        assertTrue(r.isKOAttribute(this.aa.getCompleteName()));
        assertEquals("{{Archiver},{domain/family/member/attribute/}}",
                ArrayUtils.toString(r.getSimpleReportPerArchiver()));
        assertThat(ArrayUtils.toString(r.getSimpleReportPerAttribute(false)),
                containsString("{{domain/family/member/attribute/ (id:1)," + ARCHIVER
                        + ",Attribute archiving configuration changed"));
        assertThat(ArrayUtils.toString(r.getSimpleReportPerAttribute(true)),
                containsString("{{domain/family/member/attribute/ (id:1)," + ARCHIVER
                        + ",Attribute archiving configuration changed"));
        assertEquals("{{domain},{domain/family/member/attribute/}}", ArrayUtils.toString(r.getSimpleReportPerDomain()));
        assertEquals("{{family},{domain/family/member/attribute/}}", ArrayUtils.toString(r.getSimpleReportPerFamily()));
        assertEquals(
                "{{Archiver},{domain/family/member/attribute/},{domain},{domain/family/member/attribute/},{family},{domain/family/member/attribute/}}",
                ArrayUtils.toString(r.getSimpleGlobalReport()));
    }

    @Test
    public void removeOneTest() {
        final Result r = new Result(false);
        r.addKoAttribute(this.aa);
        r.deleteKoAttribute(this.aa);
        assertEquals(0, r.getAllAttributes().size());
        assertEquals(0, r.getKoArchivers().size());
        assertEquals(0, r.getKOAttributesMap().size());
        assertEquals(0, r.getKoAttributes().size());
        assertEquals(0, r.getNumberOfKoAttributes());
        assertFalse(r.isKOAttribute(this.aa.getCompleteName()));
        assertEquals("{}", ArrayUtils.toString(r.getSimpleReportPerArchiver()));
        assertEquals("{}", ArrayUtils.toString(r.getSimpleGlobalReport()));
        assertEquals("{}", ArrayUtils.toString(r.getSimpleReportPerAttribute(false)));
        assertEquals("{}", ArrayUtils.toString(r.getSimpleReportPerAttribute(true)));
        assertEquals("{}", ArrayUtils.toString(r.getSimpleReportPerDomain()));
        assertEquals("{}", ArrayUtils.toString(r.getSimpleReportPerFamily()));
    }

    @Test
    public void addManyDifferentTest() {
        final Result r = new Result(false);
        r.addKoAttribute(this.aa);
        r.addKoAttribute(this.aa2);
        assertEquals(0, r.getAllAttributes().size());
        assertEquals(2, r.getKoArchivers().size());
        assertEquals(2, r.getKOAttributesMap().size());
        assertEquals(2, r.getNumberOfKoAttributes());
        assertEquals(2, r.getKoAttributes().size());
        assertTrue(r.isKOAttribute(this.aa.getCompleteName()));
        assertTrue(r.isKOAttribute(this.aa2.getCompleteName()));
        assertThat(
                ArrayUtils.toString(r.getSimpleReportPerArchiver()),
                containsString("{{Archiver,Archiver_2},{domain/family/member/attribute/,domain_2/family_2/member_2/attribute_2/}}"));
        assertThat(ArrayUtils.toString(r.getSimpleReportPerAttribute(false)),
                containsString("{{domain/family/member/attribute/ (id:1)," + ARCHIVER
                        + ",Attribute archiving configuration changed"));

        assertThat(
                ArrayUtils.toString(r.getSimpleReportPerAttribute(false)),
                containsString(",{domain_2/family_2/member_2/attribute_2/ (id:1)," + ARCHIVER_2 + ",Diagnostic disable"));

        assertThat(
                ArrayUtils.toString(r.getSimpleReportPerAttribute(true)),
                containsString("{{domain_2/family_2/member_2/attribute_2/ (id:1)," + ARCHIVER_2 + ",Diagnostic disable"));

        assertThat(ArrayUtils.toString(r.getSimpleReportPerAttribute(true)), containsString(ARCHIVER
                + ",Attribute archiving configuration changed"));
        assertThat(
                ArrayUtils.toString(r.getSimpleReportPerDomain()),
                containsString("{{domain,domain_2},{domain/family/member/attribute/,domain_2/family_2/member_2/attribute_2/}}"));
        assertThat(
                ArrayUtils.toString(r.getSimpleReportPerFamily()),
                containsString("{{family,family_2},{domain/family/member/attribute/,domain_2/family_2/member_2/attribute_2/}}"));
        assertThat(
                ArrayUtils.toString(r.getSimpleGlobalReport()),
                containsString("{{Archiver,Archiver_2},{domain/family/member/attribute/,domain_2/family_2/member_2/attribute_2/},{domain,domain_2},{domain/family/member/attribute/,domain_2/family_2/member_2/attribute_2/},{family,family_2},{domain/family/member/attribute/,domain_2/family_2/member_2/attribute_2/}}"));
    }

    @Test
    public void addManyTest() {
        final Result r = new Result(false);
        r.addKoAttribute(this.aa);
        r.addKoAttribute(this.aa2);
        r.addKoAttribute(this.aa3);
        assertEquals(0, r.getAllAttributes().size());

        final Map<String, Archiver> errorArchivers = r.getKoArchivers();
        assertEquals(2, errorArchivers.size());
        final Archiver a = errorArchivers.get(ARCHIVER);
        final Archiver a2 = errorArchivers.get(ARCHIVER_2);
        assertEquals(1, a.getKOAttributes().size());
        assertTrue(a.getKOAttributes().contains(ATTR_COMPLETE_NAME));
        assertEquals(2, a2.getKOAttributes().size());
        assertTrue(a2.getKOAttributes().contains(ATTR_COMPLETE_NAME_2));
        assertTrue(a2.getKOAttributes().contains(ATTR_COMPLETE_NAME_3));

        assertEquals(3, r.getKOAttributesMap().size());
        assertEquals(3, r.getNumberOfKoAttributes());
        assertEquals(3, r.getKoAttributes().size());
        assertTrue(r.isKOAttribute(this.aa.getCompleteName()));
        assertTrue(r.isKOAttribute(this.aa2.getCompleteName()));
        assertTrue(r.isKOAttribute(this.aa3.getCompleteName()));
    }
}

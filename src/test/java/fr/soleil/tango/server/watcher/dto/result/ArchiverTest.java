package fr.soleil.tango.server.watcher.dto.result;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;

public class ArchiverTest {
    private static final String ARCHIVER_1 = "a/rchive/r";
    private static final String ARCHIVER_2 = "a/rchive/r2";

    private static final String ATTR = "Domain/Family/Member/Attr";
    private static final String ATTR_2 = "Domain/Family/Member/Attr2";
    private static final String ATTR_3 = "Domain/Family2/Member/Attr";

    @Test
    public void initTest() {
        final Archiver a = new Archiver(ARCHIVER_1, false);
        assertEquals(ARCHIVER_1, a.getName());
        assertTrue(a.isKOEmpty());
    }

    @Test
    public void addTest() {
        final Archiver a = new Archiver(ARCHIVER_1, false);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_3);
        aa.setArchiver(ARCHIVER_2);
        final ArchivedAttribute aa2 = new ArchivedAttribute(ATTR);
        aa2.setArchiver(ARCHIVER_1);
        final ArchivedAttribute aa3 = new ArchivedAttribute(ATTR_2);
        aa3.setArchiver(ARCHIVER_1);
        a.addKOAttribute(aa);
        assertTrue(a.isKOEmpty());
        a.addKOAttribute(aa2);
        assertEquals(1, a.getKOAttributes().size());
        a.addKOAttribute(aa3);
        assertEquals(2, a.getKOAttributes().size());
        a.addKOAttribute(aa3);
        assertEquals(2, a.getKOAttributes().size());
    }

    @Test
    public void removeTest() {
        final Archiver a = new Archiver(ARCHIVER_1, false);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_3);
        aa.setArchiver(ARCHIVER_2);
        final ArchivedAttribute aa2 = new ArchivedAttribute(ATTR);
        aa2.setArchiver(ARCHIVER_1);
        final ArchivedAttribute aa3 = new ArchivedAttribute(ATTR_2);
        aa3.setArchiver(ARCHIVER_1);
        a.addKOAttribute(aa);
        assertTrue(a.isKOEmpty());
        a.addKOAttribute(aa2);
        assertEquals(1, a.getKOAttributes().size());
        a.addKOAttribute(aa3);
        assertEquals(2, a.getKOAttributes().size());
        a.addKOAttribute(aa3);
        assertEquals(2, a.getKOAttributes().size());
        a.removeKOAttribute(aa3.getCompleteName());
        assertEquals(1, a.getKOAttributes().size());
        a.removeKOAttribute(aa.getCompleteName());
        assertEquals(1, a.getKOAttributes().size());
        a.removeKOAttribute(aa2.getCompleteName());
        assertEquals(0, a.getKOAttributes().size());
        assertTrue(a.isKOEmpty());
    }

    @Test
    public void getTest() {
        final Archiver a = new Archiver(ARCHIVER_1, false);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_3);
        aa.setArchiver(ARCHIVER_2);
        final ArchivedAttribute aa2 = new ArchivedAttribute(ATTR);
        aa2.setArchiver(ARCHIVER_1);
        final ArchivedAttribute aa3 = new ArchivedAttribute(ATTR_2);
        aa3.setArchiver(ARCHIVER_1);
        a.addKOAttribute(aa);
        assertTrue(a.isKOEmpty());

        a.addKOAttribute(aa2);
        Collection<String> attrs = a.getKOAttributes();
        assertEquals(1, attrs.size());
        attrs.contains(aa2.getCompleteName());

        a.addKOAttribute(aa3);
        attrs = a.getKOAttributes();
        assertEquals(2, attrs.size());
        attrs.contains(aa2.getCompleteName());
        attrs.contains(aa3.getCompleteName());

        a.addKOAttribute(aa3);
        attrs = a.getKOAttributes();
        attrs.contains(aa2.getCompleteName());
        attrs.contains(aa3.getCompleteName());
    }

    @Test
    public void getterTest() {
        final Archiver a = new Archiver(ARCHIVER_1, false);
        a.setScalarLoad(1);
        a.setSpectrumLoad(1);
        a.setImageLoad(1);
        a.setTotalLoad(1);

        assertEquals(1, a.getScalarLoad());
        assertEquals(1, a.getSpectrumLoad());
        assertEquals(1, a.getImageLoad());
        assertEquals(1, a.getTotalLoad());

    }
}

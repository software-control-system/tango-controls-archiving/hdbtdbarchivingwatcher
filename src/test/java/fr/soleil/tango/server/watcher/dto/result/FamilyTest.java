package fr.soleil.tango.server.watcher.dto.result;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import fr.soleil.tango.server.watcher.dto.attribute.ArchivedAttribute;

public class FamilyTest {

    private static final String FAMILY = "Family";

    private static final String ATTR = "Domain/Family/Member/Attr";
    private static final String ATTR_2 = "Domain/Family/Member/Attr2";
    private static final String ATTR_WRONG = "Domain/Family2/Member/Attr";

    @Test
    public void initTest() {
        final Family f = new Family(FAMILY);
        assertEquals(FAMILY, f.getName());
        assertTrue(f.isKOEmpty());
    }

    @Test
    public void addTest() {
        final Family f = new Family(FAMILY);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_WRONG);
        final ArchivedAttribute aa2 = new ArchivedAttribute(ATTR);
        final ArchivedAttribute aa3 = new ArchivedAttribute(ATTR_2);
        f.addKOAttribute(aa);
        assertTrue(f.isKOEmpty());
        f.addKOAttribute(aa2);
        assertEquals(1, f.getKOAttributes().size());
        f.addKOAttribute(aa3);
        assertEquals(2, f.getKOAttributes().size());
        f.addKOAttribute(aa3);
        assertEquals(2, f.getKOAttributes().size());
    }

    @Test
    public void removeTest() {
        final Family f = new Family(FAMILY);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_WRONG);
        final ArchivedAttribute aa2 = new ArchivedAttribute(ATTR);
        final ArchivedAttribute aa3 = new ArchivedAttribute(ATTR_2);
        f.addKOAttribute(aa);
        assertTrue(f.isKOEmpty());
        f.addKOAttribute(aa2);
        assertEquals(1, f.getKOAttributes().size());
        f.addKOAttribute(aa3);
        assertEquals(2, f.getKOAttributes().size());
        f.addKOAttribute(aa3);
        assertEquals(2, f.getKOAttributes().size());
        f.removeKOAttribute(aa3.getCompleteName());
        assertEquals(1, f.getKOAttributes().size());
        f.removeKOAttribute(aa.getCompleteName());
        assertEquals(1, f.getKOAttributes().size());
        f.removeKOAttribute(aa2.getCompleteName());
        assertEquals(0, f.getKOAttributes().size());
        assertTrue(f.isKOEmpty());
    }

    @Test
    public void getTest() {
        final Family f = new Family(FAMILY);
        final ArchivedAttribute aa = new ArchivedAttribute(ATTR_WRONG);
        final ArchivedAttribute aa2 = new ArchivedAttribute(ATTR);
        final ArchivedAttribute aa3 = new ArchivedAttribute(ATTR_2);
        f.addKOAttribute(aa);
        assertTrue(f.isKOEmpty());

        f.addKOAttribute(aa2);
        Collection<String> attrs = f.getKOAttributes();
        assertEquals(1, attrs.size());
        attrs.contains(aa2.getCompleteName());

        f.addKOAttribute(aa3);
        attrs = f.getKOAttributes();
        assertEquals(2, attrs.size());
        attrs.contains(aa2.getCompleteName());
        attrs.contains(aa3.getCompleteName());

        f.addKOAttribute(aa3);
        attrs = f.getKOAttributes();
        attrs.contains(aa2.getCompleteName());
        attrs.contains(aa3.getCompleteName());
    }
}

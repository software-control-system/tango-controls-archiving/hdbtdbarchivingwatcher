package fr.soleil.tango.server.watcher.dto.attribute;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;

public class ModeDataTest {

    private static final String ARCHIVER = "Archiver";

    @Test
    public void getterTest() {
	ModeData md = new ModeData();
	Mode m = new Mode();
	md.setArchiver(ARCHIVER);
	md.setMode(m);
	assertEquals(ARCHIVER, md.getArchiver());
	assertEquals(m, md.getMode());
    }

}

package fr.soleil.tango.server.watcher.controller.cyclemanager;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.Hashtable;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.tango.server.watcher.controller.AttributeSelection;
import fr.soleil.tango.server.watcher.db.DBReader;
import fr.soleil.tango.server.watcher.dto.attribute.ModeData;

@PowerMockIgnore("javax.management.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ DBReader.class, CycleManager.class })
public class CycleManagerTest {

    private static final String ATTR_CONTROL_OK_1 = "tango/tangotest/1/status";
    private static final String ATTR_CONTROL_OK_2 = "tango/tangotest/1/double_scalar";
    private static final String ATTR_CONTROL_OK_3 = "tango/tangotest/1/ampli";

    private static final String ATTR_CONTROL_KO_1 = "tango/tangotest/1/boolean_scalar";
    private static final String ATTR_CONTROL_KO_2 = "tango/tangotest/1/string_scalar";
    private static final String ATTR_CONTROL_KO_3 = "tango/tangotest/1/test";

    private static Hashtable<String, ModeData> dbNormalReplyFor1 = new Hashtable<String, ModeData>();
    private static Hashtable<String, ModeData> dbNormalReplyForMany = new Hashtable<String, ModeData>();
    private static Hashtable<String, ModeData> dbErrorReplyFor1 = new Hashtable<String, ModeData>();
    private static Hashtable<String, ModeData> dbErrorReplyForMany = new Hashtable<String, ModeData>();
    private static Hashtable<String, ModeData> dbMixteReplyForMany = new Hashtable<String, ModeData>();

    private static final ModeData MODE_DATA = new ModeData();
    private static final Integer SAFETY_PERIOD = 15 * 60 * 1000;

    static {
        dbNormalReplyFor1.put(ATTR_CONTROL_OK_1, new ModeData());

        dbNormalReplyForMany.put(ATTR_CONTROL_OK_1, new ModeData());
        dbNormalReplyForMany.put(ATTR_CONTROL_OK_2, new ModeData());
        dbNormalReplyForMany.put(ATTR_CONTROL_OK_3, new ModeData());

        dbErrorReplyFor1.put(ATTR_CONTROL_KO_1, new ModeData());

        dbErrorReplyForMany.put(ATTR_CONTROL_KO_1, new ModeData());
        dbErrorReplyForMany.put(ATTR_CONTROL_KO_2, new ModeData());
        dbErrorReplyForMany.put(ATTR_CONTROL_KO_3, new ModeData());

        dbMixteReplyForMany.put(ATTR_CONTROL_OK_1, new ModeData());
        dbMixteReplyForMany.put(ATTR_CONTROL_OK_2, new ModeData());
        dbMixteReplyForMany.put(ATTR_CONTROL_OK_3, new ModeData());
        dbMixteReplyForMany.put(ATTR_CONTROL_KO_1, new ModeData());
        dbMixteReplyForMany.put(ATTR_CONTROL_KO_2, new ModeData());
        dbMixteReplyForMany.put(ATTR_CONTROL_KO_3, new ModeData());

        Mode mode = new Mode();
        mode.setModeP(new ModePeriode(30000));
        MODE_DATA.setMode(mode);
        MODE_DATA.setArchiver("fake_archiver");
    }

    private final DBReader dbReader = PowerMockito.mock(DBReader.class);

    @Test
    public void oneOkAttributeTest() throws Exception {
        try {
            this.setGoodDbReaderOk(true);
            AttributeSelection as = new AttributeSelection(this.dbReader);
            final CycleManager cm = new CycleManager(this.dbReader, 1000, false, false, false, as, SAFETY_PERIOD, false);

            Thread t = new Thread() {
                @Override
                public void run() {
                    cm.startDiagnosis();
                    while (true) {
                        try {
                            if (isInterrupted()) {
                                break;
                            }
                            cm.run();
                            if (isInterrupted()) {
                                break;
                            }
                            sleep(60000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            break;
                        }
                    }
                    cm.stopDiagnosis();
                }
            };
            t.start();
            while (cm.getNbrEndCycle() < 1) {
                Thread.sleep(10);
            }
            t.interrupt();
            for (String s : dbNormalReplyFor1.keySet()) {
                PowerMockito.verifyPrivate(cm, times(1)).invoke("processStep", s);
            }
            assertEquals(0, cm.getResult().getNumberOfKoAttributes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Test
    public void manyOkAttributeTest() throws Exception {
        this.setGoodDbReaderOk(false);
        AttributeSelection as = new AttributeSelection(this.dbReader);
        final CycleManager cm = new CycleManager(this.dbReader, 1000, false, false, false, as, SAFETY_PERIOD, false);
        Thread t = new Thread() {
            @Override
            public void run() {
                cm.startDiagnosis();
                while (true) {
                    try {
                        if (isInterrupted()) {
                            break;
                        }
                        cm.run();
                        if (isInterrupted()) {
                            break;
                        }
                        sleep(60000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        break;
                    }
                }
                cm.stopDiagnosis();
            }
        };
        t.start();
        while (cm.getNbrEndCycle() < 1) {
            Thread.sleep(10);
        }
        t.interrupt();

        assertEquals(0, cm.getResult().getNumberOfKoAttributes());
    }


    private void setGoodDbReaderOk(boolean onlyOneAttr) throws DevFailed {
        this.setGoodDbReader();
        DateTime dt = new DateTime();
        if (onlyOneAttr) {
            when(this.dbReader.getArchivedAttributes(new AttrWriteType[0], new Integer[0], new AttrDataFormat[0]))
                    .thenReturn(dbNormalReplyFor1);
        } else {
            when(this.dbReader.getArchivedAttributes(new AttrWriteType[0], new Integer[0], new AttrDataFormat[0]))
                    .thenReturn(dbNormalReplyForMany);
        }
        when(
                this.dbReader.getTimeValueNullOrNotOfLastInsert(ATTR_CONTROL_OK_1, AttrDataFormat._SCALAR,
                        AttrWriteType._READ_WRITE)).thenReturn(
                new String[] { (new Timestamp(dt.getMillis())).toString(), "notnull" });
        when(
                this.dbReader.getTimeValueNullOrNotOfLastInsert(ATTR_CONTROL_OK_2, AttrDataFormat._SCALAR,
                        AttrWriteType._READ_WRITE)).thenReturn(
                new String[] { (new Timestamp(dt.getMillis())).toString(), "notnull" });
        when(
                this.dbReader.getTimeValueNullOrNotOfLastInsert(ATTR_CONTROL_OK_3, AttrDataFormat._SCALAR,
                        AttrWriteType._READ_WRITE)).thenReturn(
                new String[] { (new Timestamp(dt.getMillis())).toString(), "notnull" });
    }



    private void setGoodDbReader() throws DevFailed {
        reset(this.dbReader);

        // OK FOR ALL
        when(this.dbReader.getModeDataForAttribute(anyString())).thenReturn(MODE_DATA);
        when(this.dbReader.now()).thenReturn(new Timestamp(System.currentTimeMillis()));
        when(this.dbReader.getAttFormatCriterion(anyString())).thenReturn(AttrDataFormat._SCALAR);
        when(this.dbReader.getAttWritableCriterion(anyString())).thenReturn(AttrWriteType._READ_WRITE);
        when(this.dbReader.getAttributeId(anyString())).thenReturn(1);
    }
}

package fr.soleil.tango.server.watcher.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.tango.server.watcher.controller.Controller.Control;
import fr.soleil.tango.server.watcher.db.DBReader;
import fr.soleil.tango.server.watcher.dto.attribute.ModeData;

@RunWith(MockitoJUnitRunner.class)
public class ControllerTest {

    private static final String ATTR_NAME_OK = "tango/tangotest/1/status";
    private static final String ATTR_NAME_KO = "tango/tangotest/1/double_scalar";
    private static final String ATTR_NAME_NULL = "tango/tangotest/1/boolean_scalar";
    private static final ModeData MODE_DATA = new ModeData();
    private static final Integer SAFETY_PERIOD = 15 * 60 * 1000;

    {
        final Mode mode = new Mode();
        mode.setModeP(new ModePeriode(30000));
        MODE_DATA.setMode(mode);
        MODE_DATA.setArchiver("fake_archiver");
    }

    @Mock
    private DBReader dbReader;

    @Test
    public void ControllOkTest() throws DevFailed, ArchivingException {
        final Controller c = new Controller(this.dbReader, SAFETY_PERIOD);
        assertEquals(Control.CONTROL_OK, c.control(ATTR_NAME_OK, MODE_DATA));
    }

    @Test
    public void ControllKoTest() throws DevFailed, ArchivingException {
        final Controller c = new Controller(this.dbReader, SAFETY_PERIOD);
        assertEquals(Control.CONTROL_KO, c.control(ATTR_NAME_KO, MODE_DATA));
    }

    @Test
    public void ControllNullTest() throws DevFailed, ArchivingException {
        final Controller c = new Controller(this.dbReader, SAFETY_PERIOD);
        assertEquals(Control.CONTROL_NULL, c.control(ATTR_NAME_NULL, MODE_DATA));
    }

    @Before
    public void setGoodDbReader() throws DevFailed {
        reset(this.dbReader);
        final DateTime dt = new DateTime();
        when(
                this.dbReader.getTimeValueNullOrNotOfLastInsert(ATTR_NAME_OK, AttrDataFormat._SCALAR,
                        AttrWriteType._READ_WRITE)).thenReturn(
                new String[] { new Timestamp(dt.getMillis()).toString(), "notnull" });
        when(
                this.dbReader.getTimeValueNullOrNotOfLastInsert(ATTR_NAME_KO, AttrDataFormat._SCALAR,
                        AttrWriteType._READ_WRITE)).thenReturn(
                new String[] { new Timestamp(dt.minusDays(1).getMillis()).toString(), "notnull" });
        when(
                this.dbReader.getTimeValueNullOrNotOfLastInsert(ATTR_NAME_NULL, AttrDataFormat._SCALAR,
                        AttrWriteType._READ_WRITE)).thenReturn(
                new String[] { new Timestamp(dt.getMillis()).toString(), "null" });

        when(this.dbReader.now()).thenReturn(new Timestamp(System.currentTimeMillis()));

        when(this.dbReader.getAttFormatCriterion(anyString())).thenReturn(AttrDataFormat._SCALAR);
        when(this.dbReader.getAttWritableCriterion(anyString())).thenReturn(AttrWriteType._READ_WRITE);
    }
}
